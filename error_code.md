|Code|Error|
|----|-----|
|1000|Validation error|
|1001|Email already exists|
|1002|Not found exception|
|1003|Token does not match|
|1004|Password doestn't match|
|1005|Email already exist|
|1006|email verified|
|1009|can't upload file|
|1010|Employee didn't applied this job or already accepted|
|1011|Token & ID not match|
|1012|The application already accepted|
|1013|Permission denied|
|1014|Conversation already created|