var $ = window.$ = require('jquery');
var jQuery = window.jQuery = require('jquery');
require('angular');
require('lodash');
require('angular-cookies');
require('angular-resource');
require('angular-jwt');
require('./config');
require('./api/');

(function(app) {
    app.run(['$rootScope', '$cookieStore', 'API', function($rootScope, $cookieStore, API) {
        console.log($cookieStore.get(JWT_TOKEN_COOKIE_KEY));
        if (!_.isNil($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
            API.user.profile(['role'])
                .then(function(user) {
                    $rootScope.user = user;
                    $rootScope.isLoggedIn = true;
                    console.log($rootScope.user);
                })
                .catch(function(error) {
                    $rootScope.isLoggedIn = false;
                    $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                });
        } else {
            $rootScope.isLoggedIn = false;
            $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
        }
    }]);


    app.controller('MainCtrl', MainCtrl);
    MainCtrl.$inject = ['$rootScope'];

    function MainCtrl($rootScope, $state, Notification, $mdDialog) {

    }
})(angular.module('hybird-app', [
    'app.config',
    'ngCookies',
    'ngResource',
    'app.api',
]));
