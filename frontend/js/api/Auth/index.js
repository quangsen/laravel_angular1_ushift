(function(app) {
    app.service('AuthService', AuthService);
    AuthService.$inject = ['$http', '$q', '$cookieStore', 'ApiUrl'];

    function AuthService($http, $q, $cookieStore, ApiUrl) {
        this.login = function(email, password, remember) {
            var url = ApiUrl.get('/api/login');
            var data = {
                email: email,
                password: password,
            };
            var deferred = $q.defer();
            $http.post(url, data)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
        this.refreshToken = function() {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/auth/refresh-token');
            $http.put(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.logout = function() {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/auth/logout');
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }
})(angular.module('app.api.auth', []));
