var Category = require('../../models/Category');
(function(app) {
    app.service('CategoryService', CategoryService);
    CategoryService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function CategoryService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/categories/:id');
        var resource = $resource(url, { cache: true }, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            }
        });

        this.get = function(level) {
            var deferred = $q.defer();
            if (level === undefined) {
                resource.get({}, function(response) {
                    var categories = _.map(response.data, function(item) {
                        return new Category(item);
                    });
                    deferred.resolve(categories);
                }, function(error) {
                    deferred.reject(error);
                });
            } else {
                var url = ApiUrl.get('/api/categories/level/' + parseInt(level));
                $http.get(url, { cache: true })
                    .then(function(response) {
                        var categories = _.map(response.data.data, function(item) {
                            return new Category(item);
                        });
                        deferred.resolve(categories);
                    }, function(error) {
                        deferred.reject(error);
                    });
            }
            return deferred.promise;
        };

        this.show = function(id) {
            var deferred = $q.defer();
            resource.get({ id: id, include: 'children' }, function(result) {
                var category = result.data;
                category.children = _.map(category.children.data, function(item) {
                    return new Category(item);
                });
                deferred.resolve(new Category(category));
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

    }
})(angular.module('app.api.category', []));
