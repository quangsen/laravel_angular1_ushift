require('./ApiUrl');
require('./Auth/');
require('./User/');
require('./Category/');
require('./Job/');
require('./Industry/');
require('./EducationLevel/');
require('./PreferredPosition/');
require('./PreferredLocation/');
require('./WorkingExperience/');
require('./Inbox/');
(function(app) {
    app.factory('API', theFactory);

    theFactory.$inject = [
        'AuthService',
        'UserService',
        'CategoryService',
        'JobService',
        'IndustryService',
        'EducationLevelService',
        'PreferredPositionService',
        'PreferredLocationService',
        'WorkingExperienceService',
        'InboxService'
    ];

    function theFactory(
        AuthService,
        UserService,
        CategoryService,
        JobService,
        IndustryService,
        EducationLevelService,
        PreferredPositionService,
        PreferredLocationService,
        WorkingExperienceService,
        InboxService
    ) {
        return {
            auth: AuthService,
            user: UserService,
            category: CategoryService,
            job: JobService,
            industry: IndustryService,
            education_level: EducationLevelService,
            preferred_position: PreferredPositionService,
            preferred_location: PreferredLocationService,
            working_experience: WorkingExperienceService,
            inbox: InboxService
        };
    }
})(angular.module('app.api', [
    'app.api.url',
    'app.api.auth',
    'app.api.user',
    'app.api.category',
    'app.api.job',
    'app.api.industry',
    'app.api.education-level',
    'app.api.preferred-position',
    'app.api.preferred-location',
    'app.api.working-experience',
    'app.api.inbox',
]));
