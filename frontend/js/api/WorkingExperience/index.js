var WorkingExperience = require('../../models/WorkingExperience');

(function(app) {

    app.service('WorkingExperienceService', WorkingExperienceService);
    WorkingExperienceService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function WorkingExperienceService ($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/working-experiences/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function() {
            var deferred = $q.defer();
            resource.get({}, function(response) {
                var workingExperiences = _.map(response.data, function(item) {
                    return new WorkingExperience(item);
                });
                deferred.resolve(workingExperiences);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.working-experience', []));
