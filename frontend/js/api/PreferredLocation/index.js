(function(app) {

    app.service('PreferredLocationService', PreferredLocationService);
    PreferredLocationService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function PreferredLocationService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/preferred-locations/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function() {
            var deferred = $q.defer();
            resource.get({}, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.preferred-location', []));
