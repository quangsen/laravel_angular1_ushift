var EducationLevel = require('../../models/EducationLevel');

(function(app) {

    app.service('EducationLevelService', EducationLevelService);
    EducationLevelService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function EducationLevelService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/education-levels/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function() {
            var deferred = $q.defer();
            resource.get({}, function(response) {
                var educationLevels = _.map(response.data, function(item) {
                    return new EducationLevel(item);
                });
                deferred.resolve(educationLevels);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.education-level', []));
