var Job = require('../../models/Job');
var Employer = require('../../models/Employer');
var User = require('../../models/User');
var Pagination = require('../../models/Pagination');
var EmployeeApplied = require('../../models/EmployeeApplied');

(function(app) {

    app.service('JobService', JobService);
    JobService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function JobService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/jobs/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            delete: {
                method: 'DELETE'
            }
        });

        this.get = function(data, include) {
            var params = {};
            params = data || {
                page: 1
            };
            if (include !== undefined && include !== null) {
                params.include = include.join(',');
            }
            var deferred = $q.defer();
            if (data.category_id === undefined) {
                resource.get(params, function(response) {
                    var jobs = _.map(response.data, function(item) {
                        return new Job(item);
                    });
                    var data = {
                        jobs: jobs,
                        pagination: new Pagination(response.meta.pagination)
                    };
                    deferred.resolve(data);
                }, function(error) {
                    deferred.reject(error);
                });
            } else {
                var url = ApiUrl.get('/api/categories/' + data.category_id + '/jobs');
                $http.get(url, {
                        cache: true,
                        params: {
                            page: data.page,
                            active: 1,
                            include: 'employer'
                        }
                    })
                    .then(function(response) {
                        var jobs = _.map(response.data.data, function(item) {
                            return new Job(item);
                        });
                        var data = {
                            jobs: jobs,
                            pagination: new Pagination(response.data.meta.pagination)
                        };
                        deferred.resolve(data);
                    }, function(error) {
                        deferred.reject(error.data);
                    });
            }
            return deferred.promise;
        };

        this.create = function(data) {
            var deferred = $q.defer();
            data = data || {};
            resource.create(data, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(job) {
            var deferred = $q.defer();
            job = job || {};
            resource.update({
                id: job.getId()
            }, job, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getBySlug = function(slug, include) {
            slug = slug || '';
            if (!_.isUndefined(include) && _.isArray(include)) {
                include = include.join(',');
            }
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/slug/' + slug);
            $http.get(url, {
                    params: {
                        include: include
                    }
                })
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.getRelated = function(id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + id + '/related');
            $http.get(url, {
                    params: {
                        include: 'employer'
                    }
                })
                .then(function(response) {
                    var jobs = _.map(response.data.data, function(item) {
                        item.employer = new Employer(item.employer.data);
                        return new Job(item);
                    });
                    var data = {
                        jobs: jobs,
                        pagination: new Pagination(response.data.meta.pagination),
                    };
                    deferred.resolve(data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined && include instanceof Array) {
                params.include = include.join(',');
            }
            resource.get(params, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.applications = function(id, user_id, params) {
            var deferred = $q.defer();

            params = params || {};

            var url = ApiUrl.get('/api/jobs/' + id + '/applications');

            if (!_.isUndefined(user_id)) {
                url += '/' + user_id;
            }

            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    emloyees = _.map(response.data.data, function(item) {
                        return new EmployeeApplied(item);
                    });
                    deferred.resolve(emloyees);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        function getApplications(items) {
            var applications = [];
            _.forEach(items, function(item) {
                applications.push(item.application);
            });
            return applications;
        }

        this.apply = function(data, job_id) {
            var deferred = $q.defer();
            data = data || {};
            var url = ApiUrl.get('/api/jobs/' + job_id + '/apply');
            $http.post(url, data)
                .then(function(response) {
                    var emloyees = _.groupBy(response.data.data, 'employee_id');
                    emloyees = _.map(emloyees, function(items) {
                        var obj = items[0];
                        obj.applications = getApplications(items);
                        _.unset(obj, 'application');
                        return obj;
                    });
                    emloyees = _.map(emloyees, function(item) {
                        return new EmployeeApplied(item);
                    });
                    deferred.resolve(emloyees);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.isSentApplication = function(job_id, user_id) {

            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/apply/' + user_id + '/is-sent-application');
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.changeApplicationStatus = function(job_id, employee_id, data) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/change-application-status');
            var params = {
                employee_id: employee_id,
                data: data,
            };
            $http.put(url, params)
                .then(function(response) {
                    var emloyees = _.groupBy(response.data.data, 'employee_id');
                    emloyees = _.map(emloyees, function(items) {
                        var obj = items[0];
                        obj.applications = getApplications(items);
                        _.unset(obj, 'application');
                        return obj;
                    });
                    emloyees = _.map(emloyees, function(item) {
                        return new EmployeeApplied(item);
                    });
                    deferred.resolve(emloyees);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.reject = function(job_id, employee_id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/reject');
            var params = {
                employee_id: employee_id,
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new Application(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.cancelPendingJob = function(job_id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/cancel-pending-job');
            $http.put(url)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.changeJobStatus = function(job_id, status) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/status');
            $http.put(url, {
                    completed: status
                })
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.job', []));
