var User = require('../../models/User');
var UserApplication = require('../../models/UserApplication');
var Job = require('../../models/Job');
var Shift = require('../../models/Shift');
var Pagination = require('../../models/Pagination');
(function(app) {
    app.service('UserService', UserService);

    UserService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function UserService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/users/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            },
            delete: {
                method: 'DELETE',
            }
        });

        this.profile = function(include) {
            var url = ApiUrl.get('/api/me');
            var deferred = $q.defer();
            var params = {};
            if (include !== undefined && include instanceof Array) {
                params.include = include.join(',');
            }
            $http({
                    url: url,
                    method: "GET",
                    params: params
                })
                .then(function(response) {
                    var user = new User(response.data.data);
                    deferred.resolve(user);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.get = function() {
            var deferred = $q.defer();
            resource.get({}, function(response) {
                var users = _.map(response.data, function(item) {
                    return new User(item);
                });
                deferred.resolve(users);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new User(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(user) {
            var deferred = $q.defer();
            user = user || {};
            resource.create({}, user, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(user) {
            var deferred = $q.defer();
            user = user || {};
            if (user instanceof User) {
                resource.update({
                    id: user.getId()
                }, user, function(response) {
                    var user = new User(response.data);
                    deferred.resolve(user);
                }, function(error) {
                    deferred.reject(error.data);
                });
                return deferred.promise;
            } else {
                deferred.reject('user must be instance of \models\User');
            }
        };

        this.forgot_password = function(email) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/email');
            $http.post(url, {
                    email: email
                })
                .then(function(response) {
                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.reset_password = function(data) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/reset');
            $http.post(url, data)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.verify_email = function(user_id, token) {
            var deferred = $q.defer();
            if (user_id !== undefined && user_id !== '' && token !== undefined && token !== '') {
                var url = '/api/users/' + user_id + '/verify-email';
                var params = {
                    token: token
                };
                url = ApiUrl.get(url);
                $http.put(url, params)
                    .then(function(response) {
                        deferred.resolve(response.data);
                    }, function(error) {
                        deferred.reject(error.data);
                    });
                return deferred.promise;
            } else {
                deferred.reject("id or token doesn't exist");
            }
        };

        this.last_position = function(id) {
            var deferred = $q.defer();
            var url = '/api/users/' + id + '/last-positions';
            url = ApiUrl.get(url);
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.add_last_position = function(user_id, params) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions';
            url = ApiUrl.get(url);
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_last_position = function(user_id, id, params) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions/' + id;
            url = ApiUrl.get(url);
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.delete_last_position = function(user_id, id) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions/' + id;
            url = ApiUrl.get(url);
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_employee = function(id, data) {
            var deferred = $q.defer();
            var url = '/api/users/' + id + '/employee?include=role,employee';
            data = data || {};
            $http.put(url, data)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_employer = function(id, user) {
            var deferred = $q.defer();
            var url = '/api/users/' + id + '/employer';
            user = user || {};
            $http.put(url, user)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.isOwner = function(user_id, job_id) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/is-owner-of/' + job_id;
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getJobsOfEmployer = function(user_id, params) {
            var deferred = $q.defer();
            params = params || {};
            params.status = params.status || 'pending';
            params.page = params.page || 1;
            params.per_page = params.per_page || 10;
            var url = '/api/user/' + user_id + '/employer/jobs';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var jobs = _.map(response.data.data, function(item) {
                        return new Job(item);
                    });
                    deferred.resolve(jobs);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.appliedShifts = function(user_id, params) {
            var deferred = $q.defer();
            var url = '/api/user/' + user_id + '/applied-shifts';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var shifts = _.map(response.data.data, function(item) {
                        return new Shift(item);
                    });
                    deferred.resolve(shifts);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.user', []));
