(function(app) {

    app.service('PreferredPositionService', PreferredPositionService);
    PreferredPositionService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function PreferredPositionService ($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/preferred-positions/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function() {
            var deferred = $q.defer();
            resource.get({}, function(response) {
                var preferredPositions = response.data;
                deferred.resolve(preferredPositions);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.preferred-position', []));
