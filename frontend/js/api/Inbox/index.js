var User = require('../../models/User');
var Conversation = require('../../models/Conversation');
var Message = require('../../models/Message');
var Pagination = require('../../models/Pagination');
(function(app) {
    app.service('InboxService', InboxService);
    InboxService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function InboxService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/conversations/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(include) {
            var deferred = $q.defer();
            var params = {};
            if (!_.isUndefined(include) && _.isArray(include)) {
                params.include = include.join(',');
            }
            resource.get(params, function(response) {
                var conversations = _.map(response.data, function(item) {
                    if (!_.isUndefined(item.employer)) {
                        item.receiver = new User(item.employer.data);
                    }
                    if (!_.isUndefined(item.employee)) {
                        item.receiver = new User(item.employee.data);
                    }
                    return new Conversation(item);
                });
                deferred.resolve(conversations);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getLastMessage = function(conversation_id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/' + conversation_id + '/last-message');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    deferred.resolve(new Message(response.data.data));
                }, function(error) {
                    deferred.resolve(undefined);
                });
            return deferred.promise;
        };

        this.clear = function(conversation_id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/' + conversation_id + '/clear');
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.getNewMessage = function(conversation_id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/' + conversation_id + '/new-message');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var messages = _.map(response.data.data, function(item) {
                        return new Message(item);
                    });
                    deferred.resolve(messages);
                }, function(error) {
                    deferred.resolve(undefined);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                if (!_.isUndefined(response.data.employer)) {
                    response.data.receiver = new User(response.data.employer.data);
                }
                if (!_.isUndefined(response.data.employee)) {
                    response.data.receiver = new User(response.data.employee.data);
                }
                deferred.resolve(new Conversation(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.messages = function(id, page) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/' + id + '/messages');
            page = page || 1;
            var params = {
                page: page
            };
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var messages = _.map(response.data.data, function(item) {
                        return new Message(item);
                    });
                    deferred.resolve(messages);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.createNewMessageInstance = function(data) {
            return new Message(data);
        };

        this.send = function(conversation_id, message) {
            var data = {
                conversation_id: conversation_id,
                content: message.getContent(),
                type: message.getType(),
            };
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/' + conversation_id + '/messages');
            $http.post(url, data)
                .then(function(response) {
                    deferred.resolve(new Message(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getOldMessage = function(conversation_id, message_id, page) {
            var deferred = $q.defer();
            page = page || 1;
            var url = ApiUrl.get('/api/conversations/' + conversation_id + '/old-messages/' + message_id);
            $http.get(url, {
                    params: {
                        page: page
                    }
                })
                .then(function(response) {
                    var messages = _.map(response.data.data, function(item) {
                        return new Message(item);
                    });
                    deferred.resolve(messages);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.initConversation = function(receiver_id, params, job_id) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/conversations/init');
            var data = {
                receiver_id: receiver_id,
            };
            if (!_.isUndefined(job_id)) {
                data.job_id = job_id;
            }
            $http.post(url, data, {
                    params: params
                })
                .then(function(response) {
                    var conversation = response.data.data;
                    if (!_.isUndefined(conversation.employer)) {
                        conversation.receiver = new User(conversation.employer.data);
                    }
                    if (!_.isUndefined(conversation.employee)) {
                        conversation.receiver = new User(conversation.employee.data);
                    }
                    deferred.resolve(new Conversation(conversation));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }
})(angular.module('app.api.inbox', []));
