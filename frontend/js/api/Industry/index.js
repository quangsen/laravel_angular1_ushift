var Industry = require('../../models/Industry');
var PreferredPosition = require('../../models/PreferredPosition');

(function(app) {

    app.service('IndustryService', IndustryService);
    IndustryService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function IndustryService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/industries/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(include) {
            var deferred = $q.defer();
            var params = {};
            if (include !== undefined) {
                params.include = include.join(',');
            }
            resource.get(params, function(response) {
                var industries = _.map(response.data, function(item) {
                    return new Industry(item);
                });
                deferred.resolve(industries);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join(',');
            }
            resource.get(params, function(response) {
                deferred.resolve(new Industry(response.data));
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.industry', []));
