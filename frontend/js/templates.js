(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/_application_paginator.tpl.html',
    '<div class="application-paginator">\n' +
    '    <ul>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page - 1" ng-show="page > 1"><i class="material-icons">keyboard_arrow_left</i></a>\n' +
    '        </li>\n' +
    '        <li>Page <span ng-bind="page"></span> of <span ng-bind="data.getTotalPages()"></span></li>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page + 1" ng-show="page < data.getTotalPages()"><i class="material-icons">keyboard_arrow_right</i></a>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/_breadcrumb.tpl.html',
    '<nav class="breadcrumb-navbar" ng-if="items | hasItem">\n' +
    '    <div class="container">\n' +
    '        <div class="nav-wrapper">\n' +
    '            <div class="col s12">\n' +
    '                <a ng-repeat="item in items" class="breadcrumb" ng-class="{\'breadcrumb-item-first\': $first}" ng-bind="item.name" ng-href="{{item.href}}"></a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</nav>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/_conversation_item.tpl.html',
    '<img ng-src="{{ conversation.getReceiver().getImage() }}" class="circle">\n' +
    '<div class="conversation-item-content">\n' +
    '    <div ng-if="type != \'notification\'">\n' +
    '        <a ui-sref="app.inbox.message({conversation_id: conversation.getId()})" class="title" ng-bind="conversation.getTitle()"></a>\n' +
    '        <p><strong ng-bind="conversation.getReceiver().getUsername()"></strong></p>\n' +
    '    </div>\n' +
    '    <p ng-if="lastMessage.getType() == 1" ng-bind="lastMessage.getContent()"></p>\n' +
    '    <p ng-if="lastMessage.getType() == 2">\n' +
    '        The job has been completed\n' +
    '    </p>\n' +
    '    <div ng-if="lastMessage.getType() === 4">\n' +
    '        <p ng-if="lastMessage.getSenderId() == user.getId()">\n' +
    '            You have sent a application\n' +
    '        </p>\n' +
    '        <p ng-if="lastMessage.getSenderId() != user.getId()">\n' +
    '            You have new application\n' +
    '        </p>\n' +
    '    </div>\n' +
    '    <div ng-if="lastMessage.getType() === 5">\n' +
    '        <p ng-if="type == \'notification\'">\n' +
    '            <span ng-if="user.getId() === lastMessage.getSenderId() && lastMessage.isAcceptNewApplication()">You accepted a new application</span>\n' +
    '            <span ng-if="user.getId() === lastMessage.getSenderId() && !lastMessage.isAcceptNewApplication()">The application has been cancelled</span>\n' +
    '            <span ng-if="user.getId() !== lastMessage.getSenderId() && lastMessage.isAcceptNewApplication()">Your application has been accepted</span>\n' +
    '            <span ng-if="user.getId() !== lastMessage.getSenderId() && !lastMessage.isAcceptNewApplication()">Your application has been cancelled</span>\n' +
    '        </p>\n' +
    '        <p ng-if="type == \'inbox\'">\n' +
    '            <span ng-if="user.getId() === lastMessage.getSenderId() && lastMessage.isAcceptNewApplication()">You accepted a new application</span>\n' +
    '            <span ng-if="user.getId() === lastMessage.getSenderId() && !lastMessage.isAcceptNewApplication()">The application has been cancelled</span>\n' +
    '            <span ng-if="user.getId() !== lastMessage.getSenderId() && lastMessage.isAcceptNewApplication()">Your application has been accepted</span>\n' +
    '            <span ng-if="user.getId() !== lastMessage.getSenderId() && !lastMessage.isAcceptNewApplication()">Your application has been cancelled</span>\n' +
    '        </p>\n' +
    '\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/_preloader.tpl.html',
    '<div class="preloader-wrapper active small">\n' +
    '    <div class="spinner-layer spinner-red-only">\n' +
    '        <div class="circle-clipper left">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="gap-patch">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="circle-clipper right">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/_simple_pagination.tpl.html',
    '<ul class="pagination right pagination-box">\n' +
    '    <li ng-show="page !== 1" class="waves-effect" ng-click="prev()"><a href="javascript:;"><i class="material-icons">chevron_left</i></a></li>\n' +
    '    <li class="waves-effect active">\n' +
    '        Page <span ng-bind="page"></span>\n' +
    '    </li>\n' +
    '    <li class="waves-effect" ng-click="next(nextPage)"><a><i class="material-icons">chevron_right</i></a></li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/partials/pagination.tpl.html',
    '<ul class="pagination right-align right pagination-box" ng-show="data.getTotalPages() > 1">\n' +
    '	<li ng-show="firstPage !== undefined" class="waves-effect" ng-click="goto(firstPage)"><a><i class="material-icons">first_page</i></a></li>\n' +
    '	<li ng-show="prevPage !== undefined" class="waves-effect" ng-click="goto(prevPage)"><a href="javascript:;"><i class="material-icons">chevron_left</i></a></li>\n' +
    '	<li class="waves-effect active" ng-click="goto(currentPage)">\n' +
    '        <a ng-bind="currentPage.number"></a>\n' +
    '    </li>\n' +
    '	<li ng-show="nextPage !== undefined" class="waves-effect" ng-click="goto(nextPage)"><a href="javascript:;"><i class="material-icons">chevron_right</i></a></li>\n' +
    '	<li ng-show="lastPage !== undefined" class="waves-effect" ng-click="goto(lastPage)"><a href="javascript:;"><i class="material-icons">last_page</i></a></li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/application/application.tpl.html',
    '<div class="box-gray">\n' +
    '    <div class="container">\n' +
    '        <div class="row" id="application">\n' +
    '            <div class="white col s12 m4 l3 user" ng-if="user.isEmployer()">\n' +
    '                <div class="center-align avatar">\n' +
    '                    <user-profile-image image="vm.employee.getImage()"></user-profile-image>\n' +
    '                    <p><a ui-sref="app.profile({user_id: vm.employee.getEmployeeId()})"><strong ng-bind="vm.employee.getName()"></strong></a></p>\n' +
    '                </div>\n' +
    '                <p><strong>Gender: </strong><span ng-bind="vm.employee.getGender()"></span></p>\n' +
    '                <p ng-show="vm.employee.getEmail()"><strong>Email: </strong><span ng-bind="vm.employee.getEmail()"></span></p>\n' +
    '                <p ng-show="vm.employee.getMobile()"><strong>Tel: </strong><span ng-bind="vm.employee.getMobile()"></span></p>\n' +
    '                <p ng-show="vm.employee.getBirth()"><strong>Birthday: </strong><span ng-bind="vm.employee.getBirth() | timeFormat:\'LL\'"></span></p>\n' +
    '                <p ng-show="vm.employee.getAddress()"><strong>Address: </strong><span ng-bind="vm.employee.getAddress()"></span></p>\n' +
    '                <p ng-show="vm.employee.getSummary()"><strong>About Me: </strong><span ng-bind-html="vm.employee.getSummary()"></span></p>\n' +
    '                <div class="contact-btn">\n' +
    '                    <button class="btn btn-flat" ng-click="initConversation(vm.employee.getId(), vm.job.getId())">Contact</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="white col s12 m4 l3 user" ng-if="user.isEmployee()">\n' +
    '                <div class="center-align avatar">\n' +
    '                    <user-profile-image image="vm.job.employer.getImage()"></user-profile-image>\n' +
    '                    <p><a ui-sref="app.profile({user_id: vm.job.employer.getEmployeeId()})"><strong ng-bind="vm.job.employer.getName()"></strong></a></p>\n' +
    '                </div>\n' +
    '                <p><strong>Company: </strong><span ng-bind="vm.job.employer.getCompanyName()"></span></p>\n' +
    '                <p><strong>About: </strong><span ng-bind="vm.job.employer.getCompanyAbout()"></span></p>\n' +
    '                <p><strong>Preferred Contact Method:</strong> <span ng-bind="vm.job.employer.getPreferredContactMethod()"></span></p>\n' +
    '                <p><strong>Preferred Contact Time:</strong> <span ng-bind="vm.job.employer.getPreferredContactTime()"></span></p>\n' +
    '                <p ng-show="vm.job.employer.getEmail()"><strong>Email: </strong><span ng-bind="vm.job.employer.getEmail()"></span></p>\n' +
    '                <div class="contact-btn">\n' +
    '                    <button class="btn btn-flat" ng-click="initConversation(vm.job.employer.getEmployeeId(), vm.job.getId())">Contact</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col s12 m8 l9">\n' +
    '                <div class="white job">\n' +
    '                    <div class="col s12">\n' +
    '                        <div class="job-detail">\n' +
    '                            <h5 class="job-name">{{ vm.job.getTitle() }} <i ng-if="vm.job.isCompleted()" class="material-icons green-text">check_circle</i></h5>\n' +
    '                            <span><small>Job are created form: {{vm.job.getCreatedAtDate() | timeFormat: \'YYYY/MM/DD\'}}</small></span>\n' +
    '                            <div class="row">\n' +
    '                                <div class="exchenge-title col s12 m3 l2">\n' +
    '                                    <p><strong>Description</strong></p>\n' +
    '                                </div>\n' +
    '                                <div class="content col s12 m9 l10" ng-bind-html="vm.job.getShortDesc()"></div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="ee-infomation" ng-if="user.isEmployer()">\n' +
    '                            <div class="row">\n' +
    '                                <div class="work profile-card top-nav white-text col s12">\n' +
    '                                    <div class="card-header">Interested In</div>\n' +
    '                                    <div class="card-content white black-text">\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().kind_of_work">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p><strong>Preferred Work Type:</strong> <span ng-bind="vm.employee.getEmployee().kind_of_work"></span></p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().hours_per_week">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p><strong>I can work for:</strong> <span ng-bind="vm.employee.getEmployee().hours_per_week"></span> hours/week</p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().industry.id">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p><strong>Industry:</strong> <span ng-bind="vm.employee.getEmployee().industry.name"></span></span>\n' +
    '                                                </p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().preferred_positions | hasItem">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p><strong>Preferred Position:</strong> <span ng-repeat="position in vm.employee.getEmployee().preferred_positions"><span ng-bind="position.name"></span><span ng-show="!$last">, </span></span>\n' +
    '                                                </p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().preferred_locations | hasItem">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p><strong>Preferred Location:</strong> <span ng-repeat="location in vm.employee.getEmployee().preferred_locations"><span ng-bind="location.name"></span><span ng-show="!$last">, </span></span>\n' +
    '                                                </p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().allow_to_work_in_sg">\n' +
    '                                            <div class="col s12">\n' +
    '                                                <p>Allow to work in Singapore</p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="work profile-card top-nav white-text col s12">\n' +
    '                                    <div class="card-header">Working Experiences</div>\n' +
    '                                    <div class="card-content white black-text">\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().education_level.name">\n' +
    '                                            <div class="skill col s12">\n' +
    '                                                <p><strong>Education Level:</strong> <span ng-bind="vm.employee.getEmployee().education_level.name"></span></p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().working_experience.name">\n' +
    '                                            <div class="skill col s12">\n' +
    '                                                <p><strong>Experiences:</strong> <span ng-bind="vm.employee.getEmployee().working_experience.name"></span></p>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                        <div class="row" ng-if="vm.employee.getEmployee().last_positions | hasItem">\n' +
    '                                            <div class="last-positions col s12">\n' +
    '                                                <p><strong>Last Positions</strong></p>\n' +
    '                                                <table class="responsive-table bordered">\n' +
    '                                                    <thead>\n' +
    '                                                        <tr>\n' +
    '                                                            <th>Company</th>\n' +
    '                                                            <th>Role</th>\n' +
    '                                                            <th>Name of Manager</th>\n' +
    '                                                            <th>Reason of Leaving</th>\n' +
    '                                                            <th>Start</th>\n' +
    '                                                            <th>End</th>\n' +
    '                                                        </tr>\n' +
    '                                                    </thead>\n' +
    '                                                    <tbody>\n' +
    '                                                        <tr ng-repeat="position in vm.employee.getEmployee().last_positions">\n' +
    '                                                            <td ng-bind="position.company_name"></td>\n' +
    '                                                            <td ng-bind="position.role"></td>\n' +
    '                                                            <td ng-bind="position.name_of_manager"></td>\n' +
    '                                                            <td ng-bind="position.reason_of_leaving"></td>\n' +
    '                                                            <td ng-bind="position.start_date | timeFormat:\'LL\'"></td>\n' +
    '                                                            <td ng-bind="position.end_date | timeFormat:\'LL\'"></td>\n' +
    '                                                        </tr>\n' +
    '                                                    </tbody>\n' +
    '                                                </table>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '\n' +
    '                        </div>\n' +
    '                        <div class="application">\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12 m3 l2">\n' +
    '                                    <p><strong>Request</strong></p>\n' +
    '                                </div>\n' +
    '                                <div class="content col s12 m9 l10" ng-bind-html="vm.application.applications[0].getRequest()"></div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12 m3 l2">\n' +
    '                                    <p><strong>Selected Shifts</strong></p>\n' +
    '                                </div>\n' +
    '                                <div class="content col s12 m9 l10">\n' +
    '                                    <div class="selected-shift" ng-repeat="item in vm.appliedShifts">\n' +
    '                                        <div class="row">\n' +
    '                                            <div class="col s12 m4">\n' +
    '                                                <p><strong ng-bind="item.shift.getDescription()"></strong></p>\n' +
    '                                                <p><strong>Salary:</strong> {{item.shift.getSalary()}}</p>\n' +
    '                                            </div>\n' +
    '                                            <div class="col s12 m8 right-align">\n' +
    '                                                <md-checkbox ng-disabled="!vm.is_owner || vm.job.isCompleted()" ng-model="item.application.accept" aria-label="accept_application">Accept</md-checkbox>\n' +
    '                                                <!-- <md-checkbox ng-disabled="!vm.is_owner" ng-model="item.application.complete" aria-label="accept_application">Completed</md-checkbox> -->\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="right-align" ng-if="vm.is_owner && !vm.job.isCompleted()">\n' +
    '                                        <button class="btn waves-effect waves-light btn-flat" ng-click="vm.acceptSelectedShift(vm.appliedShifts)" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress">Save <i class="material-icons right icon-pending">replay</i></button>\n' +
    '                                        <button class="btn waves-effect waves-light btn-flat" ng-click="vm.markCompleteAcceptedShift(vm.appliedShifts)" ng-class="{\'disabled\': vm.inMarkCompetedProgress}" ng-disabled="vm.inMarkCompetedProgress">Mark as Completed <i class="material-icons right icon-pending">replay</i></button>\n' +
    '                                        <button class="btn waves-effect waves-light btn-flat grey darken-2" ng-click="$state.reload()">Cancel</button>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <br>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/_dashboard_home.tpl.html',
    '<div ng-if="user.isEmployer()" ng-include="\'/frontend/js/components/dashboard/employer/_dashboard_home.tpl.html\'"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/_dashboard_jobs.tpl.html',
    '<div ng-if="user.isEmployer()" ng-include="\'/frontend/js/components/dashboard/employer/_dashboard_jobs.tpl.html\'"></div>\n' +
    '<div ng-if="user.isEmployee()" ng-include="\'/frontend/js/components/dashboard/employee/_dashboard_jobs.tpl.html\'"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/dashboard.tpl.html',
    '<div id="dashboard">\n' +
    '    <div class="container">\n' +
    '        <!-- Employee -->\n' +
    '        <div class="row employee-dashboard" ng-if="vm.user.isEmployee()">\n' +
    '            <div class="col s12 m8">\n' +
    '                <div class="dashboard-navbar">\n' +
    '                    <div class="row">\n' +
    '                        <ul>\n' +
    '                            <li ui-sref-active="active">\n' +
    '                                <a ui-sref="app.dashboard.jobs({status: \'pending\'})">Pending Shifts</a>\n' +
    '                            </li>\n' +
    '                            <li ui-sref-active="active">\n' +
    '                                <a ui-sref="app.dashboard.jobs({status: \'completed\'})">Completed Shifts</a>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row dashboard-content" ui-view="dashboard-content"></div>\n' +
    '            </div>\n' +
    '            <div class="col s12 m4 dashboard-upcomming">\n' +
    '                <div class="dashboard-navbar">\n' +
    '                    <p>Upcomming Shifts</p>\n' +
    '                </div>\n' +
    '                <div class="row" ui-view="dashboard-upcomming"></div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <!-- Employer -->\n' +
    '        <div class="row employer-dashboard" ng-if="vm.user.isEmployer()">\n' +
    '            <div class="col s12 m3" ng-include="\'/frontend/js/components/dashboard/employer/_sidebar.tpl.html\'"></div>\n' +
    '            <div class="col s12 m9">\n' +
    '                <div class="row dashboard-content" ui-view="dashboard-content"></div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/create-job/create.tpl.html',
    '<div id="create-job-page" class="container create-job-page">\n' +
    '    <form ng-submit="vm.CreateJobForm.$valid && vm.create(vm.job)" name="vm.CreateJobForm" novalidate>\n' +
    '        <div class="create-job row">\n' +
    '            <div class="col s12 m8">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Offer</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" name="title" ng-model="vm.job.title" required>\n' +
    '                                <label>Title</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.title.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" name="short_desc" ng-model="vm.job.short_desc" required>\n' +
    '                                <label>Short Desc</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.short_desc.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <span>Description:</span>\n' +
    '                                <trix-editor angular-trix ng-model="vm.job.description" class="trix-content" required name="description"></trix-editor>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.description.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Requirements</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" ng-model="vm.job.note">\n' +
    '                                <label>Note</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" ng-model="vm.job.requirement">\n' +
    '                                <label>Requirement</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <select ng-model="vm.job.type" material-select name="type" required>\n' +
    '                                    <option value="" disabled selected>Choose your option</option>\n' +
    '                                    <option value="parttime">Parttime only</option>\n' +
    '                                    <option value="fulltime">Fulltime only</option>\n' +
    '                                    <option value="parttime & fulltime">Parttime & fulltime</option>\n' +
    '                                </select>\n' +
    '                                <label>What kind of work you are looking for?</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.type.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12 m6">\n' +
    '                                <md-select ng-model="vm.job.parent_category_id" aria-label="Choose a category" name="category" required>\n' +
    '                                    <option value="" disabled selected>Choose a Category</option>\n' +
    '                                    <md-option ng-value="category.getId()" ng-repeat="category in vm.categories| filterBy:[\'level\']:1">{{category.getName()}}</md-option>\n' +
    '                                </md-select>\n' +
    '                                <label class="active">Choose a category</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.category.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="input-field col s12 m6" ng-show="vm.job.parent_category_id">\n' +
    '                                <md-select ng-model="vm.job.category_id" aria-label="Choose a sub-category" name="subCategory" required>\n' +
    '                                    <option value="" disabled selected>Choose your option</option>\n' +
    '                                    <md-option ng-value="category.getId()" ng-repeat="category in vm.categories| filterBy:[\'parent_id\']:vm.job.parent_category_id">{{category.getName()}}</md-option>\n' +
    '                                </md-select>\n' +
    '                                <label class="active">Choose a sub-category</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.subCategory.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '\n' +
    '            <div class="col s12 m4 right-column">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Upload image</div>\n' +
    '                    <div class="card-content">\n' +
    '\n' +
    '\n' +
    '                        <div class="nf-drop-zone" dropzone before-upload="vm.beforeUpload" on-success="vm.onSuccess">\n' +
    '                            <div class="drop-zone-center">\n' +
    '                                <i class="material-icons">system_update_alt</i><span>Chose your image or drag them here to upload</span>\n' +
    '                            </div>\n' +
    '                            <div class="job-image-preview" ng-if="vm.job.image">\n' +
    '                                <img ng-src="{{vm.job.image}}">\n' +
    '                                <div class="edit" ng-if="vm.job.image">\n' +
    '                                    <a ng-click="vm.job.image = undefined">Remove</a>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Map</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="job-map-preview">\n' +
    '                            <p> <strong>Map Preview</strong></p>\n' +
    '                            <geocoding address="vm.job.addresss" default-lat="\'1.352083\'" default-lng="\'103.819836\'" lat="vm.job.latitude" lng="vm.job.longitude"></geocoding>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input ng-model-options="{ debounce: 1000 }" type="text" name="address" ng-model="vm.job.addresss" required>\n' +
    '                                <label>Address</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.address.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s6">\n' +
    '                                <input type="text" ng-model="vm.job.latitude">\n' +
    '                                <label ng-class="{active: vm.job.latitude}">Latitude</label>\n' +
    '                            </div>\n' +
    '                            <div class="input-field col s6">\n' +
    '                                <input type="text" ng-model="vm.job.longitude">\n' +
    '                                <label ng-class="{active: vm.job.longitude}">Longitude</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <div class="create-job">\n' +
    '            <div class="s12 m12">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Shifts</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row shifts">\n' +
    '                            <p><strong>Availabe Shifts</strong></p>\n' +
    '                            <div class="shift" ng-repeat="shift in vm.shifts">\n' +
    '                                <div class="input-field col s12 shift_description">\n' +
    '                                    <input id="shift_description" name="shift_description_{{$index}}" type="text" ng-model="shift.description">\n' +
    '                                    <label for="shift_description">Description</label>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.shift_description_{{$index}}.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col s12">\n' +
    '                                    <md-input-container class="col s4 md-input-has-placeholder">\n' +
    '                                        <label>Start Date/Time</label>\n' +
    '                                        <input mdc-datetime-picker="" name="start_{{$index}}" date="true" time="true" type="text" max-date="shift.end" ng-model="shift.start" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.start_{{$index}}.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <md-input-container class="col s4 md-input-has-placeholder">\n' +
    '                                        <label>End Date/Time</label>\n' +
    '                                        <input mdc-datetime-picker="" name="end_{{$index}}" date="true" time="true" type="text" min-date="shift.start" ng-model="shift.end" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.end_{{$index}}.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <md-input-container class="col s2">\n' +
    '                                        <input type="number" ng-model="shift.salary" name="salary" required ng-pattern="/^[1-9][0-9]*$/" ng-maxlength="7" aria-label="salary">\n' +
    '                                        <label>Salary</label>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.pattern">Please enter a valid value</span>\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.maxlength">The value is too long</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <div class="col s2 delete-btn">\n' +
    '                                        <a class="waves-effect btn-flat" ng-click="vm.removeShift(shift)"><i class="material-icons">delete</i></a>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="add-more-shift-btn">\n' +
    '                                <a class="waves-effect btn-flat" ng-click="vm.addMoreShift()"><i class="material-icons left">add</i>Add more shift</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12 right-align">\n' +
    '                        <button class="btn waves-effect waves-light" ng-disabled="vm.inProgress" ng-class="{\'disabled\': vm.inProgress}" type="submit" name="action">Submit\n' +
    '                            <i class="material-icons right">send</i>\n' +
    '                            <i class="material-icons right icon-pending">replay</i>\n' +
    '                        </button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </form>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/home/home.tpl.html',
    '<h1>This is homepage</h1>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/_messages.tpl.html',
    '<div class="chatbox">\n' +
    '    <div class="row messages-area">\n' +
    '        <div class="messages">\n' +
    '            <div ui-view="old-messages"></div>\n' +
    '            <div class="messages-collection">\n' +
    '                <div ng-repeat="message in vm.conversation.getMessages() | orderBy:\'id\'" ng-if="message.getContent()">\n' +
    '                    <message ng-if="message.getType() == 1" countmessage="countmessage" message="message" job="vm.job" user="vm.user" conversation="vm.conversation" type="normal"></message>\n' +
    '                    <message ng-if="message.getType() == 2" message="message" job="vm.job" user="vm.user" conversation="vm.conversation" type="mark_job_as_completed"></message>\n' +
    '                    <message ng-if="message.getType() == 3" message="message" job="vm.job" user="vm.user" conversation="vm.conversation" type="mark_job_as_incompleted"></message>\n' +
    '                    <message ng-if="message.getType() == 4" message="message" job="vm.job" user="vm.user" conversation="vm.conversation" type="employee_send_application"></message>\n' +
    '                    <message ng-if="message.getType() == 5" message="message" job="vm.job" user="vm.user" conversation="vm.conversation" type="employer_change_application"></message>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <message-input-box class="message-box row">\n' +
    '        <form class="col s12">\n' +
    '            <div class="input-field col s12">\n' +
    '                <i class="material-icons prefix">textsms</i>\n' +
    '                <textarea id="message-textarea" class="materialize-textarea" ng-model="vm.message" ng-keypress="vm.sendKeyPress($event, vm.message)"></textarea>\n' +
    '                <label for="message-textarea">Message</label>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '        <div class="actions col s12 right-align">\n' +
    '            <a class="waves-effect waves-light btn" ng-click="vm.send(vm.message)"><i class="material-icons right">send</i>Send</a>\n' +
    '        </div>\n' +
    '    </message-input-box>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/_old_messages.tpl.html',
    '<!-- <div class="inbox-loadmore center-align">\n' +
    '    <a ng-click="vm.page = vm.page + 1">Load more...</a>\n' +
    '</div>\n' +
    '<div class="">\n' +
    '    <ul class="collection">\n' +
    '        <li class="collection-item avatar" ng-repeat="message in vm.messages | orderBy:\'id\'" ng-if="message">\n' +
    '            <img ng-if="message.getSenderId() == vm.conversation.getReceiver().getId()" ng-src="{{ vm.conversation.getReceiver().getImage() }}" alt="" class="circle">\n' +
    '            <img ng-if="message.getSenderId() == vm.user.getId()" ng-src="{{ vm.user.getImage() }}" alt="" class="circle">\n' +
    '            <div ng-bind-html="message.getContent()"></div>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    ' -->');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/inbox.tpl.html',
    '<inbox id="inbox">\n' +
    '    <div class="row inbox-body">\n' +
    '        <div class="contacts col s12 m4 l3 hide-on-small-only">\n' +
    '            <div class="row">\n' +
    '                <ul class="collection">\n' +
    '                    <li class="collection-item avatar" ng-class="{ \'has-new-message\': conversation.hasNewMessage(user.getId()) }" ng-repeat="conversation in conversations" ui-sref-active="active">\n' +
    '                        <conversation-item type="inbox" conversation="conversation"></conversation-item>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="nav-wrapper hide-on-med-and-up" side-nav>\n' +
    '            <a href="#" data-activates="mobile-demo" class="button-collapse black-text"><i class="material-icons">menu</i> <span>Conversations</span></a>\n' +
    '            <ul class="collection side-nav" id="mobile-demo">\n' +
    '                <li class="collection-item avatar" ng-class="{ \'has-new-message\': conversation.hasNewMessage(user.getId()) }" ng-repeat="conversation in conversations" ui-sref-active="active">\n' +
    '                    <conversation-item type="inbox" conversation="conversation"></conversation-item>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '        <div ui-view="messages" class="col s12 m8 l9"></div>\n' +
    '    </div>\n' +
    '</inbox>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/job/_applications.tpl.html',
    '<div ng-show="vm.applications | hasItem">\n' +
    '    <div class="people white lighten-5" ng-repeat="item in vm.applications">\n' +
    '        <div class="qualified">\n' +
    '            <div class="col xs12 s3 m2 center-align">\n' +
    '                <div class="qualified-avatar">\n' +
    '                    <img class="avatar" ng-src="{{item[0].getImage()}}" alt="ushift">\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '            <div class="col xs12 s6 m7 qualified-information">\n' +
    '                <p class="list-description">\n' +
    '                    <span class="name">\n' +
    '                        <a ui-sref="app.job.application({user_id: item[0].getEmployeeId()})">\n' +
    '                            <strong ng-if="vm.is_owner" ng-bind="item[0].getName()"></strong>\n' +
    '                            <span ng-if="!vm.is_owner"></span>\n' +
    '                        </a>\n' +
    '                    </span>\n' +
    '                </p>\n' +
    '                <div class="text-description" ng-bind-html="item[0].application.request"></div>\n' +
    '                <div class="selected-shift">\n' +
    '                    <strong>Selected Shifts: </strong> <span ng-repeat="shift in item">{{ shift.shift.getDescription()}}<span ng-if="!$last">, </span></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col xs12 s3 m3" class="employee-actions left-align" ng-if="vm.is_owner">\n' +
    '                <a class="waves-effect waves-light btn" style="font-size: 12px;margin-top: 35px;" ng-click="initConversation(item[0].getEmployeeId(), vm.job.getId())"><i class="material-icons left" title="Message to {{item[0].getName()}}">message</i> Send message</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/job/_apply_form.tpl.html',
    '<form class="form-recruitment" name="vm.apply_form" ng-submit="vm.apply_form.$valid && vm.apply(vm.application)" novalidate>\n' +
    '    <div class="selectable-shifts">\n' +
    '        <label>Select your shifts</label>\n' +
    '        <div class="shift" ng-repeat="shift in vm.job.getShifts()">\n' +
    '            <md-checkbox ng-model="shift.selected">\n' +
    '                {{ shift.getDescription() }}, {{ shift.getStartTime() | timeFormat:\'MMMM Do YYYY, hh:mm\' }} - {{ shift.getEndTime() | timeFormat:\'MMMM Do YYYY, hh:mm\' }}\n' +
    '            </md-checkbox>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="application-editor">\n' +
    '        <trix-editor id="application-editor" angular-trix ng-model="vm.application.request" class="trix-content" name="request" required></trix-editor>\n' +
    '        <div class="errors">\n' +
    '            <span ng-show="vm.apply_form.$submitted && vm.apply_form.request.$error.required">Required</span>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <br>\n' +
    '    <div class="right-align">\n' +
    '        <button class="btn center waves-effect waves-light" ng-class="{disabled: vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">Submit\n' +
    '            <i class="material-icons right">send</i>\n' +
    '            <i class="material-icons right icon-pending">replay</i>\n' +
    '        </button>\n' +
    '    </div>\n' +
    '</form>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/job/_job.tpl.html',
    '<div class="box-job-page" ng-show="vm.job">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col s12 m8 l9 card">\n' +
    '                <div class="white job-detail">\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s12 m7 title">\n' +
    '                            <h1 ng-bind="vm.job.title"></h1>\n' +
    '                            <span><small>created on {{vm.job.getCreatedAt() | timeFormat: \'LL\'}}</small></span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="label col s12 m3 l2">\n' +
    '                            <strong>Description</strong>\n' +
    '                        </div>\n' +
    '                        <div class="content col s12 m9 l10">\n' +
    '                            <div class="content" ng-bind-html="vm.job.description"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row" ng-show="vm.job.note">\n' +
    '                        <div class="label col s12 m3 l2">\n' +
    '                            <strong>Note</strong>\n' +
    '                        </div>\n' +
    '                        <div class="content col s12 m9 l10">\n' +
    '                            <div class="title" ng-bind="vm.job.note"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row" ng-show="vm.job.requirement">\n' +
    '                        <div class="label col s12 m3 l2">\n' +
    '                            <strong>Requirement</strong>\n' +
    '                        </div>\n' +
    '                        <div class="content col s12 m9 l10">\n' +
    '                            <div class="title" ng-bind="vm.job.requirement"></div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row available">\n' +
    '                        <div class="col s12">\n' +
    '                            <div class="shifts row">\n' +
    '                                <div class="col s6 l3" ng-repeat="shift in vm.job.getShifts()">\n' +
    '                                    <div class="card">\n' +
    '                                        <div class="card-title">\n' +
    '                                            <p class="left" ng-bind="shift.description"></p>\n' +
    '                                            <md-checkbox class="right" ng-show="user.isEmployee() && !vm.job.isCompleted()" ng-model="shift.selected" aria-label="shift"></md-checkbox>\n' +
    '                                        </div>\n' +
    '                                        <div class="card-content" ng-click="vm.showApply = true">\n' +
    '                                            <p><strong>Salary: </strong>$<span ng-bind="shift.getSalary()"></span></p>\n' +
    '                                            <p><strong>From:</strong> <span ng-bind="shift.getStartTime() | timeFormat:\'MMMM Do YYYY, hh:mm\'"></span></p>\n' +
    '                                            <p><strong>To:</strong> <span ng-bind="shift.getEndTime() | timeFormat:\'MMMM Do YYYY, hh:mm\'"></span></p>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col s12 bid-project" ng-show="user.isEmployee() && !vm.job.isCompleted()">\n' +
    '                        <div class="row">\n' +
    '                            <p class="right-align">\n' +
    '                                <a href="#" class="btn center waves-effect waves-light button-bid-project" ng-click="vm.showApply = !vm.showApply">{{vm.showApply ? \'Hide\' : \'Send your application\'}}</a>\n' +
    '                            </p>\n' +
    '                        </div>\n' +
    '                        <div class="row animate-collapse apply-form" ng-show="vm.showApply" ng-include="\'/frontend/js/components/job/_apply_form.tpl.html\'"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="white box-job-detail bid-box" ui-view="applications">\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="hide-on-small-only col m4 l3 sidebar-job">\n' +
    '                <div class="space-bottom">\n' +
    '                    <p class="title center-align"><strong>The blue restaurant</strong></p>\n' +
    '                    <div class="employer-avatar center-align">\n' +
    '                        <img class="logo-shop" src="../assets/images/jobs/office.jpg" alt="logo shop">\n' +
    '                    </div>\n' +
    '                    <div class="about-us" ng-show="vm.job.company_about">\n' +
    '                        <p class="title">About us...</p>\n' +
    '                        <p class="content" ng-bind="vm.job.company_about"></p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '<!--                <div class="space-bottom" ng-if="vm.job.getLatitude() !== undefined && vm.job.getLongitude() !== undefined">\n' +
    '                    <div class="map">\n' +
    '                        <p class="title">Address</p>\n' +
    '                        <div class="light-blue-text" ng-bind="vm.job.getAddress()"></div>\n' +
    '                        <div class="map-box">\n' +
    '                            <map lat="vm.job.getLatitude()" lng="vm.job.getLongitude()"></map>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                -->\n' +
    '                <div class="space-bottom" ng-if="vm.job.getLatitude() !== undefined && vm.job.getLongitude() !== undefined">\n' +
    '                    <div class="map">\n' +
    '                        <p class="title">Address</p>\n' +
    '                        <div class="light-blue-text" ng-bind="vm.job.getAddress()"></div>\n' +
    '                        <div class="map-box">\n' +
    '                            <img src="../assets/images/jobs/map_rivervalley.jpg"/>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="space-bottom" ng-if="vm.job.getCategories() | hasItem">\n' +
    '                    <div class="tag-category">\n' +
    '                        <p class="title">Categories</p>\n' +
    '                        <p class="list-cate">\n' +
    '                            <a class="waves-effect waves-light btn signup blue-grey lighten-1" ng-repeat="category in vm.job.getCategories()" ng-bind="category.name" ng-click="vm.gotoSearchPage(category)"></a>\n' +
    '                        </p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row card" ng-if="user.isEmployer()">\n' +
    '            <div class="white box-job-detail related-box">\n' +
    '<!--                <div class="col s12 title">\n' +
    '                    <h4>Other positions nearby...</h4>\n' +
    '                </div>-->\n' +
    '                <div class="related-list">\n' +
    '                    <div class="navbar-title grey darken-3">\n' +
    '                        <div class="col s12">\n' +
    '                            <div class="col s8 m9 qualified">\n' +
    '                                <p class="white-text"><strong>Other positions nearby...</strong></p>\n' +
    '                            </div>\n' +
    '                            \n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="job white lighten-5" ng-repeat="item in vm.jobs">\n' +
    '                        <job item="item"></job>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/job/_job_item.tpl.html',
    '<div class="col s12" style="margin-top: 10px;">\n' +
    '    <div class="col s12 m3 l2 img-thumbnail center-align">\n' +
    '        <img ng-src="{{item.image}}">\n' +
    '        <label>The Blue restaurant</label>\n' +
    '    </div>\n' +
    '    <div class="col s12 m9 l10 content">\n' +
    '        <div class="row">\n' +
    '            <div class="col s12">\n' +
    '                <a ui-sref="app.job({slug: item.getSlug()})">\n' +
    '                    <h5 class="title">{{item.title}} <i ng-if="item.isCompleted()" class="material-icons">check_circle</i></h5>\n' +
    '                </a>\n' +
    '                <div class="short-description" ng-if="item.getShortDesc() != \'\'" ng-bind-html="item.getShortDesc()"></div>\n' +
    '                <div class="list-description">\n' +
    '                    <small class="online"><strong>Update:</strong> <span>{{item.timestamps.updated_at.date| timeFormat:\'LLLL\'}}</span></small>\n' +
    '                </div>\n' +
    '\n' +
    '                <br/>\n' +
    '\n' +
    '                <div class="time-recr" ng-if="action != \'manage\' && item.getShifts()|hasItem">\n' +
    '                    <div class="col s12 m4" ng-repeat="shift in item.getShifts()">\n' +
    '                        <div class="shift" style="border-color: rgba(196, 78, 56, 0.6)">\n' +
    '                            <div>\n' +
    '                                <div class="col s12 m12 shift-title center">\n' +
    '                                    <p ng-bind="shift.getDescription()" style="font-size: 16px;margin:6px 0px;"></p>\n' +
    '                                    <span class="shift-status-icon" ng-if="shift.isTaken()">\n' +
    '                                        <i class="material-icons">done</i>\n' +
    '                                    </span>\n' +
    '                                    <span class="shift-status-icon" ng-if="!shift.isTaken() && shift.users|hasItem">\n' +
    '                                        <i class="material-icons text-black">supervisor_account</i>({{ shift.users | length }})\n' +
    '                                    </span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div>\n' +
    '                                <div class="col s6 m6  center middle" style="font-size: 24px;color: #C44E38">\n' +
    '                                    <p style="margin: 6px 0px;">\n' +
    '                                        {{shift.getDuration()}}3<span style=\'font-size:12px\'>h</span></p>\n' +
    '                                </div>\n' +
    '                                <div class="col s6 m6 center middle" style="font-size: 24px;color: #C44E38">\n' +
    '                                    <p style="margin: 5px 0px;">{{shift.getSalary()}}<span style=\'font-size:12px\'>$/h</span></p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div>\n' +
    '                                <div class="col s12 m12 center">\n' +
    '                                    <p style="font-size: 14px;margin:6px 0px;">Tanjong pagar</p>\n' +
    '                                </div>\n' +
    '\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                </button>\n' +
    '                <div ng-if="action == \'manage\'" class="job-manage-actions">\n' +
    '                    <button ng-if="!item.isCompleted()" class="btn waves-effect waves-light" ng-click="complete(item.getId())" ng-disabled="inMarkCompletedProgress" ng-class="{\'disabled\': inMarkCompletedProgress}">\n' +
    '                        <i class="material-icons left">done</i>\n' +
    '                        <i class="material-icons left icon-pending">replay</i> Mark as completed\n' +
    '                    </button>\n' +
    '                    <a ui-sref="app.update-job({slug: item.getSlug()})">\n' +
    '                        <button class="btn waves-effect waves-light">\n' +
    '                            <i class="material-icons left">edit</i>\n' +
    '                            <i class="material-icons left icon-pending">replay</i> Edit\n' +
    '                        </button>\n' +
    '                    </a>\n' +
    '                    <button class="btn waves-effect waves-light" ng-click="delete(item.getId())" ng-disabled="inProgress" ng-class="{\'disabled\': inProgress}">\n' +
    '                        <i class="material-icons left">delete</i>\n' +
    '                        <i class="material-icons left icon-pending">replay</i> Delete\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/password/forgot.tpl.html',
    '<div id="forgot-password" update-text-fields>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12 m6 l4 offset-m3 offset-l4">\n' +
    '            <div class="row">\n' +
    '                <div class="logo-image center-align">\n' +
    '                    <a ng-href="{{ asset(\'/\') }}" force-reload>\n' +
    '                        <img ng-src="{{asset(\'/assets/images/logo.png\')}}" alt="Ushift logo">\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="forgot-password-form">\n' +
    '                <div class="row">\n' +
    '                    <form class="col s12" name="vm.forgotPasswordForm" ng-submit="vm.forgotPasswordForm.$valid && vm.forgot(email)" novalidate>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input id="email" type="email" class="validate" ng-model="email" name="email" required ng-maxlength="40" ng-pattern=\'/^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$/\'>\n' +
    '                                <label for="email">Email</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <p class="red-text" ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.required">Required</p>\n' +
    '                                    <p class="red-text" ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.pattern">Please enter a valid email address</p>\n' +
    '                                    <p class="red-text" ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.maxlength">The value is too long</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="right-align">\n' +
    '                                <a ui-sref="user.register">Create new account</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row action-btn">\n' +
    '                            <button class="btn waves-effect waves-light" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">Forgot Password\n' +
    '                                <i class="material-icons right">trending_flat</i>\n' +
    '                                <i class="material-icons right icon-pending">replay</i>\n' +
    '                            </button>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/password/reset.tpl.html',
    '<div id="reset-password" update-text-fields>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12 m6 l4 offset-m3 offset-l4">\n' +
    '            <div class="row">\n' +
    '                <div class="logo-image center-align">\n' +
    '                    <a ng-href="{{ asset(\'/\') }}" force-reload>\n' +
    '                        <img ng-src="{{asset(\'/assets/images/logo.png\')}}" alt="Ushift logo">\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="reset-password-form">\n' +
    '                <div class="row">\n' +
    '                    <form class="col s12" ng-submit="vm.resetPasswordForm.$valid && vm.reset(password, password_confirmation)" name="vm.resetPasswordForm" novalidate>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input id="password" type="password" class="validate" ng-model="password" name="password" required ng-minlength="4" ng-maxlength="255">\n' +
    '                                <label for="password">Password</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password.$error.required">Required</p>\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password.$error.minlength">The value is too short</p>\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password.$error.maxlength">The value is too long</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input id="password_confirmation" type="password" class="validate" ng-model="password_confirmation" name="password_confirmation" required ng-maxlength="255" data-password-verify="password">\n' +
    '                                <label for="password_confirmation">Password Confirmation</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password.$error.required">Required</p>\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password.$error.maxlength">The value is too long</p>\n' +
    '                                    <p class="red-text" ng-show="vm.resetPasswordForm.$submitted && vm.resetPasswordForm.password_confirmation.$error.passwordVerify">Password confirmation doesn\'t match</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="right-align">\n' +
    '                                <a ui-sref="user.login">Login</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row action-btn">\n' +
    '                            <button class="btn waves-effect waves-light" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">Reset Password\n' +
    '                                <i class="material-icons right">trending_flat</i>\n' +
    '                                <i class="material-icons right icon-pending">replay</i>\n' +
    '                            </button>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/login/login.tpl.html',
    '<div class="fixed-header" ng-include="\'/frontend/js/common/layouts/partials/_header.tpl.html\'"></div>\n' +
    '<div id="login" update-text-fields>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12 m6 l4 offset-m3 offset-l4">\n' +
    '            <div class="login-form">\n' +
    '                <div class="row">\n' +
    '                    <h1 class="center-align">Log in</h1>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <form class="col s12" name="vm.loginForm" ng-submit="vm.loginForm.$valid && vm.login(vm.user)" novalidate>\n' +
    '                        <div class="row">\n' +
    '                            <md-input-container class="col s12">\n' +
    '                                <label class="required" for="email">Email</label>\n' +
    '                                <input id="email" type="email" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </md-input-container>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <md-input-container class="col s12">\n' +
    '                                <label class="required" for="email">Password</label>\n' +
    '                                <input id="email" type="password" class="validate" ng-model="vm.user.password" name="password" required ng-maxlength="40">\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.password.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.password.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </md-input-container>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <ul class="form-links">\n' +
    '                                <li class="form-links-item left">\n' +
    '                                    <a ui-sref="user.register">Create new account?</a>\n' +
    '                                </li>\n' +
    '                                <li class="form-links-item right">\n' +
    '                                    <a ui-sref="user.password.forgot">Can\'t log in?</a>\n' +
    '                                </li>\n' +
    '                            </ul>\n' +
    '                        </div>\n' +
    '                        <div class="row login-btn">\n' +
    '                            <button class="btn waves-effect waves-light" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">Login\n' +
    '                                <i class="material-icons right">trending_flat</i>\n' +
    '                                <i class="material-icons right icon-pending">replay</i>\n' +
    '                            </button>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/_employee_info.tpl.html',
    '<div class="row">\n' +
    '    <div class="work profile-card top-nav white-text col s12">\n' +
    '        <div class="card-header">Interested In</div>\n' +
    '        <div class="card-content white black-text">\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().kind_of_work">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Preferred Work Type:</strong> <span ng-bind="vm.profile.getEmployee().kind_of_work"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().hours_per_week">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>I can work for:</strong> <span ng-bind="vm.profile.getEmployee().hours_per_week"></span> hours/week</p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().industry.id">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Industry:</strong> <span ng-bind="vm.profile.getEmployee().industry.name"></span></span>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().preferred_positions | hasItem">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Preferred Position:</strong> <span ng-repeat="position in vm.profile.getEmployee().preferred_positions"><span ng-bind="position.name"></span><span ng-show="!$last">, </span></span>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().preferred_locations | hasItem">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Preferred Location:</strong> <span ng-repeat="location in vm.profile.getEmployee().preferred_locations"><span ng-bind="location.name"></span><span ng-show="!$last">, </span></span>\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().allow_to_work_in_sg">\n' +
    '                <div class="col s12">\n' +
    '                    <p>Allow to work in Singapore</p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div class="row">\n' +
    '    <div class="work profile-card top-nav white-text col s12">\n' +
    '        <div class="card-header">Working Experiences</div>\n' +
    '        <div class="card-content white black-text">\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().education_level.name">\n' +
    '                <div class="skill col s12">\n' +
    '                    <p><strong>Education Level:</strong> <span ng-bind="vm.profile.getEmployee().education_level.name"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().working_experience.name">\n' +
    '                <div class="skill col s12">\n' +
    '                    <p><strong>Experiences:</strong> <span ng-bind="vm.profile.getEmployee().working_experience.name"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getEmployee().last_positions | hasItem">\n' +
    '                <div class="last-positions col s12">\n' +
    '                    <p><strong>Last Positions</strong></p>\n' +
    '                    <table class="responsive-table striped bordered">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Company</th>\n' +
    '                                <th>Type</th>\n' +
    '                                <th>Country</th>\n' +
    '                                <th>Role</th>\n' +
    '                                <th>Name of Manager</th>\n' +
    '                                <th>Reason of Leaving</th>\n' +
    '                                <th>Start</th>\n' +
    '                                <th>End</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="position in vm.profile.getEmployee().last_positions">\n' +
    '                                <td ng-bind="position.company_name"></td>\n' +
    '                                <td ng-bind="position.type"></td>\n' +
    '                                <td ng-bind="position.country"></td>\n' +
    '                                <td ng-bind="position.role"></td>\n' +
    '                                <td ng-bind="position.name_of_manager"></td>\n' +
    '                                <td ng-bind="position.reason_of_leaving"></td>\n' +
    '                                <td ng-bind="position.start_date | timeFormat:\'LL\'"></td>\n' +
    '                                <td ng-bind="position.end_date | timeFormat:\'LL\'"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/_employee_sidebar.tpl.html',
    '<div class="previous-shifts profile-card top-nav white-text col s12">\n' +
    '    <div class="card-header">Previous Works</div>\n' +
    '    <div class="card-content white black-text">\n' +
    '        <ul class="collection">\n' +
    '            <li class="collection-item avatar" ng-repeat="shift in vm.shifts">\n' +
    '                <img ng-src="{{shift.job.image}}" alt="" class="circle">\n' +
    '                <span class="title" ng-bind="shift.description"></span>\n' +
    '                <div class="shift-info">\n' +
    '                    <p><strong ng-bind="shift.job.title"></strong></p>\n' +
    '                    <p>\n' +
    '                        <strong>From:</strong> {{ shift.getStartTime() | timeFormat:\'MMMM Do YYYY, hh:mm\'}}\n' +
    '                    </p>\n' +
    '                    <p>\n' +
    '                        <strong>To:</strong> {{ shift.getEndTime() | timeFormat:\'MMMM Do YYYY, hh:mm\' }}\n' +
    '                    </p>\n' +
    '                </div>\n' +
    '            </li>\n' +
    '        </ul>\n' +
    '        <simple-pagination page="vm.page"></simple-pagination>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/_employer_info.tpl.html',
    '<div class="row">\n' +
    '    <div class="work profile-card top-nav white-text col s12">\n' +
    '        <div class="card-header">Company</div>\n' +
    '        <div class="card-content white black-text">\n' +
    '            <div class="row" ng-if="vm.profile.getCompanyName()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Name:</strong> <span ng-bind="vm.profile.getCompanyName()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getCompanyAbout()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>About:</strong> <span ng-bind="vm.profile.getCompanyAbout()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getCompanyAddress()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Address:</strong> <span ng-bind="vm.profile.getCompanyAddress()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getRegistedNumber()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Registed Number:</strong> <span ng-bind="vm.profile.getRegistedNumber()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getCompanyMainActivity()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Main Activity:</strong> <span ng-bind="vm.profile.getCompanyMainActivity()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getNumberOfOutlets()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Number Of Outlets:</strong> <span ng-bind="vm.profile.getNumberOfOutlets()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getPreferredContactMethod()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Preferred Contact Method:</strong> <span ng-bind="vm.profile.getPreferredContactMethod()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row" ng-if="vm.profile.getPreferredContactTime()">\n' +
    '                <div class="col s12">\n' +
    '                    <p><strong>Preferred Contact Time:</strong> <span ng-bind="vm.profile.getPreferredContactTime()"></span></p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/_employer_sidebar.tpl.html',
    '<div class="previous-shifts profile-card top-nav white-text col s12">\n' +
    '    <div class="card-header">Completed Jobs</div>\n' +
    '    <div class="card-content white black-text">\n' +
    '        <div class="list-jobs">\n' +
    '            <div class="job" ng-repeat="item in vm.jobs">\n' +
    '                <job item="item"></job>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <simple-pagination page="vm.page"></simple-pagination>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/profile.tpl.html',
    '<div class="profile">\n' +
    '    <div class="profile-cover" style="height: 200px;background-image: url(\'{{vm.profile.getCover()}}\')">\n' +
    '        <div class="container" style="min-height: 200px;">\n' +
    '            <div class="user-infomation" style="height: 120px;\n' +
    '    overflow: auto;\n' +
    '    position: absolute;\n' +
    '    top: 50px;\n' +
    '    width: 100%;">\n' +
    '                <div class="avatar">\n' +
    '                    <user-profile-image class="left" image="vm.profile.getImage()"></user-profile-image>\n' +
    '                    <h5 class="left username" ng-bind="vm.profile.getUsername()"></h5>\n' +
    '                </div>\n' +
    '                <div class="profile-button">\n' +
    '                    <a ng-if="user !== undefined && vm.profile.getId() === user.getId()" ui-sref="app.profile.update.general" class="edit-profile waves-effect waves-light btn">Edit Profile</a>\n' +
    '                    <a ng-if="vm.profile.isEmployee() && user.isEmployer()" class="send-message waves-effect waves-light btn grey darken-4" ng-click="initConversation(vm.profile.getId())">Send Message</a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="box-gray">\n' +
    '        <div class="container profile-container">\n' +
    '            <div class="row">\n' +
    '                <div class="col m7 s12">\n' +
    '                    <div class="row">\n' +
    '                        <div class="general-information profile-card top-nav white-text col s12">\n' +
    '                            <div class="card-header">General information</div>\n' +
    '                            <div class="card-content white black-text">\n' +
    '                                <div class="row" ng-if="vm.profile.getFirstName()">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <p><strong>First Name:</strong> <span ng-bind="vm.profile.getFirstName()"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row" ng-if="vm.profile.getLastName()">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <p><strong>First Name:</strong> <span ng-bind="vm.profile.getLastName()"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row" ng-if="vm.profile.getGender()">\n' +
    '                                    <div class="gender col s12">\n' +
    '                                        <p><strong>Gender:</strong> <span ng-bind="vm.profile.getGender()"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="birthday col s12" ng-if="vm.profile.getBirth()">\n' +
    '                                        <p><strong>Birthday:</strong> <span ng-bind="vm.profile.getBirth() | timeFormat: \'LL\'"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="address col s12" ng-if="vm.profile.getAddress() || vm.profile.getCity() || vm.profile.getCountry()">\n' +
    '                                        <p>\n' +
    '                                            <strong>Address: </strong>\n' +
    '                                            <span ng-bind="vm.profile.getAddress()"></span><span ng-if="vm.profile.getAddress()">, </span>\n' +
    '                                            <span ng-bind="vm.profile.getCity()"></span><span ng-if="vm.profile.getCity()">, </span>\n' +
    '                                            <span ng-bind="vm.profile.getCountry()"></span>\n' +
    '                                        </p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="summary col s12" ng-if="vm.profile.summary">\n' +
    '                                        <p><strong>Summary</strong></p>\n' +
    '                                        <p class="value" ng-bind="vm.profile.summary"></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="contact profile-card top-nav white-text col s12">\n' +
    '                            <div class="card-header">Contact</div>\n' +
    '                            <div class="card-content white black-text">\n' +
    '                                <div class="row" ng-if="vm.profile.mobile">\n' +
    '                                    <div class="mobile col s12">\n' +
    '                                        <p><strong>Mobile:</strong> <span ng-bind="vm.profile.mobile"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="email col s12">\n' +
    '                                        <p><strong>Email:</strong> <span ng-bind="vm.profile.email"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div ng-if="vm.profile.isEmployee()" ng-include="\'/frontend/js/components/profile/_employee_info.tpl.html\'"></div>\n' +
    '                    <div ng-if="vm.profile.isEmployer()" ng-include="\'/frontend/js/components/profile/_employer_info.tpl.html\'"></div>\n' +
    '                </div>\n' +
    '                <div class="col m5 s12">\n' +
    '                    <div class="row">\n' +
    '                        <div ng-if="vm.profile.isEmployee()" ng-include="\'/frontend/js/components/profile/_employee_sidebar.tpl.html\'"></div>\n' +
    '                        <div ng-if="vm.profile.isEmployer()" ng-include="\'/frontend/js/components/profile/_employer_sidebar.tpl.html\'"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/register/register.tpl.html',
    '<div class="fixed-header" ng-include="\'/frontend/js/common/layouts/partials/_header.tpl.html\'"></div>\n' +
    '<div id="register">\n' +
    '    <div id="choose-user-type" ng-if="!vm.role">\n' +
    '        <div class="row">\n' +
    '            <h1 class="center-align">Let\'s get started!</h1>\n' +
    '            <p class="center-align">First, tell us what you\'re looking for.</p>\n' +
    '        </div>\n' +
    '        <div class="container">\n' +
    '            <div class="row">\n' +
    '                <div class="col s12 m6">\n' +
    '                    <!--<p class="center-align">I want to hire a freelancer</p>-->\n' +
    '                    <div class="center-align">\n' +
    '                        <button ng-click="vm.role = \'employer\'" class="btn waves-effect waves-light" type="submit" name="action">I want to hire a freelancer</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col s12 m6">\n' +
    '                    <!--<p class="center-align">I\'m looking for a job</p>-->\n' +
    '                    <div class="center-align">\n' +
    '                        <button ng-click="vm.role = \'employee\'" class="btn waves-effect waves-light" type="submit" name="action">I\'m looking for a job</button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div id="register-form" ng-if="vm.role">\n' +
    '        <div class="row">\n' +
    '            <div class="col s12 m6 offset-m3 l4 offset-l4">\n' +
    '                <form name="vm.registerForm" ng-submit="vm.registerForm.$valid && vm.register(vm.user)" update-text-fields novalidate>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label class="required">First Name</label>\n' +
    '                            <input id="first-name" type="text" ng-model="vm.user.first_name" name="first_name" required ng-maxlength="20">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.first_name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.first_name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label class="required">Last Name</label>\n' +
    '                            <input id="last-name" type="text" ng-model="vm.user.last_name" name="last_name" required ng-maxlength="20">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.last_name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.last_name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12">\n' +
    '                            <label class="required" for="email">Email</label>\n' +
    '                            <input id="email" type="email" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.email.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.email.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12">\n' +
    '                            <label class="required" for="email">Password</label>\n' +
    '                            <input id="email" type="password" class="validate" ng-model="vm.user.password" name="password" required ng-maxlength="40">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.password.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.registerForm.$submitted && vm.registerForm.password.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <ul class="form-links">\n' +
    '                            <li class="form-links-item left">\n' +
    '                                <a ui-sref="user.register" ng-click="vm.role = undefined">Back</a>\n' +
    '                            </li>\n' +
    '                            <li class="form-links-item right">\n' +
    '                                <a ui-sref="user.login">Already have account</a>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                    </div>\n' +
    '                    <div class="row action-btn">\n' +
    '                        <button class="btn waves-effect waves-light" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">register\n' +
    '                            <i class="material-icons right">trending_flat</i>\n' +
    '                            <i class="material-icons right icon-pending">replay</i>\n' +
    '                        </button>\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/search/_sidebar.tpl.html',
    '<div id="er-dashboard-sidebar">\n' +
    '    <ul class="er-sidebar collapsible" data-collapsible="expandable">\n' +
    '        <li class="er-sidebar-item" ng-repeat="item in employerSidebarItems" ng-class="{\'has-submenu\': hasSubmenu(item)}" ui-sref-active="er-navbar-item-active">\n' +
    '            <a ng-if="item.state" ui-sref="{{item.state}}">\n' +
    '                <div class="icon">\n' +
    '                    <i class="material-icons">{{ item.icon }}</i>\n' +
    '                </div>\n' +
    '                <div class="menu">\n' +
    '                    {{item.menu}}\n' +
    '                </div>\n' +
    '            </a>\n' +
    '            <a ng-if="!item.state" class="collapsible-header active">\n' +
    '                <div class="icon">\n' +
    '                    <i class="material-icons">{{ item.icon }}</i>\n' +
    '                </div>\n' +
    '                <div class="menu">\n' +
    '                    {{item.menu}}\n' +
    '                </div>\n' +
    '            </a>\n' +
    '            <div class="submenu collapsible-body" ng-show="item.submenu | hasItem">\n' +
    '                <ul>\n' +
    '                    <li ng-repeat="submenu in item.submenu" ui-sref-active="er-navbar-item-active">\n' +
    '                        <a ui-sref="{{submenu.state}}({{submenu.stateOption | json}})" ui-sref-opts="">{{submenu.menu}}</a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/search/search-jobs.tpl.html',
    '<div class="white card list-jobs">\n' +
    '    <div class="navbar-title grey darken-3">\n' +
    '<!--        <div class="col s12 qualified">\n' +
    '            <p class="white-text"><strong>JOBS</strong></p>\n' +
    '        </div>-->\n' +
    '    </div>\n' +
    '    <preloader ng-if="vm.jobs | isUndefined"></preloader>\n' +
    '    <div class="job white lighten-5" ng-repeat="item in vm.jobs">\n' +
    '        <job item="item"></job>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/search/search-sidebar.tpl.html',
    '<div class="container">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12 m4 l3 sub-category hide-on-small-only" ng-include="\'/frontend/js/components/search/_sidebar.tpl.html\'">\n' +
    '            <!--            <div ng-repeat="parent_category in parent_categories">\n' +
    '                            <a ui-sref="app.search.sidebar.parent_category({parent_category_slug: parent_category.getSlug(), page: undefined})">\n' +
    '                                <p class="title"><strong ng-bind="parent_category.getName()"></strong></p>\n' +
    '                            </a>\n' +
    '                            <ul>\n' +
    '                                <li ng-repeat="item in categories | filterBy:[\'parent_id\']:parent_category.getId()" ng-class="{active: $state.params.child_category_slug == item.getSlug()}">\n' +
    '                                    <a ui-sref="app.search.sidebar.parent_category.child_category({parent_category_slug: parent_category.getSlug(), child_category_slug: item.getSlug(), page: undefined})" ng-bind="item.getName()"></a>\n' +
    '                                </li>\n' +
    '                            </ul>\n' +
    '                        </div>-->\n' +
    '            \n' +
    '        </div>\n' +
    '        <div class="col s12 m8 l9 list-jobs">\n' +
    '            <div ui-view="jobs"></div>\n' +
    '        </div>\n' +
    '        <div class="col m12 l10 offset-l2 pagination">\n' +
    '            <pagination data="pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/search/search.tpl.html',
    '<div id="search">\n' +
    '    <nav class="white search-categories">\n' +
    '        <div class="container">\n' +
    '            <div class="nav-wrapper" side-nav>\n' +
    '                <a href="#" data-activates="mobile-demo" class="button-collapse black-text"><i class="material-icons">menu</i></a>\n' +
    '<!--                <ul class="left hide-on-med-and-down">\n' +
    '                                        <li ng-class="{active: $state.current.name == \'app.search.sidebar.all\'}">\n' +
    '                                            <a ui-sref="app.search.sidebar.all({page: undefined})">All</a>\n' +
    '                                        </li>\n' +
    '                    <li ng-repeat="item in categories| filterBy:[\'level\']:1" ng-class="{active: $state.params.parent_category_slug == item.getSlug()}">\n' +
    '                        <a ui-sref="app.search.sidebar.parent_category({parent_category_slug: item.getSlug(), page: undefined})" ng-bind="item.getName()"></a>\n' +
    '                    </li>\n' +
    '\n' +
    '                </ul>-->\n' +
    '\n' +
    '                <md-toolbar>\n' +
    '                    <md-menu-bar>\n' +
    '                        \n' +
    '<!--                    <li ng-class="{active: $state.current.name == \'app.search.sidebar.all\'}">\n' +
    '                                            <a ui-sref="app.search.sidebar.all({page: undefined})">All</a>\n' +
    '                                        </li>-->\n' +
    '                        \n' +
    '                        \n' +
    '                            <!-- Trigger element is a md-button with an icon -->\n' +
    '                            <button  ui-sref="app.search.sidebar.all({page: undefined})" aria-label="All jobs">\n' +
    '                                <span ng-class="{active: $state.current.name == \'app.search.sidebar.all\'}" >All</span>\n' +
    '                            </button>\n' +
    '<!--                            <md-menu-content width="6" >\n' +
    '                   \n' +
    '                                <md-menu-item ng-repeat="item in categories | filterBy:[\'parent_id\']:parent_category.getId()">\n' +
    '                                    <md-button ui-sref="app.search.sidebar.all({page: undefined})">All</md-button>\n' +
    '                                </md-menu-item>\n' +
    '\n' +
    '                            </md-menu-content>-->\n' +
    '                        \n' +
    '                        <md-menu ng-repeat="parent_category in parent_categories">\n' +
    '                            <!-- Trigger element is a md-button with an icon -->\n' +
    '                            <button  ng-click="$mdOpenMenu()"  aria-label="Open sample menu">\n' +
    '                                <span ng-class="capitalize" ng-bind="parent_category.getName()"></span>\n' +
    '                            </button>\n' +
    '                            <md-menu-content width="6" >\n' +
    '                   \n' +
    '                                <md-menu-item ng-repeat="item in categories | filterBy:[\'parent_id\']:parent_category.getId()">\n' +
    '                                    <md-button ui-sref="app.search.sidebar.parent_category.child_category({parent_category_slug: parent_category.getSlug(), child_category_slug: item.getSlug(), page: undefined})" ng-bind="item.getName()"></md-button>\n' +
    '                                </md-menu-item>\n' +
    '\n' +
    '                            </md-menu-content>\n' +
    '                        </md-menu>\n' +
    '                    </md-menu-bar>\n' +
    '                </md-toolbar> \n' +
    '\n' +
    '\n' +
    '                <ul class="side-nav" id="mobile-demo">\n' +
    '                    <li ng-class="{active: $state.current.name == \'app.search\'}">\n' +
    '                        <a ui-sref="app.search({page: undefined})">All</a>\n' +
    '                    </li>\n' +
    '                    <li ng-repeat="item in categories| filterBy:[\'level\']:1" ng-class="{active: $state.params.parent_category_slug == item.getSlug()}">\n' +
    '                        <a ui-sref="app.search.sidebar.parent_category({parent_category_slug: item.getSlug(), page: undefined})" ng-bind="item.getName()"></a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </nav>\n' +
    '    <div ui-view="search-content"></div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/update-job/update.tpl.html',
    '<div id="create-job-page" class="container create-job-page">\n' +
    '    <form ng-submit="vm.CreateJobForm.$valid && vm.update(vm.job)" name="vm.CreateJobForm" novalidate>\n' +
    '        <div class="create-job row">\n' +
    '            <div class="col s12 m8">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Offer</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" name="title" ng-model="vm.job.title" required>\n' +
    '                                <label ng-class="{\'active\': vm.job.title}">Title</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.title.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" name="short_desc" ng-model="vm.job.short_desc" required>\n' +
    '                                <label ng-class="{\'active\': vm.job.short_desc}">Short Desc</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.short_desc.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <span>Description:</span>\n' +
    '                                <trix-editor angular-trix ng-model="vm.job.description" class="trix-content" required name="description"></trix-editor>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.description.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Requirements</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" ng-model="vm.job.note">\n' +
    '                                <label ng-class="{\'active\': vm.job.note}">Note</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input type="text" ng-model="vm.job.requirement">\n' +
    '                                <label ng-class="{\'active\': vm.job.requirement}">Requirement</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <select ng-model="vm.job.type" material-select name="type" required>\n' +
    '                                    <option value="" disabled selected>Choose your option</option>\n' +
    '                                    <option value="parttime">Parttime only</option>\n' +
    '                                    <option value="fulltime">Fulltime only</option>\n' +
    '                                    <option value="parttime & fulltime">Parttime & fulltime</option>\n' +
    '                                </select>\n' +
    '                                <label>What kind of work you are looking for?</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.type.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12 m6">\n' +
    '                                <md-select ng-model="vm.job.parent_category_id" aria-label="Choose a category" name="category" required>\n' +
    '                                    <option value="" disabled selected>Choose a Category</option>\n' +
    '                                    <md-option ng-value="category.getId()" ng-repeat="category in vm.categories| filterBy:[\'level\']:1">{{category.getName()}}</md-option>\n' +
    '                                </md-select>\n' +
    '                                <label class="active">Choose a category</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.category.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="input-field col s12 m6" ng-show="vm.job.parent_category_id">\n' +
    '                                <md-select ng-model="vm.job.category_id" aria-label="Choose a sub-category" name="subCategory" required>\n' +
    '                                    <option value="" disabled selected>Choose your option</option>\n' +
    '                                    <md-option ng-value="category.getId()" ng-repeat="category in vm.categories| filterBy:[\'parent_id\']:vm.job.parent_category_id">{{category.getName()}}</md-option>\n' +
    '                                </md-select>\n' +
    '                                <label class="active">Choose a sub-category</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.subCategory.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '\n' +
    '            <div class="col s12 m4 right-column">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Upload image</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="nf-drop-zone" dropzone before-upload="vm.beforeUpload" on-success="vm.onSuccess">\n' +
    '                            <div class="drop-zone-center">\n' +
    '                                <i class="material-icons">system_update_alt</i><span>Chose your image or drag them here to upload</span>\n' +
    '                            </div>\n' +
    '                            <div class="job-image-preview" ng-if="vm.job.image">\n' +
    '                                <img ng-src="{{vm.job.image}}">\n' +
    '                                <div class="edit" ng-if="vm.job.image">\n' +
    '                                    <a ng-click="vm.job.image = undefined">Remove</a>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Map</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="job-map-preview">\n' +
    '                            <p> <strong>Map Preview</strong></p>\n' +
    '                            <geocoding address="vm.job.address" default-lat="\'1.352083\'" default-lng="\'103.819836\'" lat="vm.job.latitude" lng="vm.job.longitude"></geocoding>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s12">\n' +
    '                                <input ng-model-options="{ debounce: 1000 }" type="text" name="address" ng-model="vm.job.address" required>\n' +
    '                                <label ng-class="{\'active\': vm.job.address}">Address</label>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.address.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '\n' +
    '                        <div class="row">\n' +
    '                            <div class="input-field col s6">\n' +
    '                                <input type="text" ng-model="vm.job.latitude">\n' +
    '                                <label ng-class="{active: vm.job.latitude}">Latitude</label>\n' +
    '                            </div>\n' +
    '                            <div class="input-field col s6">\n' +
    '                                <input type="text" ng-model="vm.job.longitude">\n' +
    '                                <label ng-class="{active: vm.job.longitude}">Longitude</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '\n' +
    '        </div>\n' +
    '        <div class="create-job">\n' +
    '            <div class="s12 m12">\n' +
    '                <div class="card">\n' +
    '                    <div class="card-title blue-grey darken-1">Shifts</div>\n' +
    '                    <div class="card-content">\n' +
    '                        <div class="row shifts">\n' +
    '                            <p><strong>Availabe Shifts</strong></p>\n' +
    '                            <div class="shift" ng-repeat="shift in vm.shifts">\n' +
    '                                <div class="input-field col s12 shift_description">\n' +
    '                                    <input id="shift_description" name="shift_description_{{$index}}" type="text" ng-model="shift.description">\n' +
    '                                    <label ng-class="{\'active\': shift.description}" for="shift_description">Description</label>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.shift_description_{{$index}}.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col s12">\n' +
    '                                    <md-input-container class="col s4 md-input-has-placeholder">\n' +
    '                                        <label>Start Date/Time</label>\n' +
    '                                        <input mdc-datetime-picker="" name="start_{{$index}}" date="true" time="true" type="text" max-date="shift.end" ng-model="shift.start" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.start_{{$index}}.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <md-input-container class="col s4 md-input-has-placeholder">\n' +
    '                                        <label>End Date/Time</label>\n' +
    '                                        <input mdc-datetime-picker="" name="end_{{$index}}" date="true" time="true" type="text" min-date="shift.start" ng-model="shift.end" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.end_{{$index}}.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <md-input-container class="col s2">\n' +
    '                                        <input type="text" ng-model="shift.salary" name="salary" required ng-pattern="/^[1-9][0-9]*$/" ng-maxlength="7" aria-label="salary">\n' +
    '                                        <label>Salary</label>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.pattern">Please enter a valid value</span>\n' +
    '                                            <span ng-show="vm.CreateJobForm.$submitted && vm.CreateJobForm.salary.$error.maxlength">The value is too long</span>\n' +
    '                                        </div>\n' +
    '                                    </md-input-container>\n' +
    '                                    <div class="col s2 delete-btn">\n' +
    '                                        <a class="waves-effect btn-flat" ng-click="vm.removeShift(shift)"><i class="material-icons">delete</i></a>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="add-more-shift-btn">\n' +
    '                                <a class="waves-effect btn-flat" ng-click="vm.addMoreShift()"><i class="material-icons left">add</i>Add more shift</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12 right-align">\n' +
    '                        <button class="btn waves-effect waves-light" ng-disabled="vm.inProgress" ng-class="{\'disabled\': vm.inProgress}" type="submit" name="action">Submit\n' +
    '                            <i class="material-icons right">send</i>\n' +
    '                            <i class="material-icons right icon-pending">replay</i>\n' +
    '                        </button>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </form>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/verify/verify.tpl.html',
    '<div class="verify">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col s12">\n' +
    '                <p ng-class="vm.verifyClass" class="center-align" ng-bind="vm.verifyMessage"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="col s12 verify-button center-align">\n' +
    '                <a class="waves-effect waves-light btn" ui-sref="app.dashboard">Dashboard</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/layouts/pages/content-only.tpl.html',
    '<div ui-view="content"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/layouts/pages/master.tpl.html',
    '<div ui-view="header"></div>\n' +
    '<div ui-view="content" class="global-page"></div>\n' +
    '<div ui-view="footer"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/layouts/partials/_footer.tpl.html',
    '<!-- Scroll Top Start -->\n' +
    '<div class="scroll-to-top">\n' +
    '    <div class="scrollup" scroll-top>\n' +
    '        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- Scroll Top End -->\n' +
    '<!-- ===========================Footer Start================================  -->\n' +
    '<footer>\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-4 col-sm-4 col s12 m4">\n' +
    '                <div class="footer-left">\n' +
    '                    <div class="footer-logo">\n' +
    '                        <img ng-src="{{ asset(\'/assets/images/footer_logo.png\') }}" alt="" />\n' +
    '                    </div>\n' +
    '                    <div class="address">\n' +
    '                        <address>\n' +
    '                            <a href="tel:+6582452082"><i class="fa fa-headphones" aria-hidden="true"></i> +65 8245 2082</a>\n' +
    '                            <br>\n' +
    '                            <a href="mailto:hello@ushift.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> hello@ushift.com</a>\n' +
    '                            <br>\n' +
    '<!--                            <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> Live Chat</a>\n' +
    '                            <br>-->\n' +
    '                        </address>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="col-md-8 col-sm-8 col s12 m8">\n' +
    '                <div class="footer-right">\n' +
    '                    <p>Copyright &copy; 2016 All Right Reserved By Ushift Pte Ltd</p>\n' +
    '                    <ul>\n' +
    '                        <li><a href="/about" force-reload>About Us</a></li>\n' +
    '                        <!-- <li><a href="{{ url(\'about\') }}">About Us</a></li> -->\n' +
    '                        <li><a href="/terms-of-use" force-reload>Terms of Use</a></li>\n' +
    '                        <li><a href="/privacy-policy" force-reload>Privacy Policy</a></li>\n' +
    '                        <li><a href="/cookie-policy" force-reload>Cookie Policy</a></li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</footer>\n' +
    '<!-- /.Footer End   -->\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/layouts/partials/_header.tpl.html',
    '<div class="white">\n' +
    '    <div class="container">\n' +
    '        <nav ng-if="!isLoggedIn">\n' +
    '            <div class="nav-wrapper">\n' +
    '                <a ui-sref="app.homepage" class="brand-logo"><img ng-src="{{ asset(\'/assets/images/logo.png\')}}" alt="Ushift logo" /></a>\n' +
    '                <div ng-if="!isLoggedIn">\n' +
    '                    <ul class="right hide-on-med-and-down" ng-if="!isLoggedIn">\n' +
    '                        <li ng-show="$state.current.name !== \'user.login\'">\n' +
    '                            <a class="black-text login" ui-sref="user.login">Log in</a>\n' +
    '                        </li>\n' +
    '                        <li ng-show="$state.current.name !== \'user.register\'">\n' +
    '                            <a class="waves-effect waves-light btn signup" ui-sref="user.register">Sign Up</a>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '        <div class="row menu-main-top" ng-if="isLoggedIn">\n' +
    '            <div class="logo-main">\n' +
    '                <a ui-sref="app.dashboard" class="brand-logo"><img ng-src="{{ asset(\'/assets/images/logo.png\')}}" alt="Ushift logo" /></a>\n' +
    '            </div>\n' +
    '            <div class="box-login right-align right" ng-if="isLoggedIn" user-menu-toggle>\n' +
    '                <ul class="header-btn right-align">\n' +
    '                    <li>\n' +
    '                        <div class="user-avatar" toggle-dropdown="user-menu-dropdown">\n' +
    '                            <user-profile-image image="user.getImage()" width="42" height="42"></user-profile-image>\n' +
    '                        </div>\n' +
    '                        <div class="user-dropdown white card" id="user-menu-dropdown">\n' +
    '                            <ul>\n' +
    '                                <li>\n' +
    '                                    <div class="user-information">\n' +
    '                                        <h4 class="nickname"><strong ng-bind="vm.user.first_name"></strong></h4>\n' +
    '                                        <small ng-bind="vm.user.email"></small>\n' +
    '                                    </div>\n' +
    '                                </li>\n' +
    '                                <li class="divider"></li>\n' +
    '                                <li ng-show="$state.current.name.indexOf(\'app.dashboard\') < 0"><a ui-sref="app.dashboard">Dashboard</a></li>\n' +
    '                                <li ng-show="$state.current.name != \'app.profile\'"><a ui-sref="app.profile({user_id: \'me\'})">Profile</a></li>\n' +
    '                                <li class="divider"></li>\n' +
    '                                <li><a href="#" ng-click="logout()">Log out</a></li>\n' +
    '                            </ul>\n' +
    '                        </div>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <a ng-if="user.isEmployee()" ui-sref="app.search.sidebar.all" class="first left"><i class="material-icons">search</i> Find job</a>\n' +
    '                        <a ng-if="user.isEmployer()" ui-sref="app.search.sidebar.all" class="er-browse-job-link left">Browse jobs</a>\n' +
    '                        <a ng-if="user.isEmployer()" ui-sref="app.dashboard.new-job" class="first left"><i class="material-icons left">add</i>New Job</a>\n' +
    '                        \n' +
    '                        \n' +
    '                        <a class="notification-icon" toggle-dropdown="notification-dropdown"><i class="large material-icons">chat_bubble_outline</i><span class="unread-message" ng-show="unread && unread > 0" ng-bind="unread"></span></a>\n' +
    '                        <div id="notification-dropdown">\n' +
    '                            <ul class="collection">\n' +
    '                                <li class="collection-item avatar" ng-class="{ \'unread-message\': conversation.hasNewMessage(user.getId()) }" ng-repeat="conversation in conversations" ng-click="conversation.hasNewMessage() && clearUnread(conversation.getId())">\n' +
    '                                    <a ui-sref="app.inbox.message({conversation_id: conversation.getId()})">\n' +
    '                                        <conversation-item type="notification" conversation="conversation"></conversation-item>\n' +
    '                                    </a>\n' +
    '                                </li>\n' +
    '                                <li>\n' +
    '                                    <p class="center-align">\n' +
    '                                        <a ui-sref="app.inbox">Open Inbox</a>\n' +
    '                                    </p>\n' +
    '                                </li>\n' +
    '                            </ul>\n' +
    '                        </div>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <breadcrumb ng-if="isLoggedIn" items="breadcrumbItems"></breadcrumb>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/common/layouts/partials/_job.tpl.html',
    '<div class="card">\n' +
    '    <div class="card-image waves-effect waves-block waves-light">\n' +
    '        <img class="activator" ng-src="{{item.image}}">\n' +
    '    </div>\n' +
    '    <div class="card-content">\n' +
    '        <span class="card-title activator grey-text text-darken-4">\n' +
    '            <a ui-sref="app.job({slug: item.getSlug()})"> \n' +
    '                <h3 ng-bind="item.title"></h3>\n' +
    '            </a>\n' +
    '            <i class="button-open material-icons right">more_vert</i>\n' +
    '        </span>\n' +
    '        <p class="prince">\n' +
    '            <a ui-sref="app.job({slug: item.getSlug()})" data-hint="Favorite">\n' +
    '                <i class="fa fa-bars fa-lg" aria-hidden="true"></i>\n' +
    '            </a>\n' +
    '            <a href="" data-hint="Favorite">\n' +
    '                <i class="fa fa-heart" aria-hidden="true"></i>\n' +
    '            </a>\n' +
    '            <span class="right right-align"><i ng-bind="item.type"></i> {{item.salary}}</span>\n' +
    '        </p>\n' +
    '    </div>\n' +
    '    <div class="card-reveal">\n' +
    '        <span class="card-title grey-text text-darken-4">\n' +
    '            <h3 ng-bind="item.title"></h3>\n' +
    '            <i class="button-close material-icons right">close</i>\n' +
    '        </span>\n' +
    '        <p class="card-description" ng-bind="item.short_desc"></p>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/employee/_dashboard_jobs.tpl.html',
    '<ul class="collection">\n' +
    '    <li class="collection-item avatar" ng-repeat="shift in vm.shifts">\n' +
    '        <img ng-src="{{shift.job.image}}" alt="" class="circle">\n' +
    '        <span class="title" ng-bind="shift.description"></span>\n' +
    '        <div class="shift-info">\n' +
    '            <p><a ui-sref="app.job({slug: shift.job.slug})"><strong ng-bind="shift.job.title"></strong></a></p>\n' +
    '            <p>\n' +
    '                <strong>From:</strong> {{ shift.getStartTime() | timeFormat:\'MMMM Do YYYY, hh:mm\'}}\n' +
    '            </p>\n' +
    '            <p>\n' +
    '                <strong>To:</strong> {{ shift.getEndTime() | timeFormat:\'MMMM Do YYYY, hh:mm\' }}\n' +
    '            </p>\n' +
    '        </div>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '<simple-pagination page="vm.page" ng-show="vm.showPagination"></simple-pagination>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/employee/_upcomming.tpl.html',
    '<div class="col s12">\n' +
    '    <ul class="collection">\n' +
    '        <li class="collection-item avatar" ng-repeat="shift in vm.shifts">\n' +
    '            <img ng-src="{{shift.job.image}}" alt="" class="circle">\n' +
    '            <span class="title" ng-bind="shift.description"></span>\n' +
    '            <div class="shift-info">\n' +
    '                <p><a ui-sref="app.job({slug: shift.job.slug})"><strong ng-bind="shift.job.title"></strong></a></p>\n' +
    '                <p>\n' +
    '                    <strong>From:</strong> {{ shift.getStartTime() | timeFormat:\'MMMM Do YYYY, hh:mm\'}}\n' +
    '                </p>\n' +
    '                <p>\n' +
    '                    <strong>To:</strong> {{ shift.getEndTime() | timeFormat:\'MMMM Do YYYY, hh:mm\' }}\n' +
    '                </p>\n' +
    '            </div>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '    <!-- <simple-pagination page="vm.page"></simple-pagination> -->\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/employer/_dashboard_home.tpl.html',
    '<div class="col s12 m6">\n' +
    '    <div class="er-widget widget-first-job" ng-if="vm.jobs | empty">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title">Post your first Job</div>\n' +
    '            <div class="card-content">\n' +
    '                <p>Let\'s post your first Job</p>\n' +
    '                <a ui-sref="app.dashboard.new-job" class="waves-effect waves-light btn"><i class="material-icons left">add</i>New Job</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="er-widget widget-last-job" ng-if="vm.jobs | hasItem">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title">Your last Job</div>\n' +
    '            <div class="card-content">\n' +
    '                <div class="job">\n' +
    '                    <job item="vm.jobs[0]"></job>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div class="col s12 m6">\n' +
    '    <div class="er-widget widget-complete-profile">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title">Complete your profile</div>\n' +
    '            <div class="card-content">\n' +
    '                <div class="col s6">\n' +
    '                    <div class="progress">\n' +
    '                        <div ng-show="vm.completedProfile" class="determinate" style="width: {{vm.completedProfile}}%">{{vm.completedProfile}}%</div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col s6 right-align">\n' +
    '                    <a ui-sref="app.profile.update.general({user_id: \'me\'})">update your profile</a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="er-widget widget-company-profile">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title">Company profile</div>\n' +
    '            <div class="card-content">\n' +
    '                <div class="col s3">\n' +
    '                    <div class="company-image">\n' +
    '                        <img ng-src="{{vm.user.getImage()}}" alt="company image">\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="col s9">\n' +
    '                    <table class="striped">\n' +
    '                        <tbody>\n' +
    '                            <tr ng-if="vm.user.employer.getCompanyName()">\n' +
    '                                <td>Name</td>\n' +
    '                                <td ng-bind="vm.user.employer.getCompanyName()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="vm.user.employer.getCompanyAbout()">\n' +
    '                                <td>About</td>\n' +
    '                                <td ng-bind="vm.user.employer.getCompanyAbout()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="vm.user.employer.getCompanyMainActivity()">\n' +
    '                                <td>Main Activity</td>\n' +
    '                                <td ng-bind="vm.user.employer.getCompanyMainActivity()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="vm.user.employer.getCompanyAddress()">\n' +
    '                                <td>Address</td>\n' +
    '                                <td ng-bind="vm.user.employer.getCompanyAddress()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="m.user.employer.getNumberOfOutlets()">\n' +
    '                                <td>Number of outlets</td>\n' +
    '                                <td ng-bind="vm.user.employer.getNumberOfOutlets()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="vm.user.employer.getPerferredContactMethod()">\n' +
    '                                <td>Contact method</td>\n' +
    '                                <td ng-bind="vm.user.employer.getPerferredContactMethod()"></td>\n' +
    '                            </tr>\n' +
    '                            <tr ng-if="vm.user.employer.getPreferredContactTime()">\n' +
    '                                <td>Contact time</td>\n' +
    '                                <td ng-bind="vm.user.employer.getPreferredContactTime()"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                    <div class="right-align update-company-info">\n' +
    '                        <a ui-sref="app.profile.update.company-infomation">Update</a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/employer/_dashboard_jobs.tpl.html',
    '<div class="list-jobs">\n' +
    '    <div ng-if="vm.jobs | empty" class="col s12">\n' +
    '        <div ng-if="vm.jobs | empty" class="post-new-jobs">\n' +
    '            <p>Let\'s post your Job</p>\n' +
    '            <a ui-sref="app.dashboard.new-job" class="waves-effect waves-light btn"><i class="material-icons left">add</i>New Job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="job" ng-repeat="item in vm.jobs">\n' +
    '        <job item="item" action="vm.action"></job>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<simple-pagination page="vm.page" ng-show="vm.showPagination"></simple-pagination>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/dashboard/employer/_sidebar.tpl.html',
    '<div id="er-dashboard-sidebar">\n' +
    '    <ul class="er-sidebar collapsible" data-collapsible="expandable">\n' +
    '        <li class="er-sidebar-item" ng-repeat="item in vm.employerSidebarItems" ng-class="{\'has-submenu\': vm.hasSubmenu(item)}" ui-sref-active="er-navbar-item-active">\n' +
    '            <a ng-if="item.state" ui-sref="{{item.state}}">\n' +
    '                <div class="icon">\n' +
    '                    <i class="material-icons">{{ item.icon }}</i>\n' +
    '                </div>\n' +
    '                <div class="menu">\n' +
    '                    {{item.menu}}\n' +
    '                </div>\n' +
    '            </a>\n' +
    '            <a ng-if="!item.state" class="collapsible-header active">\n' +
    '                <div class="icon">\n' +
    '                    <i class="material-icons">{{ item.icon }}</i>\n' +
    '                </div>\n' +
    '                <div class="menu">\n' +
    '                    {{item.menu}}\n' +
    '                </div>\n' +
    '            </a>\n' +
    '            <div class="submenu collapsible-body" ng-show="item.submenu | hasItem">\n' +
    '                <ul>\n' +
    '                    <li ng-repeat="submenu in item.submenu" ui-sref-active-eq="er-navbar-item-active">\n' +
    '                        <a ui-sref="{{submenu.state}}({{submenu.stateOption | json}})" ui-sref-opts="">{{submenu.menu}}</a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/message_template/_type_employee_send_application.tpl.html',
    '<div class="message-employee-send-application">\n' +
    '    <div ng-if="user.getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>Your application has been submited</p>\n' +
    '            <a ui-sref="app.job.application({slug: job.getSlug(), user_id: message.getSenderId()})" target="_blank">View job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div ng-if="conversation.getReceiver().getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>You received a new application</p>\n' +
    '            <a ui-sref="app.job.application({slug: job.getSlug(), user_id: message.getSenderId()})" target="_blank">View Application</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/message_template/_type_employer_change_application.tpl.html',
    '<div class="message-employer-changed-application">\n' +
    '    <div ng-if="user.getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p ng-if="user.getId() === message.getSenderId() && message.isAcceptNewApplication()">You accepted a new application</p>\n' +
    '            <p ng-if="user.getId() === message.getSenderId() && !message.isAcceptNewApplication()">The application has been cancelled</p>\n' +
    '            <a ui-sref="app.job.application({slug: message.getJobSlug(), user_id: message.getEmployeeId()})" target="_blank">View application</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div ng-if="user.getId() != message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p ng-if="user.getId() !== lastMessage.getSenderId() && lastMessage.isAcceptNewApplication()">Your application has been accepted</p>\n' +
    '            <p ng-if="user.getId() !== lastMessage.getSenderId() && !lastMessage.isAcceptNewApplication()">Your application has been cancelled</p>\n' +
    '            <a ui-sref="app.job.application({slug: job.getSlug(), user_id: user.getId()})" target="_blank">View Application</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/message_template/_type_mark_job_as_completed.tpl.html',
    '<div class="message-type-mark mark-completed">\n' +
    '    <div ng-if="user.getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>The job has been completed</p>\n' +
    '            <a ui-sref="app.job({slug: job.getSlug()})" target="_blank">View job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '\n' +
    '    <div ng-if="conversation.getReceiver().getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>The job has been completed</p>\n' +
    '            <a ui-sref="app.job({slug: job.getSlug()})" target="_blank">View job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/message_template/_type_mark_job_as_incompleted.tpl.html',
    '<div class="message-type-mark mark-completed">\n' +
    '    <div ng-if="user.getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>You marked this job as <strong>incompleted</strong></p>\n' +
    '            <a ui-sref="app.job({slug: job.getSlug()})" target="_blank">View job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div ng-if="conversation.getReceiver().getId() == message.getSenderId()">\n' +
    '        <div class="center-align">\n' +
    '            <p>The job has been completed</p>\n' +
    '            <a ui-sref="app.job({slug: job.getSlug()})" target="_blank">View job</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/inbox/message_template/_type_normal.tpl.html',
    '<div class="message-type-normal">\n' +
    '    <div class="message-image">\n' +
    '        <img ng-if="message.getSenderId() == conversation.getReceiver().getId()" ng-src="{{ conversation.getReceiver().getImage() }}" alt="" class="circle">\n' +
    '        <img ng-if="message.getSenderId() == user.getId()" ng-src="{{ user.getImage() }}" alt="" class="circle">\n' +
    '    </div>\n' +
    '    <div class="message-content" ng-class="{\'message-red\' : message.getSenderId() == user.getId()}" >\n' +
    '        <div ng-bind-html="message.getContent()"></div>\n' +
    '    </div>\n' +
    '    <div ng-if="!message.getId()" class="progress">\n' +
    '        <div class="indeterminate"></div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_company_infomation.tpl.html',
    '<div class="card">\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <div class="mdl-stepper-horizontal-alternative">\n' +
    '                <div class="mdl-stepper-step step-done" ng-click="vm.$parent.previous()">\n' +
    '                    <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                    <div class="mdl-stepper-title">General Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                    <div class="mdl-stepper-title">Company Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Profile Created</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div class="card">\n' +
    '    <div class="card-title blue-grey darken-1">Company Infomations</div>\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <form id="update-profile-form" name="vm.ProfileUpdateForm" ng-submit="vm.ProfileUpdateForm.$valid && vm.$parent.update(vm.profile)" update-text-fields novalidate>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12">\n' +
    '                        <label class="required">Company Name</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.company_name" name="company_name" required ng-maxlength="100">\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.company_name.$error.required">Required</span>\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.company_name.$error.maxlength">The value is too long</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12">\n' +
    '                        <label class="required">Company Address</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.company_address" name="company_address" required>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.company_address.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label class="required">Number of Outlets</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.number_of_outlets" name="number_of_outlets" required>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.number_of_outlets.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label class="required">Registed Number</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.registed_number" name="registed_number" required ng-maxlength="15">\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.registed_number.$error.required">Required</span>\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.registed_number.$error.maxlength">The value is too long</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12">\n' +
    '                        <label class="required">Company Main Activity</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.company_main_activity" name="company_main_activity" required>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.company_main_activity.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label class="required">Contact</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.preferred_contact_method" name="preferred_contact_method" required>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.preferred_contact_method.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label class="required">Contact Time</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employer.preferred_contact_time" name="preferred_contact_time" required>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.preferred_contact_time.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12">\n' +
    '                        <label>Company About</label>\n' +
    '                        <textarea ng-model="vm.profile.employer.company_about"></textarea>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="card-action">\n' +
    '        <div class="row">\n' +
    '            <div class="col s4 left-align">\n' +
    '                <button ng-click="vm.$parent.skip()" class="btn btn-flat grey">Skip</button>\n' +
    '            </div>\n' +
    '            <div class="col s4">\n' +
    '\n' +
    '            </div>\n' +
    '            <div class="col s4 right-align">\n' +
    '                <a ng-click="vm.$parent.previous()">Previous</a>\n' +
    '                <button form="update-profile-form" class="btn btn-flat">Save</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <!--        <a ng-click="vm.$parent.skip()">Skip</a>-->\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_general_infomation.tpl.html',
    '<div class="card">\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12 center-align">\n' +
    '            <div class="mdl-stepper-horizontal-alternative">\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                    <div class="mdl-stepper-title">General Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                    <div class="mdl-stepper-title">Company Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Profiles Deployed</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<form id="update-profile-form" name="vm.ProfileUpdateForm" ng-submit="vm.ProfileUpdateForm.$valid && vm.$parent.update(vm.profile)" update-text-fields novalidate>\n' +
    '    <div class="col s12 m4">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title blue-grey darken-1">General Informations</div>\n' +
    '            <div class="card-content white-text">\n' +
    '                <div class="row">\n' +
    '                    <div class="input-field col s12 upload-avatar">\n' +
    '                        <div class="image-preview">\n' +
    '                            <user-profile-image image="vm.profile.image"></user-profile-image>\n' +
    '                        </div>\n' +
    '                        <a class="change-avatar-btn" dropzone before-upload="vm.beforeUploadAvatar" on-success="vm.uploadAvatar"><i class="material-icons">mode_edit</i> Change your Avatar</a>\n' +
    '                        <i class="vertical-middle preUploadAvatarStatus material-icons small red-text" ng-show="vm.preUploadAvatarMessage !== undefined">error</i>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="card">\n' +
    '            <div class="card-title blue-grey darken-1">General Informations</div>\n' +
    '            <div class="card-content white-text">\n' +
    '                <div class="row">\n' +
    '                    <div class="input-field col s12 upload-avatar">\n' +
    '                        <div class="cover-preview">\n' +
    '                            <img ng-src="{{vm.profile.cover}}">\n' +
    '                        </div>\n' +
    '                        <a class="change-avatar-btn" dropzone before-upload="vm.beforeUploadCover" on-success="vm.uploadCover"><i class="material-icons">mode_edit</i> Change your Cover</a>\n' +
    '                        <i class="vertical-middle preUploadAvatarStatus material-icons small red-text" ng-show="vm.preUploadAvatarMessage !== undefined">error</i>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col s12 m8">\n' +
    '        <div class="card">\n' +
    '            <div class="card-title blue-grey darken-1">General Informations</div>\n' +
    '            <div class="card-content white-text">\n' +
    '                <div class="col s12">\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label class="required">First Name</label>\n' +
    '                            <input id="first-name" type="text" ng-model="vm.profile.first_name" name="first_name" required ng-maxlength="20">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.first_name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.first_name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label class="required">Last Name</label>\n' +
    '                            <input id="last-name" type="text" ng-model="vm.profile.last_name" name="last_name" required ng-maxlength="20">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.last_name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.last_name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label>Gender</label>\n' +
    '                            <md-select ng-model="vm.profile.gender" aria-label="Gender">\n' +
    '                                <md-select-label>{{ vm.profile.gender}}</md-select-label>\n' +
    '                                <md-option ng-value="gender.value" ng-repeat="gender in vm.genders">{{gender.display}}</md-option>\n' +
    '                            </md-select>\n' +
    '                        </md-input-container>\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label>Birthday (YYYY-MM-DD)</label>\n' +
    '                            <input type="text" ng-model="vm.profile.birth" name="birthday" ui-mask="9999-99-99" ui-mask-placeholder-char="space">\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12">\n' +
    '                            <label>Mobile</label>\n' +
    '                            <input type="text" ng-model="vm.profile.mobile" name="mobile" ng-pattern="/^(\\+\\d{2,3})?\\s?(\\d{6,12})$/">\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.mobile.$error.pattern">Please enter a valid mobile number +xx xxxxxxxx</span>\n' +
    '                            </div>\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12">\n' +
    '                            <label>Address</label>\n' +
    '                            <input type="text" ng-model="vm.profile.address" name="address">\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label>City</label>\n' +
    '                            <input type="text" ng-model="vm.profile.city" name="city">\n' +
    '                        </md-input-container>\n' +
    '                        <md-input-container class="col s12 m6">\n' +
    '                            <label>Country</label>\n' +
    '                            <input type="text" ng-model="vm.profile.country" name="country">\n' +
    '                        </md-input-container>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <div class="card-action">\n' +
    '            <div class="row">\n' +
    '                <div class="col s6 m4 left-align">\n' +
    '                    <button ng-click="vm.$parent.skip()" class="btn btn-flat grey">Skip</button>\n' +
    '                </div>\n' +
    '\n' +
    '                <div class="col s6 m4 offset-m4 right-align">\n' +
    '                    <button form="update-profile-form" class="btn btn-flat">Continue</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <!--        <a ng-click="vm.$parent.skip()">Skip</a>-->\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</form>\n' +
    '<!--    \n' +
    '\n' +
    '    <div class="card-action">\n' +
    '        <button form="update-profile-form" class="btn btn-flat">Save & Next</button>\n' +
    '        <a ng-click="vm.$parent.skip()">Skip</a>\n' +
    '    </div>\n' +
    '\n' +
    '-->\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_interested_in.tpl.html',
    '<div class="card">\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <div class="mdl-stepper-horizontal-alternative">\n' +
    '                <div class="mdl-stepper-step step-done" ng-click="vm.$parent.previous()">\n' +
    '                    <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                    <div class="mdl-stepper-title">General Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                    <div class="mdl-stepper-title">Interested In</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Working Experiences</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Profile Created</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div class="card">\n' +
    '    <div class="card-title blue-grey darken-1">Interested In</div>\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <form id="update-profile-form" name="vm.ProfileUpdateForm" ng-submit="vm.ProfileUpdateForm.$valid && vm.$parent.update(vm.profile)" update-text-fields novalidate>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>Select your industry *</label>\n' +
    '                        <md-select ng-model="vm.profile.employee.industry" aria-label="industry">\n' +
    '                            <md-option ng-repeat="industry in vm.industries" ng-value="industry">{{industry.name}}</md-option>\n' +
    '                        </md-select>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>Select your position *</label>\n' +
    '                        <md-select name="perferred_positions" ng-model="vm.profile.employee.preferred_positions[0]" aria-label="preferred position" required>\n' +
    '                            <md-option ng-value="position" ng-repeat="position in vm.preferred_positions">{{position.getName()}}</md-option>\n' +
    '                        </md-select>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.perferred_positions.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>What type of job you are looking for</label>\n' +
    '                        <md-select ng-model="vm.profile.employee.kind_of_work" aria-label="kind_of_work">\n' +
    '                            <md-option ng-repeat="kind_of_work in vm.kind_of_works" ng-value="kind_of_work">{{kind_of_work}}</md-option>\n' +
    '                        </md-select>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>Select your location</label>\n' +
    '                        <md-select ng-model="vm.profile.employee.preferred_locations" aria-label="preferred_locations" multiple="true">\n' +
    '                            <md-option ng-repeat="preferred_location in vm.preferred_locations" ng-value="preferred_location">{{preferred_location.name}}</md-option>\n' +
    '                        </md-select>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <md-checkbox ng-model="vm.profile.employee.allow_to_work_in_sg" aria-label="allow_to_work_in_sg">\n' +
    '                            Allow to work in Singapore\n' +
    '                        </md-checkbox>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>How many hour you can work each week?</label>\n' +
    '                        <input type="text" ng-model="vm.profile.employee.hours_per_week" name="hours_per_week" ng-pattern="/^\\d{1,4}$/">\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.hours_per_week.$error.pattern">Please enter a valid number</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '<!--    <div class="card-action">\n' +
    '        <button form="update-profile-form" class="btn btn-flat">Save & Next</button>\n' +
    '        <a ng-click="vm.$parent.skip()">Skip</a>\n' +
    '    </div>-->\n' +
    '    <div class="card-action">\n' +
    '        <div class="row">\n' +
    '            <div class="col s4 left-align">\n' +
    '                <button ng-click="vm.$parent.skip()" class="btn btn-flat grey">Skip</button>\n' +
    '            </div>\n' +
    '            <div class="col s4">\n' +
    '\n' +
    '            </div>\n' +
    '            <div class="col s4 right-align">\n' +
    '                <a ng-click="vm.$parent.previous()">Previous</a>\n' +
    '                <button form="update-profile-form" class="btn btn-flat">Save</button>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '        <!--        <a ng-click="vm.$parent.skip()">Skip</a>-->\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_profile_created.tpl.html',
    '<div class="row" ng-if="vm.profile.isEmployer()">\n' +
    '    <div class="card">\n' +
    '        <div class="card-content white-text">\n' +
    '            <div class="col s12">\n' +
    '                <div class="mdl-stepper-horizontal-alternative">\n' +
    '                    <div class="mdl-stepper-step step-done" ng-click="vm.$parent.previous()">\n' +
    '                        <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                        <div class="mdl-stepper-title">General Informations</div>\n' +
    '                        <div class="mdl-stepper-bar-left"></div>\n' +
    '                        <div class="mdl-stepper-bar-right"></div>\n' +
    '                    </div>\n' +
    '                    <div class="mdl-stepper-step step-done">\n' +
    '                        <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                        <div class="mdl-stepper-title">Company Informations</div>\n' +
    '                        <div class="mdl-stepper-bar-left"></div>\n' +
    '                        <div class="mdl-stepper-bar-right"></div>\n' +
    '                    </div>\n' +
    '                    <div class="mdl-stepper-step active-step">\n' +
    '                        <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                        <div class="mdl-stepper-title">Profile Created</div>\n' +
    '                        <div class="mdl-stepper-bar-left"></div>\n' +
    '                        <div class="mdl-stepper-bar-right"></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div class="card" ng-if="vm.profile.isEmployee()">\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <div class="mdl-stepper-horizontal-alternative">\n' +
    '                <div class="mdl-stepper-step step-done" ng-click="vm.$parent.previous()">\n' +
    '                    <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                    <div class="mdl-stepper-title">General Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                    <div class="mdl-stepper-title">Interested In</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step step-done">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Working Experiences</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Profile Created</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '<div class="card">\n' +
    '    <div class="card-content">\n' +
    '        <p ng-bind="vm.message"></p>\n' +
    '    </div>\n' +
    '    <div class="card-action">\n' +
    '        <a ng-if="vm.profile.isEmployee()" ui-sref="app.search.sidebar.all">Work now</a>\n' +
    '        <a ng-if="vm.profile.isEmployer()" ui-sref="app.dashboard.new-job">Post a Job</a>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_update_profile.tpl.html',
    '<div id="update-profile">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div ui-view="update_profile_form"></div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/frontend/js/components/profile/update/_update_working_experiences.tpl.html',
    '<div class="card">\n' +
    '    <div class="card-content white-text">\n' +
    '        <div class="col s12">\n' +
    '            <div class="mdl-stepper-horizontal-alternative">\n' +
    '                <div class="mdl-stepper-step step-done" ng-click="vm.$parent.previous()">\n' +
    '                    <div class="mdl-stepper-circle"><span>1</span></div>\n' +
    '                    <div class="mdl-stepper-title">General Informations</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step step-done"  ng-click="vm.$parent.previous()">\n' +
    '                    <div class="mdl-stepper-circle"><span>2</span></div>\n' +
    '                    <div class="mdl-stepper-title">Interested In</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step active-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Working Experiences</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '                <div class="mdl-stepper-step">\n' +
    '                    <div class="mdl-stepper-circle"><span>3</span></div>\n' +
    '                    <div class="mdl-stepper-title">Profile Created</div>\n' +
    '                    <div class="mdl-stepper-bar-left"></div>\n' +
    '                    <div class="mdl-stepper-bar-right"></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<form id="update-profile-form" name="vm.ProfileUpdateForm" ng-submit="vm.ProfileUpdateForm.$valid && vm.updateLastPosition(vm.profile, vm.last_positions)" update-text-fields novalidate>\n' +
    '\n' +
    '    <div class="card">\n' +
    '        <div class="card-title blue-grey darken-1">Working Experiences</div>\n' +
    '        <div class="card-content">\n' +
    '            <div class="col s12">\n' +
    '                <div class="row">\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>Select your education level *</label>\n' +
    '                        <md-select ng-model="vm.profile.employee.education_level" aria-label="education_level">\n' +
    '                            <md-option ng-repeat="item in vm.education_levels" ng-value="item">{{item.name}}</md-option>\n' +
    '                        </md-select>\n' +
    '                    </md-input-container>\n' +
    '                    <md-input-container class="col s12 m6">\n' +
    '                        <label>Select your experiences *</label>\n' +
    '                        <md-select name="working_experience" ng-model="vm.profile.employee.working_experience" aria-label="working_experience" required>\n' +
    '                            <md-option ng-value="item" ng-repeat="item in vm.working_experiences">{{item.getName()}}</md-option>\n' +
    '                        </md-select>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.ProfileUpdateForm.$submitted && vm.ProfileUpdateForm.perferred_positions.$error.required">Required</span>\n' +
    '                        </div>\n' +
    '                    </md-input-container>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <p><strong>Last positions</strong></p>\n' +
    '                    <div class="input-field col s12 last-position">\n' +
    '                        <table class="responsive-table last-position-table">\n' +
    '                            <thead>\n' +
    '                                <tr>\n' +
    '                                    <th>Role</th>\n' +
    '                                    <th>Company</th>\n' +
    '                                    <th>Start</th>\n' +
    '                                    <th>End</th>\n' +
    '                                    <th>Reason of leaving</th>\n' +
    '                                    <th>Name of manager</th>\n' +
    '                                    <th>Remove</th>\n' +
    '                                </tr>\n' +
    '                            </thead>\n' +
    '                            <tbody>\n' +
    '                                <tr ng-repeat="item in vm.profile.employee.last_positions" ng-show="vm.profile.employee.last_positions | isArray">\n' +
    '                                    <td ng-bind="item.getRole()"></td>\n' +
    '                                    <td ng-bind="item.getCompanyName()"></td>\n' +
    '                                    <td ng-bind="item.getStartDate() | timeFormat:\'d MMMM, YYYY\'"></td>\n' +
    '                                    <td ng-bind="item.getEndDate() | timeFormat:\'d MMMM, YYYY\'"></td>\n' +
    '                                    <td ng-bind="item.getReasonOfLeaving()"></td>\n' +
    '                                    <td ng-bind="item.getNameOfManager()"></td>\n' +
    '                                    <td><a ng-click="vm.deleteLastPosition($index, item)" href="javascript:;" class="red-text"><i class="material-icons">delete</i></a></td>\n' +
    '                                </tr>\n' +
    '                                <tr ng-repeat="item in vm.last_positions">\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="item.role">\n' +
    '                                    </td>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="item.company_name">\n' +
    '                                    </td>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" pick-a-date="item.start_date" pick-a-date-options="{selectMonths:true, selectYears: 10, close: \'Accept\'}" class="datepicker" max-date="vm.today" />\n' +
    '                                    </td>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" pick-a-date="item.end_date" pick-a-date-options="{selectMonths:true, selectYears: 10, close: \'Accept\'}" class="datepicker" min-date="item.start_date" max-date="vm.today"/>\n' +
    '                                    </td>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="item.reason_of_leaving">\n' +
    '                                    </td>\n' +
    '                                    <td>\n' +
    '                                        <input type="text" ng-model="item.name_of_manager">\n' +
    '                                    </td>\n' +
    '                                    <td><a ng-click="vm.deleteLastPosition($index, item)" href="javascript:;" class="red-text"><i class="material-icons">delete</i></a></td>\n' +
    '                                </tr>\n' +
    '                            </tbody>\n' +
    '                        </table>\n' +
    '                        <a ng-click="vm.addLastPosition()" class="right btn-floating btn-large waves-effect waves-light teal lighten-1"><i class="material-icons">add</i></a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="card-action">\n' +
    '            <div class="row">\n' +
    '                <div class="col s4 left-align">\n' +
    '                    <button ng-click="vm.$parent.skip()" class="btn btn-flat grey">Skip</button>\n' +
    '                </div>\n' +
    '                <div class="col s4">\n' +
    '\n' +
    '                </div>\n' +
    '                <div class="col s4 right-align">\n' +
    '                    <a ng-click="vm.$parent.previous()">Previous</a>\n' +
    '                    <button form="update-profile-form" class="btn btn-flat">Save</button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '\n' +
    '            <!--        <a ng-click="vm.$parent.skip()">Skip</a>-->\n' +
    '        </div>\n' +
    '        <!--        <div class="card-action">\n' +
    '                    <button form="update-profile-form" class="btn btn-flat">Save & Next</button>\n' +
    '                    <a ng-click="vm.$parent.skip()">Skip</a>\n' +
    '                </div>-->\n' +
    '    </div>\n' +
    '\n' +
    '</form>');
}]);
})();
