(function(app) {
    app.config(['$httpProvider', 'jwtInterceptorProvider', function($httpProvider, jwtInterceptorProvider) {
        jwtInterceptorProvider.tokenGetter = ['jwtHelper', '$http', '$cookieStore', 'API', 'config', function(jwtHelper, $http, $cookieStore, API, config) {
            var idToken = $cookieStore.get(JWT_TOKEN_COOKIE_KEY);
            if (idToken === undefined) {
                return null;
            }
            if (config.url.substr(config.url.length - 5) == '.html') {
                return null;
            }
            if (jwtHelper.isTokenExpired(idToken)) {
                return API.auth.refreshToken()
                    .then(function(response) {
                        $cookieStore.put(JWT_TOKEN_COOKIE_KEY, response.token);
                        return response.token;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            } else {
                return idToken;
            }
        }];
        $httpProvider.interceptors.push('jwtInterceptor');
        $httpProvider.interceptors.push(['$cookieStore', function($cookieStore) {
            return {
                response: function(response) {
                    if (response.headers().authorization !== undefined) {
                        $cookieStore.put(JWT_TOKEN_COOKIE_KEY, response.headers().authorization.substr(7));
                    }
                    return response; 
                }
            };
        }]);
    }]);
})(angular.module('app.config', [
    'angular-jwt',
]));
