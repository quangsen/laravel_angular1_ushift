var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function WorkingExperience(options) {
    this.id = null;
    this.name = '';
    BaseModel.call(this, options);
}

inherits(WorkingExperience, BaseModel);

WorkingExperience.prototype.getId = function() {
    return this.id;
};

WorkingExperience.prototype.getName = function() {
    return this.name;
};

module.exports = WorkingExperience;
