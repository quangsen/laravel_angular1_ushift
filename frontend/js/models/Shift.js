var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var EmployeeApplied = require('./EmployeeApplied');

function Shift(options) {
    this.id = null;
    this.job_id = null;
    this.job = '';
    this.salary = '';
    this.description = '';
    this.type = '';
    this.start_time = '';
    this.end_time = '';
    this.request_active = false;
    this.request_status = false;
    this.selected = false;
    this.users = new EmployeeApplied();
    BaseModel.call(this, options);
}

inherits(Shift, BaseModel);

Shift.prototype.getJobId = function() {
    return this.job_id;
};


Shift.prototype.getStartTime = function() {
    return this.start_time;
};

Shift.prototype.getEndTime = function() {
    return this.end_time;
};

Shift.prototype.getType = function() {
    return this.type;
};

Shift.prototype.getSalary = function() {
    return this.salary;
};

Shift.prototype.getDescription = function() {
    return this.description;
};


Shift.prototype.isSelected = function() {
    return this.selected;
};

Shift.prototype.toggleSelected = function() {
    this.selected = !this.selected;
    return this;
};

Shift.prototype.isTaken = function() {
    var user = _.find(this.users, function(user) {
        return user.application.accept;
    });
    return !_.isUndefined(user);
};


module.exports = Shift;
