var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function EducationLevel(options) {
    this.id = null;
    this.name = '';
    BaseModel.call(this, options);
}

inherits(EducationLevel, BaseModel);

EducationLevel.prototype.getId = function() {
    return this.id;
};

EducationLevel.prototype.getName = function() {
    return this.name;
};

module.exports = EducationLevel;
