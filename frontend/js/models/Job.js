var BaseModel = require('./BaseModel');
var User = require('./User');
var Shift = require('./Shift');
var inherits = require('inherits');

function Job(options) {
    this.id = null;
    this.title = '';
    this.slug = '';
    this.image = '';
    this.address = '';
    this.latitude = '';
    this.longitude = '';
    this.short_desc = '';
    this.description = '';
    this.shifts = new Shift();
    this.note = '';
    this.type = '';
    this.requirement = '';
    this.salary = '';
    this.active = null;
    this.complete = null;
    this.employer = new User();
    this.categories = {};
    this.accepted_applications = {};

    BaseModel.call(this, options);
}

inherits(Job, BaseModel);

Job.prototype.getTitle = function() {
    return this.title;
};

Job.prototype.getSlug = function() {
    return this.slug;
};

Job.prototype.getImage = function() {
    return this.image;
};

Job.prototype.getAddress = function() {
    return this.address;
};

Job.prototype.getShortDesc = function() {
    return this.short_desc;
};

Job.prototype.getDescription = function() {
    return this.description;
};

Job.prototype.getShifts = function() {
    return this.shifts;
};

Job.prototype.getNote = function() {
    return this.note;
};

Job.prototype.getType = function() {
    return this.type;
};

Job.prototype.getPaymentType = function() {
    return this.payment_type;
};

Job.prototype.getSalary = function() {
    return this.salary;
};

Job.prototype.getActive = function() {
    return this.active;
};

Job.prototype.isActive = function() {
    return this.active === true;
};

Job.prototype.isCompleted = function() {
    return this.complete === true;
};

Job.prototype.getCategories = function() {
    return this.categories.data;
};

Job.prototype.getLatitude = function() {
    return this.latitude;
};

Job.prototype.getLongitude = function() {
    return this.longitude;
};

Job.prototype.getAcceptedApplications = function() {
    return this.accepted_applications.data;
};

Job.prototype.getSelectedShifts = function() {
    if (this.shifts !== undefined && Array.isArray(this.shifts)) {
        return _.filter(this.shifts, function(item) {
            return item.selected;
        });
    } else {
        return [];
    }
};

module.exports = Job;
