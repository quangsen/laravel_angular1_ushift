var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function LastPosition(options) {
    this.id = null;
    this.company_name = '';
    this.country = '';
    this.role = '';
    this.type = '';
    this.start_date = '';
    this.end_date = '';
    this.reason_of_leaving = '';
    this.name_of_manager = '';
    BaseModel.call(this, options);
}

inherits(LastPosition, BaseModel);

LastPosition.prototype.getCompanyName = function() {
    return this.company_name;
};

LastPosition.prototype.getCountry = function() {
    return this.country;
};

LastPosition.prototype.getRole = function() {
    return this.role;
};

LastPosition.prototype.getType = function() {
    return this.type;
};

LastPosition.prototype.getStartDate = function() {
    return this.start_date;
};

LastPosition.prototype.getEndDate = function() {
    return this.end_date;
};

LastPosition.prototype.getReasonOfLeaving = function() {
    return this.reason_of_leaving;
};

LastPosition.prototype.getNameOfManager = function() {
    return this.name_of_manager;
};

module.exports = LastPosition;
