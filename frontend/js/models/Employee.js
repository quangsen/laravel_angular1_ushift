var BaseModel = require('./BaseModel');
var Industry = require('./Industry');
var PreferredPosition = require('./PreferredPosition');
var PreferredLocation = require('./PreferredLocation');
var LastPosition = require('./LastPosition');
var WorkingExperience = require('./WorkingExperience');
var EducationLevel = require('./EducationLevel');
var inherits = require('inherits');

function Employee(options) {
    this.allow_to_work_in_sg = '';
    this.kind_of_work = '';
    this.hours_per_week = '';
    this.current_position = '';
    this.resume = '';
    this.industry = new Industry();
    this.education_level = new EducationLevel();
    this.preferred_positions = new PreferredPosition();
    this.preferred_locations = new PreferredLocation();
    this.working_experience = new WorkingExperience();
    this.last_positions = new LastPosition();
    this.email_verified = '';

    BaseModel.call(this, options);
}

inherits(Employee, BaseModel);


module.exports = Employee;
