var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Application(options) {
    this.request = '';
    this.shift_id = '';
    this.accept = false;
    this.active = false;
    this.complete = false;

    BaseModel.call(this, options);
}

inherits(Application, BaseModel);

Application.prototype.getRequest = function() {
	return this.request;
};

Application.prototype.getShiftId = function() {
	return this.shift_id;
};

Application.isAccepted = function() {
	return this.accept;
};

Application.isActive = function() {
    return this.active;
};

Application.isCompleted = function() {
	return this.complete;
};

Application.prototype.getTimeToNow = function() {
    return moment(this.getCreatedAt()).toNow();
};

module.exports = Application;
