var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var moment = require('moment');
var Application = require('./Application');

function EmployeeApplied(options) {
    this.employee_id = null;
    this.email = '';
    this.first_name = '';
    this.last_name = '';
    this.image = '';
    this.mobile = '';
    this.gender = '';
    this.birth = '';
    this.address = '';
    this.zipcode = '';
    this.city = '';
    this.country = '';
    this.summary = '';
    this.application = new Application();
    BaseModel.call(this, options);
}

inherits(EmployeeApplied, BaseModel);

EmployeeApplied.prototype.getEmployeeId = function() {
    return this.employee_id;
};

EmployeeApplied.prototype.getEmail = function() {
    return this.email;
};

EmployeeApplied.prototype.getFirstName = function() {
    return this.first_name;
};

EmployeeApplied.prototype.getLastName = function() {
    return this.last_name;
};

EmployeeApplied.prototype.getImage = function() {
    return this.image;
};

EmployeeApplied.prototype.getMobile = function() {
    return this.mobile;
};

EmployeeApplied.prototype.getGender = function() {
    return this.gender;
};

EmployeeApplied.prototype.getBirth = function() {
    return this.birth;
};

EmployeeApplied.prototype.getAddress = function() {
    return this.address;
};

EmployeeApplied.prototype.getZipcode = function() {
    return this.zipcode;
};

EmployeeApplied.prototype.getCity = function() {
    return this.city;
};

EmployeeApplied.prototype.getCountry = function() {
    return this.country;
};

EmployeeApplied.prototype.getSummary = function() {
    return this.summary;
};

EmployeeApplied.prototype.getName = function() {
    return this.getFirstName() + ' ' + this.getLastName();
};

EmployeeApplied.prototype.getTime = function() {
    return parseInt(this.application.time);
};

module.exports = EmployeeApplied;
