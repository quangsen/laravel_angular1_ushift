function BaseModel(options) {
    this.bind(options);
}

BaseModel.prototype.bind = function(options) {
    options = options || {};
    for (var k in options) {
        var v = options[k];
        if (typeof v === 'function') {
            continue;
        }

        if (this.hasOwnProperty(k)) {
            if (v !== null && v !== undefined) {
                if (typeof this[k] === 'object' && this[k] !== null && this[k].constructor.name !== 'Object' && typeof v.data !== undefined) {
                    var data;
                    if (Array.isArray(v.data)) {
                        data = createNewInstanceOfArray(this[k].constructor, v.data);
                    } else {
                        if (v.data !== undefined && typeof v.data === 'object') {
                            data = new this[k].constructor(v.data);
                        } else {
                            if (Array.isArray(v)) {
                                data = createNewInstanceOfArray(this[k].constructor, v);
                            } else {
                                data = new this[k].constructor(v);
                            }
                        }
                    }
                    this[k] = data;
                } else {
                    this[k] = v;
                }
            }
        }
        if (options.timestamps !== undefined) {
            this.timestamps = options.timestamps;
        }
    }
};


BaseModel.prototype.getId = function() {
    if (!this.id) {
        return null;
    }
    return this.id;
};

BaseModel.prototype.getCreatedAt = function() {
    return this.timestamps.created_at.date;
};

BaseModel.prototype.getUpdatedAt = function() {
    return this.updated_at.date;
};

BaseModel.prototype.getTimezoneType = function() {
    return this.timestamps.created_at.timezone_type;
};

BaseModel.prototype.getTimezone = function() {
    return this.timestamps.created_at.timezone;
};

function createNewInstanceOfArray(instance, array) {
    return _.map(array, function(item) {
        return new instance(item);
    });
}

module.exports = BaseModel;
