var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Message(options) {
    this.id = null;
    this.content = '';
    this.type = null;
    this.sender_id = null;
    this.receive_id = null;
    this.unread = null;
    BaseModel.call(this, options);
}

inherits(Message, BaseModel);

Message.prototype.getContent = function() {
    return this.content;
};

Message.prototype.getType = function() {
    return this.type;
};

Message.prototype.getSenderId = function() {
    return this.sender_id;
};

Message.prototype.isUnread = function() {
    return this.unread;
};

Message.prototype.getJobSlug = function() {
    if (this.getType() === 4 || this.getType() === 5) {
        var data = JSON.parse(this.getContent());
        return data.job_slug;
    }
};

Message.prototype.getEmployeeId = function() {
    if (this.getType() === 4 || this.getType() === 5) {
        var data = JSON.parse(this.getContent());
        return data.employee_id;
    }
};

Message.prototype.isAcceptNewApplication = function() {
    if (this.getType() === 4 || this.getType() === 5) {
        var data = JSON.parse(this.getContent());
        return data.applicationHasBeenAccept;
    }
};

module.exports = Message;
