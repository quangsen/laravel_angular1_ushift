var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Category(options) {
    this.id = null;
    this.name = '';
    this.level = null;
    this.parent_id = null;
    this.children = [];

    BaseModel.call(this, options);
}

inherits(Category, BaseModel);

Category.prototype.getId = function() {
    return this.id;
};

Category.prototype.getName = function() {
    return this.name;
};

Category.prototype.getLevel = function() {
    return this.level;
};

Category.prototype.getParentId = function() {
    return this.parent_id;
};

Category.prototype.getChildren = function() {
    return this.children;
};

Category.prototype.getSlug = function() {
    return _.kebabCase(this.name);
};

Category.prototype.addChild = function(child) {
    if (child instanceof Category) {
        this.children.push(child);
    } else {
        throw "child category must be instanceof Category";
    }
    return this;
};

Category.prototype.hasChild = function() {
    if (this.children.length > 0) {
        return true;
    } else {
        return false;
    }
};

module.exports = Category;
