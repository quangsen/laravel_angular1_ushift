var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function UserApplication(options) {
    this.id = null;
    this.title = '';
    this.slug = '';
    this.image = '';
    this.address = '';
    this.latitude = '';
    this.longitude = '';
    this.short_desc = '';
    this.description = '';
    this.start_date = null;
    this.end_date = null;
    this.note = '';
    this.type = '';
    this.payment_type = '';
    this.salary = '';
    this.active = null;
    this.employer = {};
    this.categories = {};
    this.application = {};

    BaseModel.call(this, options);
}

inherits(UserApplication, BaseModel);

UserApplication.prototype.getId = function() {
    return this.id;
};

UserApplication.prototype.getTitle = function() {
    return this.title;
};

UserApplication.prototype.getSlug = function() {
    return this.slug;
};

UserApplication.prototype.getImage = function() {
    return this.image;
};

UserApplication.prototype.getAddress = function() {
    return this.address;
};

UserApplication.prototype.getShortDesc = function() {
    return this.short_desc;
};

UserApplication.prototype.getDescription = function() {
    return this.description;
};

UserApplication.prototype.getStartDate = function() {
    return this.start_date;
};

UserApplication.prototype.getEndDate = function() {
    return this.end_date;
};

UserApplication.prototype.getNote = function() {
    return this.note;
};

UserApplication.prototype.getType = function() {
    return this.type;
};

UserApplication.prototype.getPaymentType = function() {
    return this.payment_type;
};

UserApplication.prototype.getSalary = function() {
    return this.salary;
};

UserApplication.prototype.getActive = function() {
    return this.active;
};

UserApplication.prototype.getCategories = function() {
    return this.categories.data;
};

UserApplication.prototype.getLatitude = function() {
    return this.latitude;
};

UserApplication.prototype.getLongitude = function() {
    return this.longitude;
};

module.exports = UserApplication;
