var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Employer(options) {
    this.company_name = '';
    this.company_about = '';
    this.registed_number = '';
    this.company_address = '';
    this.number_of_outlets = '';
    this.company_main_activity = '';
    this.preferred_contact_method = '';
    this.preferred_contact_time = '';

    BaseModel.call(this, options);
}

inherits(Employer, BaseModel);

Employer.prototype.getCompanyName = function() {
    return this.company_name;
};

Employer.prototype.getCompanyAbout = function() {
    return this.company_about;
};

Employer.prototype.getRegisterNumber = function() {
    return this.registed_number;
};

Employer.prototype.getCompanyAddress = function() {
    return this.company_address;
};

Employer.prototype.getNumberOfOutlets = function() {
    return this.number_of_outlets;
};

Employer.prototype.getCompanyMainActivity = function() {
    return this.company_main_activity;
};

Employer.prototype.getPerferredContactMethod = function() {
    return this.preferred_contact_method;
};

Employer.prototype.getPreferredContactTime = function() {
    return this.preferred_contact_time;
};

module.exports = Employer;
