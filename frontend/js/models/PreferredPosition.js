var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function PreferredPosition(options) {
    this.id = null;
    this.name = '';
    BaseModel.call(this, options);
}

inherits(PreferredPosition, BaseModel);

PreferredPosition.prototype.getName = function() {
    return this.name;
};

module.exports = PreferredPosition;
