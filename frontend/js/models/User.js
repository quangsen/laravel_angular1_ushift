var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var moment = require('moment');

var Employer = require('./Employer');
var Employee = require('./Employee');

function User(options) {
    this.id = null;
    this.email = '';
    this.first_name = '';
    this.last_name = '';
    this.image = '';
    this.cover = '';
    this.mobile = '';
    this.gender = '';
    this.birth = '';
    this.address = '';
    this.zipcode = '';
    this.city = '';
    this.country = '';
    this.email_verified = false;
    this.is_updated_profile = false;
    this.summary = '';
    this.role = {};
    this.employee = new Employee();
    this.employer = new Employer();

    BaseModel.call(this, options);
}

inherits(User, BaseModel);

User.prototype.getEmail = function() {
    return this.email;
};

User.prototype.getFirstName = function() {
    return this.first_name;
};

User.prototype.getLastName = function() {
    return this.last_name;
};

User.prototype.getImage = function() {
    return this.image;
};

User.prototype.getCover = function() {
    return this.cover;
};

User.prototype.getMobile = function() {
    return this.mobile;
};

User.prototype.getGender = function() {
    return this.gender;
};

User.prototype.getBirth = function() {
    return this.birth;
};

User.prototype.getAddress = function() {
    return this.address;
};

User.prototype.getZipcode = function() {
    return this.zipcode;
};

User.prototype.getCity = function() {
    return this.city;
};

User.prototype.getCountry = function() {
    return this.country;
};

User.prototype.getEmailVerified = function() {
    return this.email_verified;
};

User.prototype.isUpdatedProfile = function() {
    return this.is_updated_profile;
};

User.prototype.getSummary = function() {
    return this.summary;
};

User.prototype.getRole = function() {
    return this.role;
};

User.prototype.getUsername = function() {
    return this.first_name + ' ' + this.last_name;
};

User.prototype.getName = function() {
    return this.first_name + ' ' + this.last_name;
};

User.prototype.getAge = function() {
    if (moment(this.birth).isValid()) {
        return moment().format('YYYY') - moment(this.birth).format('YYYY');
    }
};

User.prototype.getEmployee = function() {
    return this.employee;
};

// User.prototype.getAllowToWorkInSg = function() {
//     return this.employee.data.allow_to_work_in_sg;
// };

// User.prototype.getKindOfWork = function() {
//     return this.employee.data.kind_of_work;
// };

// User.prototype.getHoursPerWeek = function() {
//     return this.employee.data.hours_per_week;
// };

// User.prototype.getCurrentPosition = function() {
//     return this.employee.data.current_position;
// };

// User.prototype.getResume = function() {
//     return this.employee.data.resume;
// };

// User.prototype.getIndustry = function() {
//     return this.employee.data.industry.data;
// };

// User.prototype.getEducationLevel = function() {
//     return this.employee.data.education_level.data;
// };

// User.prototype.getWorkingExperience = function() {
//     return this.employee.data.working_experience.data;
// };

User.prototype.getLastPositions = function() {
    return this.employee.last_positions;
};

// User.prototype.getPreferredPositions = function() {
//     return this.employee.data.preferred_positions.data;
// };

// User.prototype.getPreferredLocations = function() {
//     return this.employee.data.preferred_locations.data;
// };

User.prototype.getEmployer = function() {
    return this.employer;
};

User.prototype.getCompanyAbout = function() {
    return this.employer.company_about;
};

User.prototype.getCompanyAddress = function() {
    return this.employer.company_address;
};

User.prototype.getCompanyMainActivity = function() {
    return this.employer.company_main_activity;
};

User.prototype.getCompanyName = function() {
    return this.employer.company_name;
};

User.prototype.getNumberOfOutlets = function() {
    return this.employer.number_of_outlets;
};

User.prototype.getPreferredContactMethod = function() {
    return this.employer.preferred_contact_method;
};

User.prototype.getPreferredContactTime = function() {
    return this.employer.preferred_contact_time;
};

User.prototype.getRegistedNumber = function() {
    return this.employer.registed_number;
};

User.prototype.is = function(role) {
    return !_.isUndefined(_.find(this.role.data, function(item) {
        return _.toLower(item.name) === _.toLower(role);
    }));
};

User.prototype.isEmployer = function() {
    return this.is('employer');
};

User.prototype.isEmployee = function() {
    return this.is('employee');
};

User.prototype.getRaw = function() {
    var birth_pattern = /^([0-9]{4})([0-9]{2})([0-9]{2})$/;
    var params = {};
    for (var key in this) {
        if (!_.isFunction(this[key]) && !_.isUndefined(this[key]) && !_.isNull(this[key]) && !_.isObject(this[key])) {
            if (_.isBoolean(this[key])) {
                params[key] = this[key] ? '1' : '0';
            }
            if (_.isString(this[key]) && this[key] !== '') {
                params[key] = this[key];
            }
        }
        // Industry
        if (!_.isNil(this.employee.industry) && !_.isNil(this.employee.industry.id)) {
            params.industry_id = this.employee.industry.id;
        }
        // Preferred position
        if (!_.isUndefined(this.employee.preferred_positions) && _.isArray(this.employee.preferred_positions) && !_.isEmpty(this.employee.preferred_positions)) {
            params.preferred_position_id = this.employee.preferred_positions[0].id;
        }
        // Preferred Location
        if (!_.isUndefined(this.employee.preferred_locations) && _.isArray(this.employee.preferred_locations) && !_.isEmpty(this.employee.preferred_locations)) {
            params.preferred_location_ids = _.map(this.employee.preferred_locations, function(item) {
                return item.id;
            });
        }
        // Kind of work
        if (!_.isNil(this.employee.kind_of_work) && this.employee.kind_of_work !== '') {
            params.kind_of_work = this.employee.kind_of_work;
        }
        // Hours per week
        if (!_.isNil(this.employee.hours_per_week) && this.employee.hours_per_week !== '') {
            params.hours_per_week = this.employee.hours_per_week;
        }
        // Allow to work in SG
        if (!_.isNil(this.employee.allow_to_work_in_sg) && this.employee.allow_to_work_in_sg !== '') {
            params.allow_to_work_in_sg = this.employee.allow_to_work_in_sg;
        }
        // Education level
        if (!_.isNil(this.employee.education_level) && !_.isNil(this.employee.education_level.id)) {
            params.education_level_id = this.employee.education_level.id;
        }
        // working experience
        if (!_.isNil(this.employee.working_experience) && !_.isNil(this.employee.working_experience.id)) {
            params.working_experience_id = this.employee.working_experience.id;
        }
        // Company Name
        if (!_.isNil(this.employer.company_name) && this.employer.company_name !== '') {
            params.company_name = this.employer.company_name;
        }
        // Company About
        if (!_.isNil(this.employer.company_about) && this.employer.company_about !== '') {
            params.company_about = this.employer.company_about;
        }
        // Company Registed Number
        if (!_.isNil(this.employer.registed_number) && this.employer.registed_number !== '') {
            params.registed_number = this.employer.registed_number;
        }
        // Company Address
        if (!_.isNil(this.employer.company_address) && this.employer.company_address !== '') {
            params.company_address = this.employer.company_address;
        }
        // Company Number of Outlets
        if (!_.isNil(this.employer.number_of_outlets) && this.employer.number_of_outlets !== '') {
            params.number_of_outlets = this.employer.number_of_outlets;
        }
        // Company Main Activiry
        if (!_.isNil(this.employer.company_main_activity) && this.employer.company_main_activity !== '') {
            params.company_main_activity = this.employer.company_main_activity;
        }
        // Company Preferred Contact Method
        if (!_.isNil(this.employer.preferred_contact_time) && this.employer.preferred_contact_time !== '') {
            params.preferred_contact_time = this.employer.preferred_contact_time;
        }
        // Company Preferred Contact Time
        if (!_.isNil(this.employer.preferred_contact_method) && this.employer.preferred_contact_method !== '') {
            params.preferred_contact_method = this.employer.preferred_contact_method;
        }
    }
    // Birthday
    if (!_.isUndefined(params.birth) && params.birth.match(birth_pattern)) {
        params.birth = params.birth.replace(birth_pattern, "$1-$2-$3");
    }
    // Gender
    if (!_.isUndefined(params.gender)) {
        params.gender = params.gender === 'male' ? 0 : 1;
    }

    return params;
};

module.exports = User;
