var BaseModel = require('./BaseModel');
var PreferredPosition = require('./PreferredPosition');
var inherits = require('inherits');

function Industry(options) {
    this.id = null;
    this.name = '';
    this.preferred_positions = new PreferredPosition();
    BaseModel.call(this, options);
}

inherits(Industry, BaseModel);

Industry.prototype.getName = function() {
    return this.name;
};

Industry.prototype.getPreferredPositions = function() {
    return this.preferred_positions;
};

module.exports = Industry;
