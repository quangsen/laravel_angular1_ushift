var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var Message = require('./Message');

function Conversation(options) {
    this.id = null;
    this.title = '';
    this.job_id = '';
    this.receiver = {};
    this.messages = new Message();
    BaseModel.call(this, options);
}

inherits(Conversation, BaseModel);

Conversation.prototype.getId = function() {
    return this.id;
};

Conversation.prototype.getTitle = function() {
    return this.title;
};

Conversation.prototype.getJobId = function() {
    return this.job_id;
};

Conversation.prototype.getReceiver = function() {
    return this.receiver;
};

Conversation.prototype.getMessages = function() {
    return this.messages;
};

Conversation.prototype.hasNewMessage = function(current_user_id) {
    var unread = !_.isUndefined(_.find(this.messages, function(item) {
        return item.sender_id !== current_user_id && item.unread;
    }));
    return unread;
};

module.exports = Conversation;
