require('ui-router');

(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('user', {
                views: {
                    'main@': {
                        templateUrl: '/frontend/js/common/layouts/pages/content-only.tpl.html'
                    }
                }
            })
            .state('app', {
                views: {
                    'main@': {
                        templateUrl: '/frontend/js/common/layouts/pages/master.tpl.html',
                        controller: 'MainCtrl',
                        controllerAs: 'vm',
                    },
                    'header@app': {
                        templateUrl: '/frontend/js/common/layouts/partials/_header.tpl.html',
                        controller: 'HeaderCtrl',
                        controllerAs: 'vm',
                    },
                    'footer@app': {
                        templateUrl: '/frontend/js/common/layouts/partials/_footer.tpl.html',
                        controller: 'FooterCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user: ['$rootScope', '$cookieStore', 'API', function($rootScope, $cookieStore, API) {
                        if (!_.isUndefined($rootScope.user)) {
                            return $rootScope.user;
                        } else {
                            if (!_.isUndefined($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
                                var user = API.user.profile(['role'])
                                    .then(function(user) {
                                        $rootScope.user = user;
                                        $rootScope.isLoggedIn = true;
                                        return user;
                                    })
                                    .catch(function(error) {
                                        return undefined;
                                    });
                                return user;
                            } else {
                                $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                                return undefined;
                            }
                        }
                    }]
                }
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(angular.module('app.router', ['ui.router']));
