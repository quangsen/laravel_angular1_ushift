(function(app) {
    app.controller('SearchParentCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'categories', function($rootScope, $scope, $state, $stateParams, API, categories) {
        var vm = this;

        $rootScope.parent_category = _.find(categories, function(item) {
            return _.kebabCase(item.getName()) === $stateParams.parent_category_slug;
        });
        if (_.isUndefined($rootScope.parent_category)) {
            $state.go('app.search.sidebar.all');
        } else {
            $rootScope.title = $rootScope.parent_category.getName();
            var page = $stateParams.page || 1;
            API.job.get({ page: page, category_id: $rootScope.parent_category.getId() })
                .then(function(result) {
                    vm.jobs = result.jobs;
                    $rootScope.pagination = result.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }
        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Search',
            href: $state.href('app.search.sidebar.all'),
        }, {
            name: $rootScope.parent_category.getName(),
        }];
    }]);
})(angular.module('app.components.search.controllers.parent_category', []));
