(function(app) {
    app.controller('SearchChildCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'categories', function($rootScope, $scope, $state, $stateParams, API, categories) {
        var vm = this;
        $rootScope.parent_category = _.find(categories, function(item) {
            return _.kebabCase(item.getName()) === $stateParams.parent_category_slug;
        });
        var child_category = _.find(categories, function(item) {
            return _.kebabCase(item.getName()) === $stateParams.child_category_slug;
        });
        if (_.isUndefined(child_category)) {
            $state.go('app.search.sidebar.parent_category', { parent_category_slug: $stateParams.parent_category_slug });
        } else {
            $rootScope.title = $rootScope.parent_category.getName() + ' - ' + child_category.getName();
            var page = $stateParams.page || 1;
            API.job.get({ page: page, category_id: child_category.getId() })
                .then(function(result) {
                    vm.jobs = result.jobs;
                    $rootScope.pagination = result.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Search',
            href: $state.href('app.search.sidebar.all'),
        }, {
            name: $rootScope.parent_category.getName(),
            href: $state.href('app.search.sidebar.parent_category', { parent_category_slug: $stateParams.parent_category_slug })
        }, {
            name: child_category.getName(),
        }];

    }]);
})(angular.module('app.components.search.controllers.child_category', []));
