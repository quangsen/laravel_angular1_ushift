(function(app) {
    app.controller('SearchAllCtrl', ['$rootScope', '$state', '$stateParams', 'API', function($rootScope, $state, $stateParams, API) {
        var vm = this;
        var page = $stateParams.page || 1;
        $rootScope.title = 'Search List | Ushift';

        var include = ['employer'];
        API.job.get({ page: page }, include)
            .then(function(result) {
                vm.jobs = result.jobs;
                console.log(vm.jobs);
                $rootScope.pagination = result.pagination;
            })
            .catch(function(error) {
                throw error;
            });

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Search',
        }];

    }]);
})(angular.module('app.components.search.controllers.all', []));
