(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.search', {
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/search/search.tpl.html",
                        controller: 'SearchCtrl',
                    }
                },
                resolve: {
                    categories: ['API', function(API) {
                        return API.category.get();
                    }],
                }
            })
            .state('app.search.sidebar', {
                views: {
                    'search-content@app.search': {
                        templateUrl: "/frontend/js/components/search/search-sidebar.tpl.html",
                        controller: 'SearchSidebarCtrl',
                    }
                }
            })
            .state('app.search.sidebar.all', {
                url: '/search?page',
                views: {
                    'jobs@app.search.sidebar': {
                        templateUrl: "/frontend/js/components/search/search-jobs.tpl.html",
                        controller: 'SearchAllCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.search.sidebar.parent_category', {
                url: '/search/:parent_category_slug?page',
                views: {
                    'jobs@app.search.sidebar': {
                        templateUrl: "/frontend/js/components/search/search-jobs.tpl.html",
                        controller: 'SearchParentCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.search.sidebar.parent_category.child_category', {
                url: '/:child_category_slug',
                views: {
                    'jobs@app.search.sidebar': {
                        templateUrl: "/frontend/js/components/search/search-jobs.tpl.html",
                        controller: 'SearchChildCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.search.router', []));
