require('./router');
require('./controllers/all');
require('./controllers/parent_category');
require('./controllers/child_category');
(function(app) {
    app.controller('SearchCtrl', ['$rootScope', '$state', 'API', 'categories', function($rootScope, $state, API, categories) {
        $rootScope.checkLogin();
        $rootScope.categories = categories;
        $rootScope.title = 'Search List | Ushift';
        $rootScope.checkLogin();
    }]);

    app.controller('SearchSidebarCtrl', ['$rootScope', '$state', '$stateParams', 'API', 'categories', function($rootScope, $state, $stateParams, API, categories) {
        
        
         $rootScope.employerSidebarItems = [{
            icon: 'add_circle_outline',
            menu: 'Sort by',
            state: '',
            submenu: [{
                menu: 'Salary',
                state: 'app.search.sort',
                stateOption: {
                    status: 'salary'
                },
            }, {
                menu: 'Date',
                state: 'app.search.sort',
                stateOption: {
                    status: 'date'
                }
            }]
        }, {
            icon: 'alarm',
            menu: 'When',
            submenu: [{
                menu: 'Today',
                state: 'app.search.filter.when',
                stateOption: {
                    status: 'today'
                },
            }, {
                menu: 'Tomorrow',
                state: 'app.search.filter.when',
                stateOption: {
                    status: 'tomorrow'
                }
            }, {
                menu: 'Weekends',
                state: 'app.search.filter.when',
                stateOption: {
                    status: 'weekends'
                },
            }]
        }, {
            icon: 'repeat',
            menu: 'Multiple shift only',
            state: 'app.search.filter.repeat',
            submenu: []
        }];
        setTimeout(function() {
            $('.collapsible').collapsible();
        }, 1000);


        $rootScope.hasSubmenu = function(item) {
            return _.isArray(item.submenu) && item.submenu.length > 0;
        };
        $rootScope.parent_categories = _.filter(categories, function(item) {
            return item.getParentId() === 0;
        });
    }]);

})(angular.module('app.components.search', [
    'app.components.search.router',
    'app.components.search.controllers.all',
    'app.components.search.controllers.parent_category',
    'app.components.search.controllers.child_category',
]));
