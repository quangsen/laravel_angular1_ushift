(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.login', {
                url: '/login?redirect',
                views: {
                    'content@user': {
                        templateUrl: "/frontend/js/components/login/login.tpl.html",
                        controller: 'LoginCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.login.router', []));
