require('./router');
(function(app) {

    app.controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$rootScope', '$cookieStore', '$state', '$stateParams', 'API', 'Notification', 'Modal'];

    function LoginCtrl($rootScope, $cookieStore, $state, $stateParams, API, Notification, Modal) {
        var vm = this;
        $rootScope.title = 'Login';
        if (!_.isUndefined($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
            API.user.profile()
                .then(function(user) {
                    var content = 'You have logged in as ' + user.getUsername();
                    Modal.confirm({
                        title: 'You are already logged in',
                        textContent: content,
                        ok: 'LOG OUT',
                        cancel: 'GO TO DASHBOARD',
                    }, function() {
                        $rootScope.logout();
                    }, function() {
                        $state.go('app.dashboard');
                    });
                })
                .catch(function(error) {
                    $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                });
        }
        vm.login = function(user) {
            vm.inProgress = true;
            API.auth.login(user.email, user.password)
                .then(function(result) {
                    console.log(result, result.token);
                    $cookieStore.put(JWT_TOKEN_COOKIE_KEY, result.token);
                    vm.inProgress = false;
                    API.user.profile(['role'])
                        .then(function(user) {
                            $rootScope.user = user;
                            $rootScope.isLoggedIn = true;
                            if (!_.isNil($stateParams.redirect) && !_.isNil($rootScope.fromState) && !_.isNil($rootScope.formParams)) {
                                $state.go($rootScope.fromState, $rootScope.formParams);
                            } else {
                                if (user.isEmployer()) {
                                    $state.go('app.dashboard');
                                } else {
                                    $state.go('app.search.sidebar.all');
                                }
                            }

                        })
                        .catch(function(error) {
                            throw error;
                        });
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw (error);
                });
        };
    }

})(angular.module('app.components.login', ['app.components.login.router']));
