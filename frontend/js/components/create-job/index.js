require('./router');
(function(app) {

    app.controller('CreateJobCtrl', CreateJobCtrl);
    CreateJobCtrl.$inject = ['$rootScope', '$scope', '$state', 'API', 'Modal', 'Notification', 'user'];

    function CreateJobCtrl($rootScope, $scope, $state, API, Modal, Notification, user) {

        $rootScope.checkLogin();

        var vm = this;
        if (!user.isEmployer()) {
            Modal.alert({
                title: 'Permission denied',
                textContent: 'Only employer can create new job'
            }, function() {
                $rootScope.gotoLoginPage();
            });
        }
        $rootScope.title = "Find many employee with Ushift";

        vm.job = {};

        vm.shifts = [{
            start: '',
            end: ''
        }];

        vm.addMoreShift = function() {
            vm.shifts.push({
                start: '',
                end: ''
            });
        };

        vm.removeShift = function(shift) {
            vm.shifts = _.pull(vm.shifts, shift);
        };

        API.category.get()
            .then(function(categories) {
                vm.categories = categories;
            })
            .catch(function(error) {
                throw error;
            });


        vm.beforeUpload = function(file) {
            console.log(file);
        };

        vm.onSuccess = function(response, file) {
            console.log(response, file);
            vm.job.image = response.path;
            $scope.$apply();
        };

        vm.create = function(job) {
            job.employer_id = $rootScope.user.getId();
            job.shifts = _.map(vm.shifts, function(shift) {
                return {
                    salary: shift.salary,
                    description: shift.description,
                    start_time: moment(shift.start).format('YYYY-MM-DD hh:mm:ss'),
                    end_time: moment(shift.end).format('YYYY-MM-DD hh:mm:ss'),
                };
            });

            vm.inProgress = true;
            API.job.create(job)
                .then(function(job) {
                    Notification.show('success', 'New job has been created successfully');
                    vm.job = {
                        shifts: [{
                            start_time: '',
                            end_time: ''
                        }]
                    };
                    vm.CreateJobForm.$setPristine();
                    vm.inProgress = false;
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Create Job',
        }];
    }

})(angular.module('app.components.create_job', [
    'app.components.create_job.router'
]));
