(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.create-job', {
                url: '/create/job',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/create-job/create.tpl.html",
                        controller: 'CreateJobCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.create_job.router', []));
