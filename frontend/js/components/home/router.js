(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.homepage', {
                url: '/',
                controller: 'HomeCtrl',
                resolve: {
                    redirect: [function() {
                        document.location.reload();
                    }],
                }
            });
    }]);
})(angular.module('app.components.home.router', []));
