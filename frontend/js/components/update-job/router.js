(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.update-job', {
                url: '/update/job/:slug',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/update-job/update.tpl.html",
                        controller: 'UpdateJobCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    job: ['API', '$stateParams', function(API, $stateParams) {
                        return API.job.getBySlug($stateParams.slug, ['employer', 'categories']);
                    }]
                }
            });
    }]);
})(angular.module('app.components.update_job.router', []));
