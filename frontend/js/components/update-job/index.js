require('./router');
(function(app) {

    app.controller('UpdateJobCtrl', UpdateJobCtrl);
    UpdateJobCtrl.$inject = ['$rootScope', '$scope', '$state', 'API', 'Modal', 'Notification', 'user', 'job'];

    function UpdateJobCtrl($rootScope, $scope, $state, API, Modal, Notification, user, job) {

        $rootScope.checkLogin();

        var vm = this;
        if (!user.isEmployer()) {
            Modal.alert({
                title: 'Permission denied',
                textContent: 'Only employer can create new job'
            }, function() {
                $rootScope.gotoLoginPage();
            });
        }
        $rootScope.title = "Find many employee with Ushift";

        vm.job = job;

        var parent_category = _.find(job.getCategories(), function(item) {
            return item.parent_id === 0;
        });
        vm.job.parent_category_id = parent_category.id;

        var child_category = _.find(job.getCategories(), function(item) {
            return item.parent_id === vm.job.parent_category_id;
        });
        vm.job.category_id = child_category.id;

        console.log(job);

        vm.shifts = _.map(vm.job.getShifts(), function(item) {
            return {
                start: item.getStartTime(),
                end: item.getEndTime(),
                description: item.getDescription(),
                salary: item.getSalary()
            };
        });

        vm.addMoreShift = function() {
            vm.shifts.push({
                start: '',
                end: ''
            });
        };

        vm.removeShift = function(shift) {
            vm.shifts = _.pull(vm.shifts, shift);
        };

        API.category.get()
            .then(function(categories) {
                vm.categories = categories;
            })
            .catch(function(error) {
                throw error;
            });


        vm.beforeUpload = function(file) {
            console.log(file);
        };

        vm.onSuccess = function(response, file) {
            console.log(response, file);
            vm.job.image = response.path;
            $scope.$apply();
        };

        vm.update = function(job) {
            job.employer_id = $rootScope.user.getId();
            job.shifts = _.map(vm.shifts, function(shift) {
                return {
                    salary: shift.salary,
                    description: shift.description,
                    start_time: moment(shift.start).format('YYYY-MM-DD hh:mm:ss'),
                    end_time: moment(shift.end).format('YYYY-MM-DD hh:mm:ss'),
                };
            });

            vm.inProgress = true;
            API.job.update(job)
                .then(function(job) {
                    Notification.show('success', 'Your job has been updated successfully');
                    vm.inProgress = false;
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Update Job',
        }];
    }

})(angular.module('app.components.update_job', [
    'app.components.update_job.router'
]));
