(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.verify', {
                url: '/verify?id&token',
                views: {
                    'content@user': {
                        templateUrl: "/frontend/js/components/verify/verify.tpl.html",
                        controller: 'VerifyCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.verify.router', []));
