require('./router');
(function(app) {

    app.controller('VerifyCtrl', VerifyCtrl);
    VerifyCtrl.$inject = ['$rootScope', 'API', '$stateParams', '$cookieStore', '$state', 'Notification', '$scope'];

    function VerifyCtrl($rootScope, API, $stateParams, $cookieStore, $state, Notification, $scope) {
        var vm = this;
        var user_id = $stateParams.id;
        var token = $stateParams.token;

        if (user_id !== undefined && user_id !== '' && token !== undefined && token !== '') {
            API.user.verify_email(user_id, token)
                .then(function(result) {
                    vm.verifyMessage = "Your email have verified";
                    vm.verifyClass = 'teal-text accent-3';
                })
                .catch(function(error) {
                    vm.verifyMessage = error.message;
                    vm.verifyClass = 'red-text lighten-1';
                });
        } else {
            vm.verifyMessage = "Don't exists id or token";
            vm.verifyClass = 'red-text lighten-1';
        }
    }

})(angular.module('app.components.verify', ['app.components.verify.router']));
