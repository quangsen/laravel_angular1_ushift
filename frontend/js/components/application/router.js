(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.job.application', {
                url: '/applications/{user_id}',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/application/application.tpl.html",
                        controller: "ApplicationCtrl",
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.application.router', []));
