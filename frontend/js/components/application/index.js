require('./router');

(function(app) {

    app.controller('ApplicationCtrl', ApplicationCtrl);

    ApplicationCtrl.$inject = ['$rootScope', '$scope', '$stateParams', '$state', 'API', 'job', 'Modal', 'Notification'];

    function ApplicationCtrl($rootScope, $scope, $stateParams, $state, API, job, Modal, Notification) {
        var vm = this;
        vm.job = job;
        vm.inMarkCompetedProgress = false;
        $rootScope.title = job.getTitle();

        if ($rootScope.user.isEmployer()) {
            API.user.show($stateParams.user_id, ['role', 'employee'])
                .then(function(employee) {
                    vm.employee = employee;
                    console.log(vm.employee);
                })
                .catch(function(error) {
                    throw error;
                });
        }

        API.user.isOwner($rootScope.user.getId(), job.getId())
            .then(function(result) {
                vm.is_owner = result.is_owner;
                if (vm.is_owner || $rootScope.user.getId() === parseInt($stateParams.user_id)) {
                    API.job.applications(job.getId(), $stateParams.user_id)
                        .then(function(applications) {
                            vm.appliedShifts = _.map(applications, function(item) {
                                item.shift = _.find(job.getShifts(), function(s) {
                                    return s.getId() === item.application.getShiftId();
                                });
                                return item;
                            });
                        })
                        .catch(function(error) {
                            throw error;
                        });

                }
            })
            .catch(function(error) {
                throw error;
            });
        vm.acceptSelectedShift = function(appliedShifts) {
            vm.inProgress = true;
            var data = [];
            _.forEach(appliedShifts, function(item) {
                data.push({
                    shift_id: item.application.getShiftId(),
                    accept: item.application.accept,
                    complete: item.application.complete,
                });
            });
            API.job.changeApplicationStatus(job.getId(), $stateParams.user_id, data)
                .then(function(applications) {
                    vm.inProgress = false;
                    Notification.show('success', 'Success', 3000);
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        vm.markCompleteAcceptedShift = function(appliedShifts) {
            var hasAcceptedItem = false;
            vm.inMarkCompetedProgress = true;
            appliedShifts = _.map(appliedShifts, function(item) {
                if (item.application.accept) {
                    hasAcceptedItem = true;
                    item.application.complete = true;
                }
                return item;
            });
            if (hasAcceptedItem) {
                var data = [];
                _.forEach(appliedShifts, function(item) {
                    data.push({
                        shift_id: item.application.getShiftId(),
                        accept: item.application.accept,
                        complete: item.application.complete,
                    });
                });
                API.job.changeApplicationStatus(job.getId(), $stateParams.user_id, data)
                    .then(function(response) {
                        API.job.changeJobStatus(job.getId(), 1)
                            .then(function() {
                                vm.inMarkCompetedProgress = false;
                                vm.job.complete = true;
                                Notification.show('success', 'Success', 3000);
                            })
                            .catch(function(error) {
                                vm.inMarkCompetedProgress = false;
                                throw error;
                            });
                    })
                    .catch(function(error) {
                        vm.inMarkCompetedProgress = false;
                        throw error;
                    });
            } else {
                vm.inMarkCompetedProgress = false;
                throw {
                    message: 'Please select a shift'
                };
            }
        };

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Search',
            href: $state.href('app.search.sidebar.all'),
        }, {
            href: $state.href('app.job', {
                slug: vm.job.getSlug()
            }),
            name: vm.job.getTitle(),
        }, {
            name: 'Application',
        }];
    }

})(angular.module('app.components.application', [
    'app.components.application.router'
]));
