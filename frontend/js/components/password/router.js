(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.password', {
                url: '/password',
                views: {
                    'content@user': {
                        template: '<div ui-view="password"></div>',
                    }
                }
            })
            .state('user.password.forgot', {
                url: '/forgot',
                views: {
                    'password@user.password': {
                        templateUrl: "/frontend/js/components/password/forgot.tpl.html",
                        controller: 'PasswordCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('user.password.reset', {
                url: '/reset?email&token',
                views: {
                    'password@user.password': {
                        templateUrl: "/frontend/js/components/password/reset.tpl.html",
                        controller: 'PasswordCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.password.router', []));
