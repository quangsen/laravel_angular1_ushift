require('./router');
(function(app) {
    app.controller('PasswordCtrl', PasswordCtrl);
    PasswordCtrl.$inject = ['$stateParams', '$cookieStore', '$state', '$rootScope', 'API', 'Notification', 'Modal'];

    function PasswordCtrl($stateParams, $cookieStore, $state, $rootScope, API, Notification, Modal) {
        var vm = this;
        $rootScope.title = "Forgot your password";
        if (!_.isUndefined($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
            API.user.profile()
                .then(function(user) {
                    var content = 'You have logged in as ' + user.getUsername();
                    Modal.confirm({
                        title: 'You are already logged in',
                        textContent: content,
                        ok: 'LOG OUT',
                        cancel: 'GO TO HOME PAGE',
                    }, function() {
                        $rootScope.logout();
                    }, function() {
                        document.location.href = BASE_URL;
                    });
                })
                .catch(function(error) {

                });
        }
        vm.forgot = function(email) {
            vm.inProgress = true;
            API.user.forgot_password(email)
                .then(function(result) {
                    vm.inProgress = false;
                    Notification.show('success', 'An email was sent to ' + email);
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };
        vm.reset = function(password, password_confirmation) {
            var data = {
                email: $stateParams.email,
                token: $stateParams.token,
                password: password,
                password_confirmation: password_confirmation
            };
            vm.inProgress = true;
            API.user.reset_password(data)
                .then(function(result) {
                    vm.inProgress = false;
                    $cookieStore.put(JWT_TOKEN_COOKIE_KEY, result.token);
                    API.user.profile()
                        .then(function(response) {
                            $rootScope.user = response;
                        })
                        .catch(function(error) {
                            throw error;
                        });
                    Modal.confirm({
                        title: 'Your password has been reset successful',
                        textContent: '',
                        ok: 'Go to homepage',
                        cancel: 'Go to search page',
                    }, function() {
                        $state.go('app.homepage');
                    }, function() {
                        $state.go('app.search.sidebar.all');
                    });
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };
    }
})(angular.module('app.components.password', [
    'app.components.password.router'
]));
