require('./home/');
require('./login/');
require('./register/');
require('./search/');
require('./search/');
require('./profile/');
require('./job/');
require('./password/');
require('./create-job/');
require('./update-job/');
require('./verify/');
require('./inbox/');
require('./application/');
require('./dashboard/');

(function(app) {

})(angular.module('app.components', [
    'app.components.home',
    'app.components.login',
    'app.components.register',
    'app.components.search',
    'app.components.search',
    'app.components.password',
    'app.components.profile',
    'app.components.job',
    'app.components.create_job',
    'app.components.update_job',
    'app.components.verify',
    'app.components.inbox',
    'app.components.application',
    'app.components.dashboard',
]));
