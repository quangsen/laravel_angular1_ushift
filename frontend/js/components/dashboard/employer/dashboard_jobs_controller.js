(function(app) {
    app.controller('EmployerDashboardJobCtrl', EmployerDashboardJobCtrl);
    EmployerDashboardJobCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'user'];

    function EmployerDashboardJobCtrl($rootScope, $scope, $state, $stateParams, API, user) {
        var vm = this;
        vm.showPagination = false;
        vm.user = user;
        vm.page = 1;
        vm.per_page = 10;
        vm.action = $stateParams.action;
        $scope.$watch('vm.page', function() {
            var params = {
                page: vm.page,
                status: $stateParams.status,
                per_page: vm.per_page,
            };
            API.user.getJobsOfEmployer(user.getId(), params)
                .then(function(jobs) {
                    console.log(jobs);
                    vm.jobs = jobs;
                    if (jobs.length === vm.per_page) {
                        vm.showPagination = true;
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        });

        $rootScope.$on('DeletedJob', function(event, id) {
            _.remove(vm.jobs, function(item) {
                return item.getId() === id;
            });
        });

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Jobs - ' + $stateParams.status,
        }];

    }
})(angular.module('app.components.dashboard.employer.jobs', []));
