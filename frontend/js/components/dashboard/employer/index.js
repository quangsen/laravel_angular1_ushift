require('./dashboard_jobs_controller');
require('./dashboard_home_controller');
(function(app) {
    app.controller('EmployerDashboardCtrl', EmployerDashboardCtrl);
    EmployerDashboardCtrl.$inject = ['$rootScope', '$state', '$stateParams', 'API', 'Notification', 'Modal', 'user'];

    function EmployerDashboardCtrl($rootScope, $state, $stateParams, API, Notification, Modal, user) {

        var vm = this;
        vm.user = user;

        $rootScope.title = 'Dashboard';

        vm.employerSidebarItems = [{
            icon: 'home',
            menu: 'Home',
            state: 'app.dashboard.home',
            submenu: []
        }, {
            icon: 'add_circle_outline',
            menu: 'New Job',
            state: 'app.dashboard.new-job',
            submenu: []
        }, {
            icon: 'card_travel',
            menu: 'Jobs',
            submenu: [{
                menu: 'Active Job',
                state: 'app.dashboard.jobs',
                stateOption: {
                    status: 'active',
                    action: null
                }
            }, {
                menu: 'Today',
                state: 'app.dashboard.jobs',
                stateOption: {
                    status: 'today',
                    action: null
                }
            }, {
                menu: 'Archived',
                state: 'app.dashboard.jobs',
                stateOption: {
                    status: 'archived',
                    action: null
                }
            }, {
                menu: 'All Jobs',
                state: 'app.dashboard.jobs',
                stateOption: {
                    status: 'all',
                    action: null
                }
            }, {
                menu: 'Manage',
                state: 'app.dashboard.jobs',
                stateOption: {
                    status: 'all',
                    action: 'manage'
                }
            }]
        }, {
            icon: 'chat_bubble_outline',
            menu: 'Message',
            state: 'app.inbox',
            submenu: []
        }];
        setTimeout(function() {
            $('.collapsible').collapsible();
        }, 1000);


        vm.hasSubmenu = function(item) {
            return _.isArray(item.submenu) && item.submenu.length > 0;
        };
        if (_.isUndefined(user)) {
            Modal.alert('Login required', function() {
                $state.go('user.login');
            });
        }
    }

})(angular.module('app.components.dashboard.employer', [
    'app.components.dashboard.employer.jobs',
    'app.components.dashboard.employer.home'
]));
