(function(app) {
    app.controller('EmployerDashboardHome', EmployerDashboardHome);

    EmployerDashboardHome.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'user'];

    function EmployerDashboardHome($rootScope, $scope, $state, $stateParams, API, user) {
        var vm = this;
        var params = {
            page: 1,
            per_page: 5,
            status: 'all',
        };

        API.user.getJobsOfEmployer(user.getId(), params)
            .then(function(jobs) {
                console.log(jobs);
                vm.jobs = jobs;
            })
            .catch(function(error) {
                throw error;
            });

        API.user.profile(['role', 'employer'])
            .then(function(user) {
                vm.user = user;
                vm.completedProfile = caculateCompletedProfilePercent(user);
                console.log(user);
            })
            .catch(function(error) {
                throw error;
            });


        function caculateCompletedProfilePercent(user) {
            var completed = 0;
            var total = 0;
            for (var k in user) {
                if (_.isString(user[k]) && k !== 'summary') {
                    total++;
                    if (!_.isUndefined(user[k]) && !_.isNil(user[k]) && user[k] !== '') {
                        completed++;
                    }
                }
            }
            var employer = user.employer;
            for (k in employer) {
                if (_.isString(employer[k])) {
                    total++;
                    if (!_.isUndefined(employer[k]) && !_.isNil(employer[k]) && employer[k] !== '') {
                        completed++;
                    }
                }
            }
            return Math.round(completed / total * 100);
        }

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Home',
        }];
    }
})(angular.module('app.components.dashboard.employer.home', []));
