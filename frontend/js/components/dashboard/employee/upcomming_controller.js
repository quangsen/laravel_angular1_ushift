(function(app) {
    app.controller('EmployeeUpcommingShiftCtrl', EmployeeUpcommingShiftCtrl);
    EmployeeUpcommingShiftCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', 'user'];

    function EmployeeUpcommingShiftCtrl($rootScope, $scope, $stateParams, API, Notification, user) {
        var vm = this;
        vm.user = user;
        vm.page = $stateParams.page || 1;

        var isFirstTime = true;

        $scope.$watch('vm.page', function() {
            var params = {
                period: 'upcomming',
                page: vm.page,
                accept: true,
                complete: false
            };
            API.user.appliedShifts(user.getId(), params)
                .then(function(shifts) {
                    console.log(shifts);
                    vm.shifts = shifts;
                    if (isFirstTime) {
                        isFirstTime = false;
                    } else {
                        $rootScope.scrollTop();
                    }
                })
                .catch(function(error) {
                    throw errro;
                });
        });
    }
})(angular.module('app.components.dashboard.employee.upcomming', []));
