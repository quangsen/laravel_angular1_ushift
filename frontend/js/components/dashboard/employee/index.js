require('./dashboard_jobs_controller');
require('./upcomming_controller');

(function(app) {
    app.controller('EmployeeDashboardCtrl', EmployeeDashboardCtrl);
    EmployeeDashboardCtrl.$inject = ['$rootScope', '$state', '$stateParams', 'API', 'Notification', 'Modal', 'user'];

    function EmployeeDashboardCtrl($rootScope, $state, $stateParams, API, Notification, Modal, user) {

        var vm = this;

        vm.user = user;

        $state.go('app.dashboard.jobs', {
            status: "pending"
        });
        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
        }];
        $rootScope.title = 'Dashboard';

    }
})(angular.module('app.components.dashboard.employee', [
    'app.components.dashboard.employee.jobs',
    'app.components.dashboard.employee.upcomming',
]));
