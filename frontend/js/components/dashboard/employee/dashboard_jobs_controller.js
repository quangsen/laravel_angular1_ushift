(function(app) {
    app.controller('EmployeeDashboardJobCtrl', EmployeeDashboardJobCtrl);
    EmployeeDashboardJobCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'user'];

    function EmployeeDashboardJobCtrl($rootScope, $scope, $state, $stateParams, API, user) {
        var vm = this;
        vm.user = user;
        vm.page = $stateParams.page || 1;
        vm.per_page = 10;

        vm.showPagination = false;

        var isFirstTime = true;

        $scope.$watch('vm.page', function() {
            var params = {
                period: 'all',
                page: vm.page,
                per_page: vm.per_page,
            };
            if ($stateParams.status === 'pending') {
                params.accept = false;
                params.complete = false;
            } else {
                params.accept = true;
                params.complete = true;
            }
            API.user.appliedShifts(user.getId(), params)
                .then(function(shifts) {
                    vm.shifts = shifts;
                    if (shifts.length === vm.per_page) {
                        vm.showPagination = true;
                    }
                    if (isFirstTime) {
                        isFirstTime = false;
                    } else {
                        $rootScope.scrollTop();
                    }
                })
                .catch(function(error) {
                    throw errro;
                });
        });
    }
})(angular.module('app.components.dashboard.employee.jobs', []));
