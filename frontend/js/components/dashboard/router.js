(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.dashboard', {
                url: '/dashboard',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/dashboard/dashboard.tpl.html",
                        controllerProvider: ['$rootScope', '$state', function($rootScope, $state) {
                            if (_.isUndefined($rootScope.user)) {
                                $state.go('user.login');
                            } else {
                                if ($rootScope.user.isEmployer()) {
                                    return 'EmployerDashboardCtrl';
                                } else {
                                    return 'EmployeeDashboardCtrl';
                                }
                            }
                        }],
                        controllerAs: 'vm',
                    },
                    'dashboard-upcomming@app.dashboard': {
                        templateUrl: "/frontend/js/components/dashboard/employee/_upcomming.tpl.html",
                        controller: "EmployeeUpcommingShiftCtrl",
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.dashboard.home', {
                url: '/home',
                views: {
                    'dashboard-content@app.dashboard': {
                        templateUrl: "/frontend/js/components/dashboard/_dashboard_home.tpl.html",
                        controllerProvider: ['$rootScope', '$state', function($rootScope, $state) {
                            if (_.isUndefined($rootScope.user)) {
                                $state.go('user.login');
                            } else {
                                if ($rootScope.user.isEmployer()) {
                                    return 'EmployerDashboardHome';
                                } else {
                                    // return 'EmployeeDashboardCtrl';
                                }
                            }
                        }],
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.dashboard.new-job', {
                url: '/new-job',
                views: {
                    'dashboard-content@app.dashboard': {
                        templateUrl: "/frontend/js/components/create-job/create.tpl.html",
                        controller: "CreateJobCtrl",
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.dashboard.inbox', {
                url: '/inbox',
                views: {
                    'dashboard-content@app.dashboard': {
                        templateUrl: "/frontend/js/components/inbox/inbox.tpl.html",
                        controller: "InboxCtrl",
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.dashboard.jobs', {
                url: '/jobs/:status?action',
                views: {
                    'dashboard-content@app.dashboard': {
                        templateUrl: "/frontend/js/components/dashboard/_dashboard_jobs.tpl.html",
                        controllerProvider: ['$rootScope', '$state', function($rootScope, $state) {
                            if (!_.isUndefined($rootScope.user)) {
                                if ($rootScope.user.isEmployer()) {
                                    return 'EmployerDashboardJobCtrl';
                                } else {
                                    return 'EmployeeDashboardJobCtrl';
                                }
                            } else {
                                $state.go('user.login');
                            }
                        }],
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.dashboard.router', []));
