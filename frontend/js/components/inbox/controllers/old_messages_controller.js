(function(app) {
    app.controller('OldMessageCtrl', OldMessageCtrl);
    OldMessageCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'user'];

    function OldMessageCtrl($rootScope, $scope, $state, $stateParams, API, user) {
        var vm = this;
        vm.page = 0;
        vm.user = user;
        $scope.$watch(function() {
            return $scope.$parent.vm.conversation;
        }, function(conversation) {
            vm.conversation = conversation;
        });

        $rootScope.$on('MessagesLoaded', function() {
            $scope.$watch('vm.page', function() {
                var min_id = _.min(_.map($scope.$parent.vm.messages, function(item) {
                    return item.getId();
                }));

                if (!_.isUndefined(min_id) && vm.page > 0) {
                    API.inbox.getOldMessage($stateParams.conversation_id, min_id, vm.page)
                        .then(function(messages) {
                            console.log(messages);
                            vm.messages = _.concat(vm.messages, messages);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        });
    }
})(angular.module('app.componenets.inbox.controllers.old_message', []));
