(function(app) {
    app.controller('MessageCtrl', MessageCtrl);
    MessageCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$interval', 'API', 'Notification', 'Modal', 'user'];

    function MessageCtrl($rootScope, $scope, $state, $stateParams, $interval, API, Notification, Modal, user) {
        var vm = this;
        vm.user = user;
        vm.page = 1;

        $rootScope.title = 'Message - ' + user.getName();

        $rootScope.$watch('conversations', function(conversations) {
            if (!_.isUndefined(conversations)) {
                vm.conversation = _.find(conversations, function(item) {
                    return item.getId() == parseInt($stateParams.conversation_id);
                });
                API.inbox.clear(vm.conversation.getId())
                    .then(function(response) {
                        clearUnread();
                        $rootScope.updateNotificationNumber();
                    })
                    .catch(function(error) {
                        throw error;
                    });
                API.job.show(vm.conversation.getJobId(), ['employer'])
                    .then(function(job) {
                        vm.job = job;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        });

        $rootScope.$$listeners.hasNewMessage = [];
        $rootScope.$on('hasNewMessage', function(event, conversation_id) {
            if (!_.isUndefined(vm.conversation) && conversation_id === vm.conversation.getId()) {
                clearUnread();
            }
        });

        function clearUnread() {
            _.map(vm.conversation.messages, function(item) {
                item.unread = false;
                return item;
            });
        }

        vm.sendKeyPress = function(event, msg) {
            if (!event.ctrlKey && event.keyCode === 13 && !_.isUndefined(msg) && msg !== '') {
                vm.send(msg);
            }
        };

        vm.send = function(msg) {
            vm.message = '';
            var data = {
                sender_id: user.getId(),
                type: 1,
                content: msg,
            };
            msg = API.inbox.createNewMessageInstance(data);
            vm.conversation.messages.unshift(msg);
            API.inbox.send($stateParams.conversation_id, msg)
                .then(function(message) {
                    vm.conversation.messages = _.map(vm.conversation.messages, function(item) {
                        if (_.isEqual(item, msg)) {
                            return message;
                        } else {
                            return item;
                        }
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        };

        vm.changeJobStatus = function(job_id, status) {
            API.job.changeJobStatus(job_id, status)
                .then(function(job) {
                    console.log(job);
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

        vm.changeApplicationStatus = function(job_id, employee_id, accept) {
            API.job.changeApplicationStatus(job_id, employee_id, accept)
                .then(function(application) {
                    console.log(application);
                    $state.reload();
                })
                .catch(function(error) {
                    throw error;
                });
        };

    }
})(angular.module('app.componenets.inbox.controllers.message', []));
