(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.inbox', {
                url: '/inbox',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/inbox/inbox.tpl.html",
                        controller: "InboxCtrl",
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.inbox.message', {
                url: '/:conversation_id',
                views: {
                    'messages@app.inbox': {
                        templateUrl: '/frontend/js/components/inbox/_messages.tpl.html',
                        controller: 'MessageCtrl',
                        controllerAs: 'vm',
                    },
                    'old-messages@app.inbox.message': {
                        templateUrl: '/frontend/js/components/inbox/_old_messages.tpl.html',
                        controller: 'OldMessageCtrl',
                        controllerAs: 'vm',
                    },
                }
            });
    }]);
})(angular.module('app.components.inbox.router', []));
