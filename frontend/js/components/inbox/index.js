require('./router');
require('./message_directive');
require('./controllers/old_messages_controller');
require('./controllers/message_controller');
(function(app) {
    app.controller('InboxCtrl', InboxCtrl);
    InboxCtrl.$inject = ['$rootScope', '$state', '$stateParams', 'API', 'Notification', 'Modal', 'user'];

    function InboxCtrl($rootScope, $state, $stateParams, API, Notification, Modal, user) {
        $rootScope.checkLogin();
        var vm = this;
        
        $rootScope.title = 'Inbox';

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Inbox',
        }];
    }

    app.directive('inbox', ['$rootScope', function($rootScope) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                var inboxHeight = $(window).height() - $('div[ui-view="header"]').height() - 90;
                $(element).find('.inbox-body').height(inboxHeight);
                $('.messages-area').height(inboxHeight - $('.message-box').height());
                $('.message-box').find('textarea').on('keyup', function() {
                    caculateMessagesHeight();
                    setTimeout(function() {
                        caculateMessagesHeight();
                    }, 200);
                });

                scope.$watch(function() {
                    return $('.messages-collection').height();
                }, function() {
                    var height = $('.messages').height();
                    if (height > 0) {
                        $('.messages-area').scrollTop(height);
                    }
                    caculateMessagesHeight();
                });

                function caculateMessagesHeight() {
                    $('.messages-area').height(inboxHeight - $('.message-box').height());
                    scrollToBottom();
                }

                function scrollToBottom() {
                    $('.messages').scrollTop($('.messages').height());
                }
            }
        };
    }]);
})(angular.module('app.components.inbox', [
    'app.components.inbox.router',
    'app.components.inbox.directives.message',
    'app.componenets.inbox.controllers.old_message',
    'app.componenets.inbox.controllers.message',
]));
