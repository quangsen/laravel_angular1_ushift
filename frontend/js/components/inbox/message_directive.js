(function(app) {
    app.directive('message', ['API', function(API) {
        return {
            restrict: 'E',
            scope: {
                message: '=',
                user: '=',
                conversation: '=',
                job: '=',
            },
            templateUrl: function(element, attrs) {
                return '/frontend/js/components/inbox/message_template/_type_' + attrs.type + '.tpl.html';
            },
            link: function(scope, element, attrs) {
                $(element).click(function() {
                    console.log(scope.message);
                });
            }
        };
    }]);
})(angular.module('app.components.inbox.directives.message', []));
