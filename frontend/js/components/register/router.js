(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.register', {
                url: '/register',
                views: {
                    'content@user': {
                        templateUrl: "/frontend/js/components/register/register.tpl.html",
                        controller: 'RegisterCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.register.router', []));
