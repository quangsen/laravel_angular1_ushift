require('./router');
(function(app) {
    app.controller('RegisterCtrl', RegisterCtrl);

    RegisterCtrl.$inject = ['$rootScope', '$state', 'API', 'Notification', '$cookieStore', 'Modal'];

    function RegisterCtrl($rootScope, $state, API, Notification, $cookieStore, Modal) {
        var vm = this;
        $rootScope.title = 'Register';

        if (!_.isUndefined($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
            API.user.profile()
                .then(function(user) {
                    var content = 'You have logged in as ' + user.getUsername();
                    Modal.confirm({
                        title: 'You are already logged in',
                        textContent: content,
                        ok: 'LOG OUT',
                        cancel: 'GO TO HOME PAGE',
                    }, function() {
                        $rootScope.logout(function() {
                            document.location.reload();
                        });
                    }, function() {
                        document.location.href = BASE_URL;
                    });
                })
                .catch(function(error) {
                    $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                });
        }

        vm.register = function(user) {
            vm.inProgress = true;
            if (user === undefined) {
                user = {};
            }
            user.role = vm.role;
            API.user.create(user)
                .then(function(result) {
                    $cookieStore.put(JWT_TOKEN_COOKIE_KEY, result.token);
                    API.user.profile(['role'])
                        .then(function(user) {
                            vm.inProgress = false;
                            $rootScope.isFirstTime = true;
                            $rootScope.user = user;
                            $rootScope.isLoggedIn = true;
                            $state.go('app.profile.update.general', {
                                user_id: 'me'
                            });
                        })
                        .catch(function(error) {
                            $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                            vm.inProgress = false;
                            throw error;
                        });
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };
    }
})(angular.module('app.components.register', ['app.components.register.router']));
