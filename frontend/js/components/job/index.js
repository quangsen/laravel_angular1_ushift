require('./router');
require('./applications_controller');
var Category = require('../../models/Category');
(function(app) {

    app.controller('JobCtrl', JobCtrl);
    JobCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'job', 'Modal', 'Notification'];

    function JobCtrl($rootScope, $scope, $state, $stateParams, API, job, Modal, Notification) {
        $rootScope.checkLogin({
            redirect: false
        });

        var vm = this;
        vm.job = job;
        console.log(job);

        if ($stateParams.showApply !== undefined && $stateParams.showApply === 'true') {
            vm.showApply = true;
        } else {
            vm.showApply = false;
        }

        $rootScope.title = _.capitalize(job.getTitle());
        var job_id = job.getId();
        API.job.getRelated(job_id)
            .then(function(result) {
                vm.jobs = result.jobs;
                _.remove(vm.jobs, function(item) {
                    return item.getId() === job.getId();
                });
                vm.pagination = result.pagination;
            })
            .catch(function(error) {
                throw error;
            });

        vm.gotoSearchPage = function(category) {
            category = new Category(category);
            if (category.getLevel() === 1) {
                $state.go('app.search.sidebar.parent_category', {
                    parent_category_slug: _.kebabCase(category.getName())
                });
            } else {
                var parent_category = new Category(_.find(job.getCategories()), function(item) {
                    return item.id === category.getParentId();
                });
                $state.go('app.search.sidebar.parent_category.child_category', {
                    parent_category_slug: _.kebabCase(parent_category.getName()),
                    child_category_slug: _.kebabCase(category.getName())
                });
            }
        };

        vm.apply = function(data) {
            var job_id = vm.job.getId();
            var user_id = $rootScope.user.getId();
            var confirmTitle, confirmContent;

            data.shift_ids = _.map(vm.job.getSelectedShifts(), function(item) {
                return item.getId();
            });

            vm.inProgress = true;
            API.job.isSentApplication(job_id, user_id)
                .then(function(result) {
                    console.log(result);
                    if (result.is_sent_application) {
                        confirmTitle = 'Do you want update your application?';
                        confirmContent = 'You have sent an application before';

                        Modal.confirm({
                            title: confirmTitle,
                            textContent: confirmContent,
                            ok: 'SEND',
                            cancle: 'CANCEL'
                        }, function() {
                            sendApplication(data, job_id);
                        }, function() {
                            vm.inProgress = false;
                        });
                    } else {
                        sendApplication(data, job_id);
                    }

                })
                .catch(function(error) {
                    throw error;
                });
        };

        function sendApplication(data, job_id) {
            API.job.apply(data, job_id)
                .then(function(application) {
                    vm.apply_form.$setPristine();
                    vm.inProgress = false;
                    $rootScope.$broadcast('UserApply');
                    Notification.show('success', 'Your application has been submitted', 3000);
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        }

        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: 'Search',
            href: $state.href('app.search.sidebar.all'),
        }, {
            name: job.getTitle(),
        }];

    }

})(angular.module('app.components.job', [
    'app.components.job.router',
    'app.components.job.applications'
]));
