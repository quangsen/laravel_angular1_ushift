(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.job', {
                url: '/job/{slug}?showApply',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/job/_job.tpl.html",
                        controller: 'JobCtrl',
                        controllerAs: 'vm',
                    },
                    'applications@app.job': {
                        templateUrl: "/frontend/js/components/job/_applications.tpl.html",
                        controller: 'JobApplicationCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    job: ['API', '$stateParams', function(API, $stateParams) {
                        return API.job.getBySlug($stateParams.slug, ['employer', 'categories']);
                    }]
                }
            });
    }]);
})(angular.module('app.components.job.router', []));
