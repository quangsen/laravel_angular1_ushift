(function(app) {
    app.controller('JobApplicationCtrl', JobApplicationCtrl);
    JobApplicationCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', '$filter', 'API', 'job', 'Modal', 'Notification'];

    function JobApplicationCtrl($rootScope, $scope, $state, $stateParams, $filter, API, job, Modal, Notification) {
        var vm = this;

        vm.job = job;

        $rootScope.$on('UserApply', function(event) {
            $state.reload();
        });

        vm.page = 1;

        // $scope.$watch('vm.page', function() {
        //     if (_.isUndefined(vm.page)) {
        //         page = 1;
        //     } else {
        //         page = vm.page;
        //     }
        //     if (!_.isUndefined(vm.applications)) {
        //         var top = $('.bid-box').offset().top;
        //         $("html, body").animate({
        //             scrollTop: top
        //         }, 500);
        //     }
        // });
        var user_id = $rootScope.user.getId();
        var job_id = job.getId();
        API.user.isOwner(user_id, job_id)
            .then(function(result) {
                vm.is_owner = result.is_owner;
                if (vm.is_owner) {
                    API.job.applications(job.getId())
                        .then(function(applications) {
                            vm.applications = groupApplicationByEmployeeId(applications);
                            console.log(vm.applications);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    if ($rootScope.user.isEmployee()) {
                        API.job.applications(job.getId(), user_id)
                            .then(function(applications) {
                                vm.applications = groupApplicationByEmployeeId(applications);
                                console.log(vm.applications);
                            })
                            .catch(function(error) {
                                throw error;
                            });
                    }
                }
            })
            .catch(function(error) {

            });

        function groupApplicationByEmployeeId(applications) {
            applications = _.map(applications, function(item) {
                item.shift = _.find(vm.job.getShifts(), function(s) {
                    return s.getId() === item.application.getShiftId();
                });
                return item;
            });

            return _.toArray(_.groupBy(applications, function(item) {
                return item.getEmployeeId();
            }));

        }

        vm.accept = function(application) {
            var modalOpts = {};
            if (application.isAccepted()) {
                modalOpts.title = 'Are you sure you would like accept this application';
                modalOpts.textContent = 'Are you sure you would like accept this application';
            } else {
                modalOpts.title = 'Are you sure you would like reject this application';
                modalOpts.textContent = 'Are you sure you would like reject this application';
            }
            Modal.confirm(modalOpts, function() {
                var job_id = job.getId();
                var employee_id = application.getEmployeeId();
                var accept = application.isAccepted();
                API.job.changeApplicationStatus(job_id, employee_id, accept)
                    .then(function(result) {
                        application = result;
                        if (accept) {
                            Notification.show('success', 'Application has been accepted', 3000);
                        } else {
                            Notification.show('success', 'Application has been rejected', 3000);
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }, function() {
                application.setAccepted(!application.isAccepted());
            });
        };

    }
})(angular.module('app.components.job.applications', []));
