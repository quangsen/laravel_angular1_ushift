(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.profile', {
                url: '/profile/{user_id}',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/profile/profile.tpl.html",
                        controller: 'ProfileCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    profile: ['$stateParams', 'API', function($stateParams, API) {
                        var user;
                        if (_.isUndefined($stateParams.user_id) || $stateParams.user_id === '' || $stateParams.user_id === 'me') {
                            user = API.user.profile(['role', 'employer', 'employee'])
                                .then(function(user) {
                                    return user;
                                })
                                .catch(function(error) {
                                    return undefined;
                                });
                        } else {
                            user = API.user.show($stateParams.user_id, ['role', 'employer', 'employee'])
                                .then(function(user) {
                                    return user;
                                })
                                .catch(function(error) {
                                    return undefined;
                                });
                        }
                        return user;
                    }],
                }
            })
            .state('app.profile.update', {
                url: '/update',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/profile/update/_update_profile.tpl.html",
                        controller: 'ProfileUpdateCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.update.general', {
                url: '/general',
                views: {
                    'update_profile_form@app.profile.update': {
                        templateUrl: "/frontend/js/components/profile/update/_general_infomation.tpl.html",
                        controller: 'ProfileUpdateGeneralCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.update.interested-in', {
                url: '/interested-in',
                views: {
                    'update_profile_form@app.profile.update': {
                        templateUrl: "/frontend/js/components/profile/update/_interested_in.tpl.html",
                        controller: 'ProfileUpdateInterestedInCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.update.last-position', {
                url: '/last-position',
                views: {
                    'update_profile_form@app.profile.update': {
                        templateUrl: "/frontend/js/components/profile/update/_update_working_experiences.tpl.html",
                        controller: 'ProfileUpdateLastPositionCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.update.company-infomation', {
                url: '/company-infomation',
                views: {
                    'update_profile_form@app.profile.update': {
                        templateUrl: "/frontend/js/components/profile/update/_company_infomation.tpl.html",
                        controller: 'ProfileUpdateCompanyInfomationCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.update.profile-created', {
                url: '/profile-created',
                views: {
                    'update_profile_form@app.profile.update': {
                        templateUrl: "/frontend/js/components/profile/update/_profile_created.tpl.html",
                        controller: 'ProfileCreated',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.profile.edit', {
                url: '/edit',
                views: {
                    'content@app': {
                        templateUrl: "/frontend/js/components/profile_edit/profile_edit.tpl.html",
                        controller: 'ProfileEditCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.profile.router', []));
