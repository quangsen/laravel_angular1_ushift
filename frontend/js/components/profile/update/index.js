require('./controllers/profile_update_general');
require('./controllers/profile_update_interested_in');
require('./controllers/profile_update_company_infomation');
require('./controllers/profile_update_last_position');
require('./controllers/profile_created_controller');
(function(app) {
    app.controller('ProfileUpdateCtrl', ProfileUpdateCtrl);
    ProfileUpdateCtrl.$inject = ['$rootScope', '$state', '$stateParams', 'API', 'Modal', 'Notification', 'profile'];

    function ProfileUpdateCtrl($rootScope, $state, $stateParams, API, Modal, Notification, profile) {
        $rootScope.checkLogin();
        var vm = this;
        vm.profile = profile;

        console.log(profile);

        vm.update = function(profile) {
            console.log(profile);
            var data = profile.getRaw();
            var user_id = profile.getId();
            if (profile.isEmployee()) {
                vm.inProgress = true;
                API.user.update_employee(user_id, data)
                    .then(function(result) {
                        console.log(result);
                        $rootScope.user = result;
                        vm.skip();
                    })
                    .catch(function(error) {
                        vm.inProgress = false;
                        throw error;
                    });
            } else {
                API.user.update_employer(user_id, data)
                    .then(function(result) {
                        console.log(result);
                        vm.skip();
                    })
                    .catch(function(error) {
                        vm.inProgress = false;
                        throw error;
                    });
            }
        };

        vm.skip = function() {
            var next;
            var params = {};
            switch ($state.current.name) {
                case 'app.profile.update.general':
                    if (vm.profile.isEmployee()) {
                        next = 'app.profile.update.interested-in';
                    } else {
                        next = 'app.profile.update.company-infomation';
                    }
                    break;
                case 'app.profile.update.interested-in':
                    next = 'app.profile.update.last-position';
                    break;
                case 'app.profile.update.company-infomation':
                    next = 'app.profile.update.profile-created';
                    break;
                case 'app.profile.update.last-position':
                    next = 'app.profile.update.profile-created';
                    break;
                default:
                    next = 'app.profile';
                    params.user_id = 'me';
                    break;
            }
            console.log(next);
            $state.go(next, params);
        };

        vm.previous = function() {
            var previous;
            var params = {};
            switch ($state.current.name) {
                case 'app.profile.update.company-infomation':
                    previous = 'app.profile.update.general';
                    break;
                case 'app.profile.update.interested-in':
                    previous = 'app.profile.update.general';
                    break;
                case 'app.profile.update.last-position':
                    previous = 'app.profile.update.interested-in';
                    break;
                case 'app.profile.update.profile-created':
                    if (profile.isEmployer()) {
                        previous = 'app.profile.update.company-infomation';
                    } else {
                        previous = 'app.profile.update.last-position';
                    }
                    break;
                default:
                    previous = 'app.profile';
                    params.user_id = 'me';
                    break;
            }
            $state.go(previous, params);
        };

        $rootScope.breadcrumbItems = [{
            name: 'Home',
            href: $state.href('app.homepage'),
        }, {
            name: 'Profile',
            href: $state.href('app.profile', {
                user_id: 'me'
            }),
        }, {
            name: 'Update',
        }];
    }
})(angular.module('app.components.profile.update', [
    'app.components.profile.update.general',
    'app.components.profile.update.interestedIn',
    'app.components.profile.update.companyInfomation',
    'app.components.profile.update.last-position',
    'app.components.profile.update.profile-created',
]));
