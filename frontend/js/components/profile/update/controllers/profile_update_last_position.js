var LastPosition = require('../../../../models/LastPosition');
(function(app) {
    app.controller('ProfileUpdateLastPositionCtrl', ProfileUpdateLastPositionCtrl);
    ProfileUpdateLastPositionCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', 'profile'];

    function ProfileUpdateLastPositionCtrl($rootScope, $scope, $state, $stateParams, API, Notification, profile) {
        var vm = this;
        vm.$parent = $scope.$parent.vm;
        vm.profile = profile;

        vm.last_positions = [];

        vm.today = new Date();


        API.education_level.get()
            .then(function(education_levels) {
                console.log(education_levels);
                vm.education_levels = education_levels;
                vm.profile.employee.education_level = _.find(education_levels, function(item) {
                    return item.getId() === vm.profile.employee.education_level.getId();
                });
            })
            .catch(function(error) {
                throw error;
            });
        API.working_experience.get()
            .then(function(working_experiences) {
                console.log(working_experiences);
                vm.working_experiences = working_experiences;
                vm.profile.employee.working_experience = _.find(working_experiences, function(item) {
                    return item.getId() === vm.profile.employee.working_experience.getId();
                });
            })
            .catch(function(error) {
                throw error;
            });

        vm.addLastPosition = function() {
            vm.last_positions.push(new LastPosition());
        };

        vm.deleteLastPosition = function(index, item) {
            var user_id = vm.profile.getId();
            if (item.id === undefined || item.id === '' || item.id === null) {
                vm.last_positions.splice(index, 1);
            } else {
                API.user.delete_last_position(user_id, item.id)
                    .then(function(result) {
                        vm.profile.employee.last_positions.splice(index, 1);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        vm.updateLastPosition = function(profile, last_positions) {
            if (_.isEmpty(last_positions)) {
                vm.$parent.update(profile);
            } else {
                var count = 1;
                _.each(last_positions, function(item) {
                    API.user.add_last_position(profile.getId(), item)
                        .then(function(position) {
                            console.log(position);
                            if (count == last_positions.length) {
                                vm.$parent.update(profile);
                            } else {
                                count++;
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                });
            }
        };



    }
})(angular.module('app.components.profile.update.last-position', []));
