(function(app) {
    app.controller('ProfileUpdateCompanyInfomationCtrl', ProfileUpdateCompanyInfomationCtrl);
    ProfileUpdateCompanyInfomationCtrl.$inject = ['$scope', 'profile'];

    function ProfileUpdateCompanyInfomationCtrl($scope, profile) {
        var vm = this;
        vm.$parent = $scope.$parent.vm;
        vm.profile = profile;
        vm.genders = [{
            value: 'male',
            display: 'Male'
        }, {
            value: 'female',
            display: 'Female'
        }];
    }
})(angular.module('app.components.profile.update.companyInfomation', []));
