(function(app) {
    app.controller('ProfileCreated', ProfileCreated);
    ProfileCreated.$inject = ['$scope', 'profile'];

    function ProfileCreated($scope, profile) {
        var vm = this;
        vm.profile = profile;
        if (profile.isEmployee()) {
            vm.message = 'Thousand jobs available';
        } else {
            vm.message = 'Fill your job in a minute.';
        }
    }
})(angular.module('app.components.profile.update.profile-created', []));
