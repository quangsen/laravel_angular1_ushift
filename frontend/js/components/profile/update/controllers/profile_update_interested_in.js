(function(app) {
    app.controller('ProfileUpdateInterestedInCtrl', ProfileUpdateInterestedInCtrl);
    ProfileUpdateInterestedInCtrl.$inject = ['$scope', 'profile', 'API'];

    function ProfileUpdateInterestedInCtrl($scope, profile, API) {
        var vm = this;
        vm.$parent = $scope.$parent.vm;
        vm.profile = profile;
        console.log(profile);

        vm.kind_of_works = ['fulltime', 'parttime'];
        vm.kind_of_work = vm.kind_of_works[0];

        API.industry.get(['preferred_positions'])
            .then(function(industries) {
                vm.industries = industries;
                vm.profile.employee.industry = _.find(industries, function(item) {
                    return item.getId() === vm.profile.employee.industry.getId();
                });
            })
            .catch(function(error) {
                throw error;
            });
        API.preferred_location.get()
            .then(function(preferred_locations) {
                vm.preferred_locations = preferred_locations;
                if (!_.isNil(vm.profile.employee.preferred_locations) && _.isArray(vm.profile.employee.preferred_locations) && !_.isEmpty(vm.profile.employee.preferred_locations)) {
                    var v = _.filter(preferred_locations, function(item) {
                        var data = _.find(vm.profile.employee.preferred_locations, function(location) {
                            return location.id === item.id;
                        });
                        return _.isUndefined(data) ? false : true;
                    });
                    vm.profile.employee.preferred_locations = v;
                }
            })
            .catch(function(error) {
                throw error;
            });
        $scope.$watch('vm.profile.employee.industry.id', function() {
            if (!_.isNil(vm.profile.employee.industry) && !_.isNil(vm.profile.employee.industry.id)) {
                API.industry.show(vm.profile.employee.industry.getId(), ['preferred_positions'])
                    .then(function(industry) {
                        vm.preferred_positions = industry.getPreferredPositions();
                        vm.profile.employee.preferred_positions[0] = _.find(vm.preferred_positions, function(position) {
                            if (!_.isUndefined(vm.profile.employee.preferred_positions[0])) {
                                return position.getId() === vm.profile.employee.preferred_positions[0].getId();
                            } else {
                                return false;
                            }
                        });
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        });

    }
})(angular.module('app.components.profile.update.interestedIn', []));
