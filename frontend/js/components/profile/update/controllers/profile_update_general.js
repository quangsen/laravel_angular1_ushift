(function(app) {
    app.controller('ProfileUpdateGeneralCtrl', ProfileUpdateGeneralCtrl);
    ProfileUpdateGeneralCtrl.$inject = ['$rootScope', '$scope', 'profile'];

    function ProfileUpdateGeneralCtrl($rootScope, $scope, profile) {
        var vm = this;
        vm.$parent = $scope.$parent.vm;
        vm.profile = profile;
        vm.genders = [{
            value: 'male',
            display: 'Male'
        }, {
            value: 'female',
            display: 'Female'
        }];

        var allowedImageExtension = [
            'jpg',
            'jpeg',
            'gif',
            'png'
        ];


        vm.beforeUploadAvatar = function(file) {
            var extensionFile = $rootScope.getExtensionFile(file.name);
            if (extensionFile !== undefined) {
                if (allowedImageExtension.indexOf(extensionFile[0]) < 0) {
                    vm.copyImageName = angular.copy(vm.profile.image);
                    vm.profile.image = file.name;
                    $scope.$apply();
                    throw "Please upload valid image type";
                }
            }
        };
        vm.uploadAvatar = function(response) {
            vm.checkAvatar = true;
            vm.profile.image = response.path;
            $scope.$apply();
        };

        vm.beforeUploadCover = function(file) {
            var extensionFile = $rootScope.getExtensionFile(file.name);
            if (extensionFile !== undefined) {
                if (allowedImageExtension.indexOf(extensionFile[0]) < 0) {
                    vm.copyImageName = angular.copy(vm.profile.image);
                    vm.profile.image = file.name;
                    $scope.$apply();
                    throw "Please upload valid image type";
                }
            }
        };
        vm.uploadCover = function(response) {
            vm.checkAvatar = true;
            vm.profile.cover = response.path;
            $scope.$apply();
        };

    }
})(angular.module('app.components.profile.update.general', []));
