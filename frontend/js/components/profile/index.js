require('./router');
require('./update/');
(function(app) {

    app.controller('ProfileCtrl', ProfileCtrl);
    ProfileCtrl.$inject = ['$rootScope', '$scope', 'API', '$stateParams', '$state', 'profile'];

    function ProfileCtrl($rootScope, $scope, API, $stateParams, $state, profile) {
        $rootScope.checkLogin();

        $rootScope.title = profile.getName();

        var vm = this;

        var isFirstTime = true;

        vm.profile = profile;
        console.log(vm.profile);
        if (profile.isEmployee()) {
            vm.page = 1;
            $scope.$watch('vm.page', function() {
                API.user.appliedShifts(profile.getId(), vm.page, 'completed', 'past')
                    .then(function(shifts) {
                        console.log(shifts);
                        vm.shifts = shifts;
                        if (isFirstTime) {
                            isFirstTime = false;
                        } else {
                            $rootScope.scrollTop();
                        }
                    })
                    .catch(function(error) {
                        throw errro;
                    });
            });
        }
        if (profile.isEmployer()) {
            vm.page = 1;
            var per_page = 3;
            $scope.$watch('vm.page', function() {
                var params = {
                    status: 'completed',
                    page: vm.page,
                    per_page: per_page,
                };
                API.user.getJobsOfEmployer(profile.getId(), params)
                    .then(function(jobs) {
                        console.log(jobs);
                        vm.jobs = jobs;
                    })
                    .catch(function(error) {
                        throw error;
                    });
            });
        }
        /*
         * breadcrumb
         */
        $rootScope.breadcrumbItems = [{
            name: 'Dashboard',
            href: $state.href('app.dashboard'),
        }, {
            name: vm.profile.getUsername(),
        }];
    }

})(angular.module('app.components.profile', [
    'app.components.profile.router',
    'app.components.profile.update',
]));
