var $ = window.$ = require('jquery');
var jQuery = window.jQuery = require('jquery');
var moment = window.moment = require('moment');
require('angular');
require('lodash');
require('angular-cookies');
require('angular-resource');
require('materialize');
require('angular-material');
require('angular-aria');
require('angular-filter');
require('angular-aminate');
require('angular-sanitize');
require('material-datetimepicker');
require('angular-jwt');
require('angular-ui-mask');
require('trix');
require('angular-trix');
require('ngPickadate');
require('./config');
require('./routes');
require('./templates');
require('./components/');
require('./api/');
require('./common/layouts-js/');
require('./common/services/');
require('./common/filters/');
require('./common/directives/');
require('./common/exception');
(function(app) {
    app.run(['$rootScope', '$cookieStore', '$state', '$stateParams', '$http', 'API', '$window', '$interval', 'Notification', 'Modal',
        function($rootScope, $cookieStore, $state, $stateParams, $http, API, $window, $interval, Notification, Modal) {
            console.log($cookieStore.get(JWT_TOKEN_COOKIE_KEY));
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            /*
             * Global asset function
             *
             * @return absolute path of a resource
             */
            $rootScope.asset = function(path) {
                if (path.charAt(0) == '/') {
                    return BASE_URL + path.substr(1);
                } else {
                    return BASE_URL + path;
                }
            };

            $rootScope.checkLogin = function(options) {
                if (_.isUndefined($cookieStore.get(JWT_TOKEN_COOKIE_KEY)) || _.isUndefined($rootScope.user)) {
                    // Modal.alert('You need to logged in to access this page', function() {
                    //     $rootScope.gotoLoginPage(options);
                    // });
                    $state.go('user.login');
                }
            };

            /*
             * Global function to go to login page
             */
            $rootScope.gotoLoginPage = function(options) {
                options = options || {};
                if (options.redirect === false) {
                    $state.go('user.login');
                } else {
                    $state.go('user.login', {
                        redirect: 'back'
                    });
                }
            };

            /*
             * Global function to logout
             */
            $rootScope.logout = function(callback) {
                API.auth.logout()
                    .then(function() {
                        $cookieStore.remove(JWT_TOKEN_COOKIE_KEY);
                        if (_.isFunction(callback)) {
                            callback();
                        } else {
                            $state.go('user.login');
                            document.location.reload();
                        }
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /*
             * Global function to init conversation
             */
            $rootScope.initConversation = function(receiver_id, job_id) {
                var params = {
                    include: 'employee'
                };
                API.inbox.initConversation(receiver_id, params, job_id)
                    .then(function(conversation) {
                        var check = _.find($rootScope.conversations, function(item) {
                            return item.getId() === conversation.getId();
                        });
                        if (_.isUndefined(check)) {
                            $rootScope.conversations.unshift(conversation);
                        }
                        $state.go('app.inbox.message', {
                            conversation_id: conversation.getId()
                        });
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            $rootScope.dashboardActiveTab = 'pendingTab';

            /*
             * scroll to top every time $state was changed
             */
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                Notification.remove();
                $rootScope.toState = toState;
                $rootScope.toParams = toParams;
                $rootScope.fromState = fromState;
                $rootScope.fromParams = fromParams;
            });

            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.scrollTop();
            });

            $rootScope.scrollTop = function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
            };

            $rootScope.allowResumeExtension = [
                'doc',
                'docx',
                'pdf'
            ];

            $rootScope.getExtensionFile = function(filename) {
                return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
            };

            $(window).scroll(function() {
                if ($(this).scrollTop() > 150) {
                    $('.scrollup').fadeIn();
                } else {
                    $('.scrollup').fadeOut();
                }
            });
        }

    ]);


    app.controller('MainCtrl', MainCtrl);
    MainCtrl.$inject = ['$rootScope', '$state', 'Notification', '$mdDialog', '$interval', 'API', 'user'];

    function MainCtrl($rootScope, $state, Notification, $mdDialog, $interval, API, user) {
        if (!$rootScope.isFirstTime) {
            if (!_.isUndefined($rootScope.user) && !$rootScope.user.getEmailVerified()) {
                Notification.show('warning', 'Please verify your email', 5000);
            }

            if (!_.isUndefined($rootScope.user) && $rootScope.user.isEmployee() && $rootScope.user.getEmailVerified() && !$rootScope.user.isUpdatedProfile()) {
                Notification.show('warning', 'Please update your profile', 5000);
            }
        }

        if (!_.isUndefined(user)) {
            if (user.isEmployer()) {
                include = ['employee'];
            }
            if (user.isEmployee()) {
                include = ['employer'];
            }
            API.inbox.get(include)
                .then(function(conversations) {
                    $rootScope.conversations = conversations;
                    checkNewMessage($rootScope.conversations);
                })
                .catch(function(error) {
                    console.log(error);
                });
            $interval(function() {
                checkNewMessage($rootScope.conversations);
            }, 10000);

            $interval(function() {
                API.inbox.get(include)
                    .then(function(conversations) {
                        _.forEach(conversations, function(conversation) {
                            if (_.isUndefined(_.find($rootScope.conversations, function(c) {
                                    return c.getId() === conversation.getId();
                                }))) {
                                $rootScope.conversations.push(conversation);
                            }
                        });
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }, 15000);
        }

        function checkNewMessage(conversations) {
            _.each(conversations, function(conversation) {
                if (!_.isUndefined(conversation.getMessages()) && !_.isUndefined(conversation.getMessages()[0])) {
                    params = {
                        last_id: conversation.getMessages()[0].getId(),
                    };
                } else {
                    params = {
                        last_id: 0,
                    };
                }
                API.inbox.getNewMessage(conversation.getId(), params)
                    .then(function(messages) {
                        if (!_.isUndefined(messages)) {
                            _.each(messages, function(item) {
                                if (_.isUndefined(_.find(conversation.messages, function(i) {
                                        return i.getId() === item.getId();
                                    }))) {
                                    conversation.messages.unshift(item);
                                    $rootScope.$broadcast('hasNewMessage', conversation.getId());
                                }
                            });
                        }
                        $rootScope.updateNotificationNumber();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            });
        }
        $rootScope.updateNotificationNumber = function() {
            var count = _.countBy($rootScope.conversations, function(item) {
                return item.hasNewMessage($rootScope.user.getId());
            });
            $rootScope.unread = count.true;
        };
        $rootScope.clearUnread = function(conversation_id) {
            conversation = _.find($rootScope.conversations, function(item) {
                return item.getId() === conversation_id;
            });
            API.inbox.clear(conversation_id)
                .then(function(response) {
                    conversation.messages = _.map(conversation.messages, function(item) {
                        item.unread = false;
                        return item;
                    });
                    $rootScope.updateNotificationNumber();
                })
                .catch(function(error) {
                    throw error;
                });
        };
    }
})(angular.module('app', [
    'app.config',
    'app.router',
    'ngCookies',
    'ngResource',
    'ngMaterial',
    'ngMaterialDatePicker',
    'ngSanitize',
    'angular.filter',
    'ngAnimate',
    'ui.mask',
    'angularTrix',
    'pickadate',
    'app.template',
    'app.components',
    'app.common.directives',
    'app.api',
    'app.common.layouts-js',
    'app.common.services',
    'app.common.exceptionHandler',
    'app.common.filters',
]));
