(function(app) {

    app.controller('HeaderCtrl', HeaderCtrl);
    HeaderCtrl.$inject = ['$rootScope', '$scope', 'API', '$stateParams'];

    function HeaderCtrl($rootScope, $scope, API, $stateParams) {
        var vm = this;
        $scope.$watch('user', function(user) {
        	if (user !== undefined) {
        		vm.user = user;
        	}
        });
    }

})(angular.module('app.common.layouts-js.header', ['app.template']));
