(function(app) {
    app.directive('userProfileImage', ['$rootScope', function($rootScope) {
        return {
            strict: 'E',
            scope: {
                image: '=',
            },
            template: '<div class="user-profile-image"><img src="/assets/images/users/avatar-default.png" style="background-image: url(\'{{image}}\')" alt="user profile image" /></div>',
            link: function(scope, element, attrs) {}
        };
    }]);
})(angular.module('app.common.directives.userProfileImage', []));
