(function(app) {

    app.directive('pagination', ['$rootScope', '$state', function($rootScope, $state) {
        return {
            restrict: 'E',
            templateUrl: '/frontend/js/common/partials/pagination.tpl.html',
            scope: {
                data: '='
            },
            link: function(scope, elememt, attrs) {
                scope.state = $state;
                scope.goto = function(page) {
                    if (parseInt($state.params.page) === parseInt(page.number)) {
                        return false;
                    }
                    var params = $state.params;
                    params.page = page.number;
                    $state.go($state.current.name, params, { reload: true });
                };
                scope.$watch('data', function(data) {
                    if (data !== undefined) {
                        scope.currentPage = {};
                        // if (data.getCurrentPage() < data.getTotalPages()) {
                        //     scope.currentPage.number = data.getCurrentPage();
                        // } else {
                        //     scope.currentPage.number = data.getTotalPages();
                        // }
                        scope.currentPage.number = data.getCurrentPage();
                        
                        if (data.getCurrentPage() > 1) {
                            scope.firstPage = {};
                            scope.firstPage.number = 1;

                            scope.prevPage = {};
                            scope.prevPage.number = data.getCurrentPage() - 1;
                        } else {
                            scope.firstPage = undefined;
                            scope.prevPage = undefined;
                        }
                        if (data.getCurrentPage() < data.getTotalPages()) {
                            scope.lastPage = {};
                            scope.lastPage.number = data.total_pages;

                            scope.nextPage = {};
                            scope.nextPage.number = data.getCurrentPage() + 1;
                        } else {
                            scope.lastPage = undefined;
                            scope.nextPage = undefined;
                        }
                    }
                });
            }
        };
    }]);

})(angular.module('app.common.directives.pagination', []));
