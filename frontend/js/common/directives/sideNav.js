(function(app) {
    app.directive('sideNav', ['$rootScope', '$state', function($rootScope, $state) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $('.button-collapse').sideNav();
                $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                    $('.button-collapse').sideNav('hide');
                });
            }
        };
    }]);
})(angular.module('app.common.directives.sideNav', []));
