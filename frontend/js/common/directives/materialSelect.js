(function(app) {
    app.directive('materialSelect', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(element).material_select();
            }
        };
    });
})(angular.module('app.common.directives.materialSelect', []));
