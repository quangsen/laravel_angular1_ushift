(function(app) {
    app.directive('job', ['$rootScope', 'API', 'Modal', function($rootScope, API, Modal) {
        return {
            restrict: 'E',
            templateUrl: '/frontend/js/components/job/_job_item.tpl.html',
            scope: {
                item: '=',
                action: '=',
            },
            link: function(scope, element, attrs) {
                scope.inProgress = false;
                scope.inMarkCompletedProgress = false;
                scope.delete = function(id) {
                    Modal.confirm('Are you sure you want to delete this job?', function() {
                        scope.inProgress = true;
                        API.job.delete(id)
                            .then(function(response) {
                                scope.inProgress = false;
                                $rootScope.$broadcast('DeletedJob', id);
                            })
                            .catch(function(error) {
                                scope.inProgress = false;
                                throw error;
                            });
                    });
                };
                scope.complete = function(id) {
                    Modal.confirm('Are you sure you want to complete this Job?', function() {
                        scope.inMarkCompletedProgress = true;
                        API.job.changeJobStatus(id, 1)
                            .then(function(job) {
                                console.log(job);
                                scope.item = job;
                                scope.inMarkCompletedProgress = false;
                            })
                            .catch(function(error) {
                                scope.inMarkCompletedProgress = false;
                                throw error;
                            });
                    });
                };
            }
        };
    }]);
})(angular.module('app.common.directives.job', []));
