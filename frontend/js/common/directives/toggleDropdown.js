(function(app) {
    app.directive('toggleDropdown', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                // $('.user-avatar').click(function() {
                //     $('.user-dropdown').toggleClass('active');
                // });
                $(element).click(function(event) {
                    $('#' + attrs.toggleDropdown).toggleClass('active');
                    event.stopPropagation();
                });
                $('body').click(function(event) {
                    if (!$(event.target).parents('#' + attrs.toggleDropdown).length) {
                        $('#' + attrs.toggleDropdown).removeClass('active');
                    }
                });
                $rootScope.$on('$stateChangeStart', function(event) {
                    $('#' + attrs.toggleDropdown).removeClass('active');
                });
            }
        };
    }]);
})(angular.module('app.common.directives.toggleDropdown', []));
