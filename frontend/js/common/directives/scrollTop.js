(function(app) {

    app.directive('scrollTop', function() {
        return {
            restrict: 'AE',
            scope: false,
            link: function(scope, element, attrs) {
                $(element).click(function() {
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                });
            }
        };
    });

})(angular.module('app.common.directives.scroll-top', []));
