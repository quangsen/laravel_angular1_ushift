(function(app) {
    app.directive('footer', ['$http', function($http) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                if ($(window).width() > 768) {
                    scope.$watch(function() {
                        return $('div[ui-view="content"]').height();
                    }, function(height) {
                        footerFixBottom();
                        scope.$watch(function() {
                            return $http.pendingRequests.length > 0;
                        }, function(hasPending) {
                            footerFixBottom();
                        });
                    });

                }

                function footerFixBottom() {
                    if ($(window).height() - $('div[ui-view="header"]').height() - $('div[ui-view="content"]').height() - $(element).height() - 50 > 0) {
                        $('.wrapper').addClass('full-height');
                        $(element).addClass('bottom-footer');
                    } else {
                        $('.wrapper').removeClass('full-height');
                        $(element).removeClass('bottom-footer');
                    }
                }
            },
        };
    }]);
})(angular.module('app.common.directives.footer', []));
