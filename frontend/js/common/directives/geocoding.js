(function(app) {
    app.directive('geocoding', ['$window', function($window) {
        return {
            restrict: 'E',
            scope: {
                address: '=',
                lat: '=',
                lng: '=',
                defaultLat: '=',
                defaultLng: '=',
            },
            template: '<div id="nf-geocoding-map" class="nf-geocoding"></div>',
            link: function(scope, element, attrs) {
                var map;
                if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                    CreateMap(scope.defaultLat, scope.defaultLng);
                } else {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    script.id = 'gmap-sdk';
                    script.src = "//maps.google.com/maps/api/js?key=" + GOOGLE_MAP_API_KEY + "&callback=CreateMap";
                    document.body.appendChild(script);
                }
                $window.CreateMap = function() {
                    var lat = scope.lat || scope.defaultLat;
                    var lng = scope.lng || scope.defaultLng;
                    CreateMap(lat, lng);
                };

                scope.$watch('lat', function(lat) {
                    if (!_.isUndefined($window.google) && !_.isUndefined(lat) && !_.isUndefined(scope.lng)) {
                        CreateMap(lat, scope.lng);
                    }
                });
                scope.$watch('lng', function(lng) {
                    if (!_.isUndefined($window.google) && !_.isUndefined(lng) && !_.isUndefined(scope.lat)) {
                        CreateMap(scope.lat, lng);
                    }
                });

                function CreateMap(lat, lng) {
                    var myLatlng = new google.maps.LatLng(lat, lng);
                    var mapOptions = {
                        zoom: 14,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                    };
                    map = new google.maps.Map(document.getElementById('nf-geocoding-map'), mapOptions);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                    });
                }

                scope.$watch('address', function(address) {
                    if (!_.isUndefined(address) && !_.isUndefined(map)) {
                        codeAddress(address);
                    }
                });


                function codeAddress(address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'address': address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                            var latlng = results[0].geometry.location.toJSON();
                            scope.lat = latlng.lat;
                            scope.lng = latlng.lng;
                        } else {
                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });
                }
            }
        };
    }]);
})(angular.module('app.common.directives.geocoding', []));
