(function(app) {
    app.directive('forceReload', function() {
        return {
            restrict: 'A',
            scope: true,
            link: function(scope, element) {
                $(element).click(function() {
                	console.log(element);
                    document.location.href = $(element).attr('href');
                });
            }
        };
    });
})(angular.module('app.common.directives.forceReload', []));
