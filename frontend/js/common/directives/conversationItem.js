(function(app) {
    app.directive('conversationItem', ['$rootScope', function($rootScope) {
        return {
            restrict: 'E',
            scope: {
                conversation: '=',
                type: '@',
            },
            templateUrl: '/frontend/js/common/partials/_conversation_item.tpl.html',
            link: function(scope, element, attrs) {
                scope.user = $rootScope.user;
                scope.lastMessage = scope.conversation.messages[0];
            }
        };
    }]);
})(angular.module('app.common.directives.conversationItem', []));
