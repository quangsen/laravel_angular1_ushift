(function(app) {
    app.service('Modal', ['$mdDialog', function($mdDialog) {
        var STR_OK = 'OK';
        var STR_CANCEL = 'Cancel';
        this.alert = function() {
            var options, callable;
            if (arguments.length === 0) {
                throw "Modal alert need at least 1 param";
            } else {
                options = _.isObject(arguments[0]) ? arguments[0] : { textContent: _.toString(arguments[0]) };
                callable = _.isFunction(arguments[1]) ? arguments[1] : undefined;
            }
            if (options.ok === undefined) {
                options.ok = STR_OK;
            }
            var alert = $mdDialog.alert(options);
            $mdDialog.show(alert)
                .then(function() {
                    if (callable !== undefined) {
                        callable();
                    }
                });
        };

        this.confirm = function() {
            var options, ok_callable, cancel_callable;
            if (arguments.length === 0) {
                throw "Modal confirm need at least 1 param";
            } else {
                options = _.isObject(arguments[0]) ? arguments[0] : { textContent: _.toString(arguments[0]) };
                ok_callable = _.isFunction(arguments[1]) ? arguments[1] : undefined;
                cancel_callable = _.isFunction(arguments[2]) ? arguments[2] : undefined;
            }
            if (options.ok === undefined) {
                options.ok = STR_OK;
            }
            if (options.cancel === undefined) {
                options.cancel = STR_CANCEL;
            }
            var confirm = $mdDialog.confirm(options);
            $mdDialog.show(confirm)
                .then(function() {
                    if (ok_callable !== undefined) {
                        ok_callable();
                    }
                })
                .catch(function() {
                    if (cancel_callable !== undefined) {
                        cancel_callable();
                    }
                });
        };

    }]);
})(angular.module('app.common.services.Modal', []));
