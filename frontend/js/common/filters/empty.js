(function(app) {
    app.filter('empty', function() {
        return function(entity) {
            if (Array.isArray(entity) && entity.length === 0) {
                return true;
            } else {
                return false;
            }
        };
    });
})(angular.module('app.common.filters.empty', []));
