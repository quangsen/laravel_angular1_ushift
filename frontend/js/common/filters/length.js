(function(app) {
    app.filter('length', function() {
        return function(array) {
            if (_.isArray(array)) {
                return array.length;
            }
        };
    });
})(angular.module('app.common.filters.length', []));
