(function(app) {
    app.filter('toString', function() {
        return function(entity) {
            if (_.isString(entity)) {
                return entity;
            } else if (_.isArray(entity) || _.isObject(entity)) {
                return JSON.stringify(entity);
            } else {
                return "can't convert";
            }
        };
    });
})(angular.module('app.common.filters.toString', []));
