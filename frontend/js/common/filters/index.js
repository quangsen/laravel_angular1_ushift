require('./hasItem');
require('./range');
require('./timeFormat');
require('./trustAsHtml');
require('./toString');
require('./length');
require('./empty');

(function(app) {

})(angular.module('app.common.filters', [
    'app.common.filters.hasItem',
    'app.common.filters.range',
    'app.common.filters.timeFormat',
    'app.common.filters.trustAsHtml',
    'app.common.filters.toString',
    'app.common.filters.length',
    'app.common.filters.empty',
]));
