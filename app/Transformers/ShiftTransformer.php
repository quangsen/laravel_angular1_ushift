<?php

namespace App\Transformers;

use App\Entities\Shift;
use App\Transformers\ApplicationTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ShiftTransformer
 * @package namespace App\Transformers;
 */
class ShiftTransformer extends TransformerAbstract
{

    protected $defaultIncludes = ['users'];

    /**
     * Transform the \Shift entity
     * @param \Shift $model
     *
     * @return array
     */
    public function transform(Shift $model)
    {
        return [
            'id'          => $model->id,
            'job_id'      => $model->job_id,
            'salary'      => $model->salary,
            'description' => $model->description,
            'type'        => $model->type,
            'start_time'  => $model->start_time,
            'end_time'    => $model->end_time,
            'timestamps'  => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }

    public function includeUsers(Shift $model)
    {
        return $this->collection($model->users, new ApplicationTransformer);
    }
}
