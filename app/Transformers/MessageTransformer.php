<?php

namespace App\Transformers;

use App\Entities\Message;
use League\Fractal\TransformerAbstract;

/**
 * Class MessageTransformer
 * @package namespace App\Transformers;
 */
class MessageTransformer extends TransformerAbstract
{

    /**
     * Transform the \Message entity
     * @param \Message $model
     *
     * @return array
     */
    public function transform(Message $model)
    {
        return [
            'id'         => (int) $model->id,
            'content'    => $model->content,
            'type'       => (int) $model->type,
            'sender_id'  => (int) $model->sender_id,
            'unread'     => $model->unread == 1 ? true : false,
            'timestamps' => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }
}
