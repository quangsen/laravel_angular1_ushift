<?php

namespace App\Transformers;

use App\Entities\Employer;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployerTransformer
 * @package namespace App\Transformers;
 */
class EmployerTransformer extends TransformerAbstract
{

    /**
     * Transform the \Employer entity
     * @param \Employer $model
     *
     * @return array
     */
    public function transform(Employer $model)
    {
        return [
            'company_name'             => $model->company_name,
            'company_about'             => $model->company_about,
            'registed_number'          => $model->registed_number,
            'company_address'          => $model->company_address,
            'number_of_outlets'        => $model->number_of_outlets,
            'company_main_activity'    => $model->company_main_activity,
            'preferred_contact_method' => $model->preferred_contact_method,
            'preferred_contact_time'   => $model->preferred_contact_time,

        ];
    }
}
