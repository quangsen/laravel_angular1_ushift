<?php

namespace App\Transformers;

use App\Entities\EducationLevel;
use League\Fractal\TransformerAbstract;

/**
 * Class EducationLevelTransformer
 * @package namespace App\Transformers;
 */
class EducationLevelTransformer extends TransformerAbstract
{

    /**
     * Transform the \EducationLevel entity
     * @param \EducationLevel $model
     *
     * @return array
     */
    public function transform(EducationLevel $model)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
        ];
    }
}
