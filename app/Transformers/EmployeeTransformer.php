<?php

namespace App\Transformers;

use App\Entities\Employee;
use App\Transformers\EducationLevelTransformer;
use App\Transformers\IndustryTransformer;
use App\Transformers\LastPositionTransformer;
use App\Transformers\PreferredLocationTransformer;
use App\Transformers\PreferredPositionTransformer;
use App\Transformers\WorkingExperienceTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeTransformer
 * @package namespace App\Transformers;
 */
class EmployeeTransformer extends TransformerAbstract {

	protected $defaultIncludes = ['industry', 'education_level', 'preferred_positions', 'preferred_locations', 'working_experience', 'last_positions'];
	/**
	 * Transform the \Employee entity
	 * @param \Employee $model
	 *
	 * @return array
	 */
	public function transform(Employee $model) {
		return [
			'allow_to_work_in_sg' => $model->allow_to_work_in_sg == 0 ? false : true,
			'kind_of_work'        => $model->kind_of_work,
			'hours_per_week'      => $model->hours_per_week,
			'current_position'    => $model->current_position,
			'resume'              => filter_var($model->resume, FILTER_VALIDATE_URL) === false ? asset($model->resume) : $model->resume,
		];
	}

	public function includeIndustry(Employee $model) {
		$industry = $model->industry;
		if ($industry) {
			return $this->item($industry, new IndustryTransformer);
		} else {
			return null;
		}
	}
	public function includeEducationLevel(Employee $model) {
		$education = $model->education;
		if ($education) {
			return $this->item($education, new EducationLevelTransformer);
		} else {
			return null;
		}
	}

	public function includePreferredPositions(Employee $model) {
		$preferred_positions = $model->preferred_positions;
		if (!empty($preferred_positions)) {
			return $this->collection($preferred_positions, new PreferredPositionTransformer);
		} else {
			return null;
		}
	}
	public function includePreferredLocations(Employee $model) {
		$preferred_locations = $model->preferred_locations;
		if (!empty($preferred_locations)) {
			return $this->collection($preferred_locations, new PreferredLocationTransformer);
		} else {
			return null;
		}
	}
	public function includeWorkingExperience(Employee $model) {
		$working_experience = $model->working_experience;
		if ($working_experience) {
			return $this->item($working_experience, new WorkingExperienceTransformer);
		} else {
			return null;
		}
	}
	public function includeLastPositions(Employee $model) {
		$last_positions = $model->last_positions;
		if (!empty($last_positions)) {
			return $this->collection($last_positions, new LastPositionTransformer);
		} else {
			return null;
		}
	}
}
