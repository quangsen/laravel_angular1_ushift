<?php

namespace App\Transformers;

use App\Entities\Industry;
use App\Transformers\PreferredPositionTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class IndustryTransformer
 * @package namespace App\Transformers;
 */
class IndustryTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['preferred_positions'];
    /**
     * Transform the \Industry entity
     * @param \Industry $model
     *
     * @return array
     */
    public function transform(Industry $model)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
        ];
    }
    public function includePreferredPositions(Industry $model)
    {
        return $this->collection($model->preferred_positions, new PreferredPositionTransformer);
    }
}
