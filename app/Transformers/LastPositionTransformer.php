<?php

namespace App\Transformers;

use App\Entities\LastPosition;
use League\Fractal\TransformerAbstract;

/**
 * Class LastPositionTransformer
 * @package namespace App\Transformers;
 */
class LastPositionTransformer extends TransformerAbstract
{

    /**
     * Transform the \LastPosition entity
     * @param \LastPosition $model
     *
     * @return array
     */
    public function transform(LastPosition $model)
    {
        return [
            'id'                => (int) $model->id,
            'employee_id'       => $model->employee_id,
            'company_name'      => $model->company_name,
            'country'           => $model->country,
            'role'              => $model->role,
            'type'              => $model->type,
            'start_date'        => $model->start_date,
            'end_date'          => $model->end_date,
            'reason_of_leaving' => $model->reason_of_leaving,
            'name_of_manager'   => $model->name_of_manager,
        ];
    }
}
