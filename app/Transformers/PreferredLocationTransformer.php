<?php

namespace App\Transformers;

use App\Entities\PreferredLocation;
use League\Fractal\TransformerAbstract;

/**
 * Class PreferredLocationTransformer
 * @package namespace App\Transformers;
 */
class PreferredLocationTransformer extends TransformerAbstract
{

    /**
     * Transform the \PreferredLocation entity
     * @param \PreferredLocation $model
     *
     * @return array
     */
    public function transform(PreferredLocation $model)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
        ];
    }
}
