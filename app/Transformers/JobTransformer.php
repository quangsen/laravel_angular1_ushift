<?php

namespace App\Transformers;

use App\Entities\Job;
use App\Transformers\CategoryTransformer;
use App\Transformers\ShiftTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class JobTransformer
 * @package namespace App\Transformers;
 */
class JobTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['shifts'];

    protected $availableIncludes = ['employer', 'categories'];
    /**
     * Transform the \Job entity
     * @param \Job $model
     *
     * @return array
     */
    public function transform(Job $model)
    {
        if ($model->image == '') {
            $image = asset('/uploads/default_job_image.png');
        } else {
            $image = filter_var($model->image, FILTER_VALIDATE_URL) === false ? asset($model->image) : $model->image;
        }
        return [
            'id'          => (int) $model->id,
            'title'       => $model->title,
            'slug'        => $model->slug,
            'address'     => $model->address,
            'latitude'    => $model->latitude,
            'longitude'   => $model->longitude,
            'image'       => $image,
            'short_desc'  => $model->short_desc,
            'description' => $model->description,
            'shifts'      => $model->shifts,
            'note'        => $model->note,
            'type'        => $model->type,
            'requirement' => $model->requirement,
            'salary'      => $model->salary,
            'active'      => $model->active ? true : false,
            'complete'    => $model->complete ? true : false,

            'timestamps'  => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }

    public function includeShifts(Job $model)
    {
        return $this->collection($model->shifts, new ShiftTransformer);
    }
    public function includeEmployer(Job $model)
    {
        $userTransformer = new UserTransformer;
        $userTransformer->setDefaultIncludes(['employer']);
        return $this->item($model->employer, $userTransformer);
    }
    public function includeCategories(Job $model)
    {
        $categories = $model->categories;
        return $this->collection($categories, new CategoryTransformer);
    }
}
