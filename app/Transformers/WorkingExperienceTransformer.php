<?php

namespace App\Transformers;

use App\Entities\WorkingExperience;
use League\Fractal\TransformerAbstract;

/**
 * Class WorkingExperienceTransformer
 * @package namespace App\Transformers;
 */
class WorkingExperienceTransformer extends TransformerAbstract
{

    /**
     * Transform the \WorkingExperience entity
     * @param \WorkingExperience $model
     *
     * @return array
     */
    public function transform(WorkingExperience $model)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
        ];
    }
}
