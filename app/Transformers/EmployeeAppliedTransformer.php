<?php

namespace App\Transformers;

use App\Entities\EmployeeApplied;
use App\Entities\Shift;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeAppliedTransformer
 * @package namespace App\Transformers;
 */
class EmployeeAppliedTransformer extends TransformerAbstract
{

    /**
     * Transform the \EmployeeApplied entity
     * @param \EmployeeApplied $model
     *
     * @return array
     */
    public function transform(Shift $model)
    {
        return [
            'id'               => (int) $model->id,
            'job_id'           => (int) $model->job_id,
            'description'      => $model->description,
            'job'              => $model->job,
            'salary'           => $model->salary,
            'start_time'       => $model->start_time,
            'end_time'         => $model->end_time,
            'request_active'   => $model->pivot->active == '1' ? true : false,
            'request_accept'   => $model->pivot->accept == '1' ? true : false,
            'request_complete' => $model->pivot->complete == '1' ? true : false,
            'users'            => $model->users,
            'timestamps'       => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }
}
