<?php

namespace App\Transformers;

use App\Entities\Job;
use App\Transformers\CategoryTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class UserApplicationTransformer
 * @package namespace App\Transformers;
 */
class UserApplicationTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['employer', 'categories'];
    /**
     * Transform the \Job entity
     * @param \Job $model
     *
     * @return array
     */
    public function transform(Job $model)
    {
        if ($model->image == '') {
            $image = asset('/uploads/default_job_image.png');
        } else {
            $image = filter_var($model->image, FILTER_VALIDATE_URL) === false ? asset($model->image) : $model->image;
        }
        return [
            'id'           => (int) $model->id,
            'title'        => $model->title,
            'slug'         => $model->slug,
            'address'      => $model->address,
            'latitude'     => $model->latitude,
            'longitude'    => $model->longitude,
            'image'        => $image,
            'short_desc'   => $model->short_desc,
            'description'  => $model->description,
            'start_date'   => $model->start_date,
            'end_date'     => $model->end_date,
            'note'         => $model->note,
            'type'         => $model->type,
            'payment_type' => $model->payment_type,
            'salary'       => $model->salary,
            'active'       => $model->active ? true : false,
            'application' => [
                'request'      => $model->pivot->request,
                'price'        => $model->pivot->price,
                'kind_of_work' => $model->pivot->kind_of_work,
                'accept'       => $model->pivot->accept == 1 ? true : false,
                'active'       => $model->pivot->active == 1 ? true : false,
                'timestamps'   => [
                    'created_at' => $model->pivot->created_at,
                    'updated_at' => $model->pivot->updated_at,
                ],
            ],

            'timestamps'   => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }
    public function includeEmployer(Job $model)
    {
        $userTransformer = new UserTransformer;
        $userTransformer->setDefaultIncludes(['employer']);
        return $this->item($model->employer, $userTransformer);
    }
    public function includeCategories(Job $model)
    {
        $categories = $model->categories;
        return $this->collection($categories, new CategoryTransformer);
    }
}
