<?php

namespace App\Transformers;

use App\Entities\User;
use App\Transformers\EmployeeTransformer;
use App\Transformers\EmployerTransformer;
use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'role', 'employee', 'employer',
    ];
    public function transform(User $user)
    {
        if ($user->image == '') {
            $avatar = asset('/uploads/default_avatar.png');
        } else if (filter_var($user->image, FILTER_VALIDATE_URL) === false) {
            $avatar = asset($user->image);
        } else {
            $avatar = $user->image;
        }
        if ($user->cover == '') {
            $cover = asset('/uploads/image-cover.jpg');
        } else if (filter_var($user->cover, FILTER_VALIDATE_URL) === false) {
            $cover = asset($user->cover);
        } else {
            $cover = $user->cover;
        }
        return [
            'id'                 => (int) $user->id,
            'email'              => $user->email,
            'first_name'         => $user->first_name,
            'last_name'          => $user->last_name,
            'image'              => $avatar,
            'cover'              => $cover,
            'mobile'             => $user->mobile,
            'gender'             => $user->gender == 0 ? 'male' : 'female',
            'birth'              => $user->birth == '0000-00-00' ? '' : $user->birth,
            'address'            => $user->address,
            'zipcode'            => $user->zipcode,
            'city'               => $user->city,
            'country'            => $user->country,
            'email_verified'     => $user->email_verified == 0 ? false : true,
            'is_updated_profile' => $user->is_updated_profile == 0 ? false : true,
            'summary'            => $user->summary,
            'timestamps'         => [
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
            ],
        ];
    }
    public function includeRole(User $model)
    {
        $roles = $model->getRoles();
        return $this->collection($roles, new RoleTransformer);
    }
    public function includeEmployee(User $model)
    {
        $employee = $model->employee;
        if ($employee) {
            return $this->item($employee, new EmployeeTransformer);
        } else {
            return null;
        }
    }
    public function includeEmployer(User $model)
    {
        $employer = $model->employer;
        if ($employer) {
            return $this->item($employer, new EmployerTransformer);
        } else {
            return null;
        }
    }
}
