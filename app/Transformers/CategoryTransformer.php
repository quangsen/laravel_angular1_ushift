<?php

namespace App\Transformers;

use App\Entities\Category;
use App\Transformers\JobTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class CategoryTransformer
 * @package namespace App\Transformers;
 */
class CategoryTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'children',
        'jobs',
    ];
    /**
     * Transform the \Category entity
     * @param \Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        $children = $model->children();
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'level'      => (int) $model->level,
            "parent_id"  => (int) $model->parent_id,
            'timestamps' => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }

    public function includeChildren(Category $model)
    {
        $categories = $model->children();
        return $this->collection($categories, new self);
    }

    public function includeJobs(Category $model)
    {
        $jobs = $model->jobs()->paginate();
        return $this->collection($jobs, new JobTransformer);
    }
}
