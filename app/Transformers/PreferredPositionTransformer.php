<?php

namespace App\Transformers;

use App\Entities\PreferredPosition;
use App\Transformers\IndustryTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class PreferredPositionTransformer
 * @package namespace App\Transformers;
 */
class PreferredPositionTransformer extends TransformerAbstract
{
    /**
     * Transform the \PreferredPosition entity
     * @param \PreferredPosition $model
     *
     * @return array
     */
    public function transform(PreferredPosition $model)
    {
        return [
            'id'   => (int) $model->id,
            'name' => $model->name,
        ];
    }
}
