<?php

namespace App\Transformers;

use App\Entities\Conversation;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ConversationTransformer
 * @package namespace App\Transformers;
 */
class ConversationTransformer extends TransformerAbstract
{

    protected $defaultIncludes = ['messages'];

    protected $availableIncludes = ['employee', 'employer'];

    /**
     * Transform the \Conversation entity
     * @param \Conversation $model
     *
     * @return array
     */
    public function transform(Conversation $model)
    {
        return [
            'id'          => (int) $model->id,
            'title'       => $model->title,
            'job_id'      => $model->job_id,
            'employee_id' => $model->employee_id,
            'timestamps'  => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }

    public function includeMessages(Conversation $model)
    {
        return $this->collection($model->messages()->orderBy('id', 'desc')->get(), new MessageTransformer);
    }

    public function includeEmployee(Conversation $model)
    {
        $user = $model->employee;
        if ($user) {
            return $this->item($user, new UserTransformer);
        } else {
            return null;
        }
    }

    public function includeEmployer(Conversation $model)
    {
        $user = $model->employer()->first();
        if ($user) {
            return $this->item($user, new UserTransformer);
        } else {
            return null;
        }
    }
}
