<?php

namespace App\Transformers;

use App\Entities\Application;
use App\Entities\User;
use League\Fractal\TransformerAbstract;

/**
 * Class ApplicationTransformer
 * @package namespace App\Transformers;
 */
class ApplicationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Application entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        if ($model->image == '') {
            $avatar = asset('/uploads/default_avatar.png');
        } else if (filter_var($model->image, FILTER_VALIDATE_URL) === false) {
            $avatar = asset($model->image);
        } else {
            $avatar = $model->image;
        }
        return [
            'employee_id' => (int) $model->id,
            'email'       => $model->email,
            'first_name'  => $model->first_name,
            'last_name'   => $model->last_name,
            'image'       => $avatar,
            'mobile'      => $model->mobile,
            'gender'      => $model->gender == 0 ? 'male' : 'female',
            'birth'       => $model->birth,
            'address'     => $model->address,
            'zipcode'     => $model->zipcode,
            'city'        => $model->city,
            'country'     => $model->country,
            'summary'     => $model->summary,
            'application' => [
                'request'    => $model->pivot->request,
                'shift_id'   => $model->pivot->shift_id,
                'accept'     => $model->pivot->accept == 1 ? true : false,
                'active'     => $model->pivot->active == 1 ? true : false,
                'complete'   => $model->pivot->complete == 1 ? true : false,
                'timestamps' => [
                    'created_at' => $model->pivot->created_at,
                    'updated_at' => $model->pivot->updated_at,
                ],
            ],
        ];
    }
}
