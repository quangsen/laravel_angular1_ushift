<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\PreferredLocationRepository;
use App\Transformers\PreferredLocationTransformer;
use App\Validators\PreferredLocationValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;

class PreferredLocationController extends ApiController
{
    protected $repository;
    protected $validator;

    public function __construct(PreferredLocationRepository $repository, PreferredLocationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index()
    {
        $preferredLocations = $this->repository->all();
        return $this->response->collection($preferredLocations, new PreferredLocationTransformer);
    }

    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $data               = $request->all();
        $preferredLocations = $this->repository->create($data);
        return $this->response->item($preferredLocations, new PreferredLocationTransformer);
    }

    public function show($id)
    {
        $preferredLocations = $this->repository->find($id);
        return $this->response->item($preferredLocations, new PreferredLocationTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
