<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Respons to requests to GET /
     */
    public function index() 
    {
        $data['pagename'] = 'home';
    	return view('frontend.page.home', $data);
    }
}
