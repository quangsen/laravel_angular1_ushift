<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
	/**
     * Respons to requests to GET /about
     */
    public function about() 
    {
    	return view('frontend.page.about');
    }
}
