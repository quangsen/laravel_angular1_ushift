<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BusinessesController extends Controller
{
	/**
     * Respons to requests to GET /howitworks
     */
    public function businesses() 
    {
    	return view('frontend.page.businesses');
    }
}
