<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrivacyPolicyController extends Controller
{
    public function privacyPolicy() 
    {
    	return view('frontend.page.privacy-policy');
    }
}
