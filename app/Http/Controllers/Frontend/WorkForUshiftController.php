<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WorkForUshiftController extends Controller
{
    public function workForUshift() 
    {
    	return view('frontend.page.work-for-ushift');
    }
}
