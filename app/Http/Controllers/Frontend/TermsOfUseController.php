<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TermsOfUseController extends Controller
{
    public function termsOfUse() 
    {
    	return view('frontend.page.terms-of-use');
    }
}
