<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ShiftSeekersController extends Controller
{
	/**
     * Respons to requests to GET /howitworks
     */
    public function shiftSeekers() 
    {
    	return view('frontend.page.shift-seekers');
    }
}
