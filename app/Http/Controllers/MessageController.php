<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\MessageRepository;
use App\Transformers\MessageTransformer;
use App\Validators\ConversationValidator;
use App\Validators\MessageValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends ApiController
{
    /**
     * @var ConversationRepository
     */
    protected $repository;

    /**
     * @var ConversationValidator
     */
    protected $validator;

    public function __construct(MessageRepository $repository, MessageValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['excerpt' => []]);
        $this->middleware('role:employer|employee');
    }

    public function index()
    {
        $messages = $this->repository->getMessage();
        return $this->simplePaginator($messages, new MessageTransformer);
    }

    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $user              = Auth::user();
        $data              = $request->all();
        $data['sender_id'] = $user->id;
        $message           = $this->repository->create($data);
        return $this->response->item($message, new MessageTransformer);
    }
}
