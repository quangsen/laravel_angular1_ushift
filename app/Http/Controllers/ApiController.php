<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use Illuminate\Pagination\Paginator;
use League\Fractal\TransformerAbstract;

class ApiController extends Controller
{
    use Helpers;
    public function success()
    {
        return $this->response->array(['success' => true]);
    }

    public function simplePaginator(Paginator $paginator, TransformerAbstract $transformer)
    {

        $meta = [
            'pagination' => [
                'per_page'     => $paginator->perPage(),
                'current_page' => $paginator->currentPage(),
            ],
        ];
        return $this->response->collection($paginator->getCollection(), $transformer)->setMeta($meta);
    }
}
