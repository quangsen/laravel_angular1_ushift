<?php

namespace App\Http\Controllers;

use App\Events\FileUpload;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\ApiController;
use App\Validators\FileValidator;
use App\Validators\ValidatorInterface;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends ApiController
{
    private $validator;
    public function __construct(FileValidator $validator)
    {
        $this->validator = $validator;
    }
    public function upload(Request $request)
    {
        $data = $request->only('file');
        $this->validator->isValid($data, ValidatorInterface::RULE_CREATE);
        if (!$request->hasFile('file')) {
            throw new NotFoundException("File");
        } else {
            $file           = $request->file('file');
            $time           = new DateTime;
            $year           = $time->format('Y');
            $month          = $time->format('m');
            $origin_name    = $file->getClientOriginalName();
            $file_extension = $file->getClientOriginalExtension();
            if (!is_null($file_extension)) {
                $file_name = snake_case(str_replace('.' . $file_extension, '', $origin_name));
                $path      = "uploads/{$year}/{$month}/" . time() . "_{$file_name}.{$file_extension}";
            } else {
                $path = "uploads/{$year}/{$month}/" . time() . "_{$full_name}";
            }
            $success = Storage::put($path, File::get($file));
            if ($success) {
                Event::fire(new FileUpload($request, $path));
                return $this->response->array(['success' => $success, 'path' => asset($path)]);
            } else {
                return $this->response->error('can\'t upload file', 1009);
            }
        }
    }
}
