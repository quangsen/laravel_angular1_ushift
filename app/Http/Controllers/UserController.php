<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Events\UserRegister;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\EmployeeAppliedTransformer;
use App\Transformers\JobTransformer;
use App\Transformers\UserApplicationTransformer;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use App\Validators\ValidatorInterface;
use Event;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @Resource("Users", uri="/users")
 */
class UserController extends ApiController
{

    private $repository;
    private $employeeRepository;
    private $validator;
    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['except' => ['index', 'show', 'store', 'createToken', 'verifyEmail']]);
        // $this->middleware('jwt.refresh', ['except' => ['index', 'show', 'store', 'createToken']]);
        $this->middleware('role:admin', ['only' => ['destroy']]);
    }

    /**
     * Show all users
     *
     * Get a JSON representation of all the registered users.
     */
    public function index()
    {
        $users = $this->repository->paginate();
        return $this->response->paginator($users, new UserTransformer);
    }

    /**
     * Get user profile
     *
     * @param int
     * @return \Entities\User
     */
    public function show($id)
    {
        $user = $this->repository->find($id);
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Create new user
     *
     * @return \Entities\User
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        if (!$this->repository->findByField('email', $request->get('email'))->isEmpty()) {
            throw new Exception('Email already exist', 1005);
        }
        $data['password'] = Hash::make($data['password']);
        $user             = $this->repository->create($data);
        if ($request->has('role')) {
            $user = $this->repository->attachRole($request->get('role'), $user->id);
        }
        $token = JWTAuth::fromUser($user);
        Event::fire(new UserRegister($user));
        return $this->response->array(compact('token'));
    }

    /**
     * update employee profile
     *
     * @param int $id
     * @return \Entities\User
     */
    public function updateEmployeeProfile(Request $request, $id)
    {
        $this->validator->isValid($request, 'UPDATE_EMPLOYEE_PROFILE');
        $user = Auth::user();
        if (!$user->is('admin|employee')) {
            throw new PermissionDeniedException;
        } else {
            if ($user->id != $id) {
                throw new Exception("Token & ID not match", 1011);
            } else {
                $user = $this->repository->update($request->all(), $id);
                if (!$user->employee) {
                    $this->repository->createEmployeeProfile($request->all(), $id);
                } else {
                    $this->repository->updateEmployeeProfile($request->all(), $id);
                }
                if ($request->has('preferred_position_id')) {
                    $this->repository->updatePreferredPositons($request->only('preferred_position_id'), $id);
                }
                if ($request->has('preferred_location_ids')) {
                    $this->repository->updatePreferredLocations($request->get('preferred_location_ids'), $id);
                }
                if ($request->has('working_experience_id')) {
                    $this->repository->updateWorkingExperience($request->get('working_experience_id'), $id);
                }
                $user = $this->repository->find($id);
                if ($user->is_updated_profile == 0) {
                    $this->repository->update(['is_updated_profile' => 1], $user->id);
                }
                return $this->response->item($user, new UserTransformer);
            }
        }
    }

    /**
     * update employer profile
     *
     * @param int $id
     * @return \Entities\User
     */
    public function updateEmployerProfile(Request $request, $id)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_UPDATE);
        $user = Auth::user();
        if (!$user->is('admin|employer')) {
            throw new PermissionDeniedException;
        } else {
            if ($user->id != $id) {
                throw new Exception("Token & ID not match", 1011);
            } else {
                $user = $this->repository->update($request->all(), $id);
                if (!$user->employer) {
                    $this->repository->createEmployerProfile($request->all(), $id);
                } else {
                    $this->repository->updateEmployerProfile($request->all(), $id);
                }
                $user = $this->repository->find($id);
                if ($user->is_updated_profile == 0) {
                    $this->repository->update(['is_updated_profile' => 1], $user->id);
                }
                return $this->response->item($user, new UserTransformer);
            }
        }
    }

    /**
     * list jobs of employer
     * @param  int $id
     * @return \Entities\Jobs
     */
    public function jobs($id, Request $request)
    {
        if ($request->has('per_page')) {
            $per_page = $request->get('per_page');
        } else {
            $per_page = 10;
        }
        $active = 1;
        $jobs   = $this->repository->jobs($id, $active, $request->get('status'), $per_page);
        return $this->simplePaginator($jobs, new JobTransformer);
    }

    /**
     * list pending or completed job of employee
     * @param int $id
     * @return  \Entities\Jobs
     */
    public function employeePendingOrCompletedJobs($id, Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        if (!$user->is('admin|employee')) {
            throw new PermissionDeniedException;
        } else {
            if ($user->id != $id) {
                throw new Exception("Token & ID not match", 1011);
            } else {
                $is_accept      = isset($data['is_accept']) && $data['is_accept'] !== null ? intval($data['is_accept']) : 0;
                $paginationItem = isset($data['paginationItem']) && $data['paginationItem'] !== null ? intval($data['paginationItem']) : null;
                $applications   = $this->repository->employeePendingOrCompletedJobs($id, $paginationItem, $is_accept);
                return $this->response->paginator($applications, new UserApplicationTransformer);
            }
        }
    }

    /**
     * list jobs of employee
     * @param  int $id
     * @return \Entities\Jobs
     */
    public function employeeAppliedJobs($id)
    {
        $user = Auth::user();

        $active = (boolean) 1;
        $jobs   = $this->repository->getJobsOfEmployee($id, $active);
        return $this->response->paginator($jobs, new JobTransformer);
    }

    /**
     * list of all applied shifts
     *
     * @param int $id
     * @return \Response
     */
    public function employeeAppliedShifts(Request $request, $id)
    {
        if ($request->has('period')) {
            $period = $request->get('period');
        } else {
            $period = 'upcomming';
        }
        if ($request->has('accept')) {
            if ($request->get('accept') === true || $request->get('accept') === 'true') {
                $accept = '1';
            } else {
                $accept = '0';
            }
        } else {
            $accept = null;
        }
        if ($request->has('complete')) {
            if ($request->get('complete') === true || $request->get('complete') === 'true') {
                $complete = '1';
            } else {
                $complete = '0';
            }
        } else {
            $complete = null;
        }
        if ($request->has('per_page')) {
            $per_page = $request->get('per_page');
        } else {
            $per_page = 10;
        }
        $shifts = $this->repository->getAppliedShifts($id, $accept, $complete, $period, $per_page);
        return $this->simplePaginator($shifts, new EmployeeAppliedTransformer);
    }

    /**
     * delete a user
     *
     * @param int
     * @return mixed
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return $this->success();
    }

    /**
     * Create token from user
     *
     * @param int $id
     * @return mixed
     */
    public function createToken($id)
    {
        $user = $this->repository->find($id);
        return $this->response->array(['token' => $user->getToken()]);
    }

    /**
     * verify user's email
     *
     * @param int $id
     * @return \Entities\User
     */
    public function verifyEmail(Request $request, $id)
    {
        $user = $this->repository->find($id);
        if (!Hash::check($user->email, $request->get('token'))) {
            throw new Exception("Token does not match", 1003);
        }
        $user = $this->repository->verifyEmail($user);
        return $this->response->item($user, new UserTransformer);
    }

    public function isOwner($user_id, $job_id)
    {
        $isOwner = $this->repository->isOwner($user_id, $job_id);
        return $this->response->array(['is_owner' => $isOwner]);
    }
}
