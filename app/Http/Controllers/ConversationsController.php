<?php

namespace App\Http\Controllers;

use App\Events\MessageLoaded;
use App\Exceptions\NotFoundException;
use App\Repositories\ConversationRepository;
use App\Transformers\ConversationTransformer;
use App\Transformers\MessageTransformer;
use App\Validators\ConversationValidator;
use App\Validators\ValidatorInterface;
use Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationsController extends ApiController
{

    /**
     * @var ConversationRepository
     */
    protected $repository;

    /**
     * @var ConversationValidator
     */
    protected $validator;

    public function __construct(ConversationRepository $repository, ConversationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['excerpt' => []]);
        $this->middleware('role:employer|employee');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->is('employer')) {
            $conversations = $this->repository->getEmployerConversations($user);
        } else {
            $conversations = $this->repository->getEmployeeConversations($user);
        }
        return $this->simplePaginator($conversations, new ConversationTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $user = Auth::user();
        if ($user->is('employer')) {
            $employee_id = $request->get('receiver_id');
            $employer_id = $user->id;
        } else if ($user->is('employee')) {
            $employee_id = $user->id;
            $employer_id = $request->get('receiver_id');
        }
        $conversation = $this->repository->createNewConversation($request->get('job_id'), $employer_id, $employee_id);
        $conversation->employer()->attach($employer_id);
        return $this->response->item($conversation, new ConversationTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $conversation = $this->repository->find($id);
        return $this->response->item($conversation, new ConversationTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $conversation = $this->repository->update($request->all(), $id);
        return $this->response->item($conversation, new ConversationTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);
        return $this->success();
    }

    /**
     * Retrieve all messages of conversation
     *
     * @param int
     * @return Response
     */
    public function messages($id)
    {
        $messages = $this->repository->messages($id);
        return $this->response->paginator($messages, new MessageTransformer);
    }

    /**
     * set all uread status to 0
     *
     * @param int
     * @return Response
     */
    public function clear($id)
    {
        Event::fire(new MessageLoaded($id));
        return $this->success();
    }

    /**
     * get last message of a conversation
     *
     * @param int
     * @return Response
     */
    public function lastMessage(Request $request, $id)
    {
        $message = $this->repository->find($id)->messages()->where('id', '>', $request->get('last_id'))->orderBy('id', 'desc')->first();
        if ($message) {
            return $this->response->item($message, new MessageTransformer);
        } else {
            throw new NotFoundException;
        }
    }

    public function addMessage(Request $request, $id)
    {
        $sender            = Auth::user();
        $data              = $request->all();
        $data['sender_id'] = $sender->id;
        $message           = $this->repository->addMessage($data, $id);
        return $this->response->item($message, new MessageTransformer);
    }

    /**
     * Retrieve all new message of a conversation
     *
     * @param $int
     * @return Response - collection of message
     */
    public function newMessages(Request $request, $conversation_id)
    {
        $user     = Auth::user();
        $messages = $this->repository->newMessages($conversation_id, $request->get('last_id'));
        return $this->response->collection($messages, new MessageTransformer);
    }

    public function oldMessages($conversation_id, $message_id)
    {
        $user     = Auth::user();
        $messages = $this->repository->oldMessages($conversation_id, $message_id);
        return $this->simplePaginator($messages, new MessageTransformer);
    }

    public function initConversation(Request $request)
    {
        $this->validator->isValid($request, 'init');
        $user = Auth::user();
        if ($user->is('employer')) {
            $employee_id = $request->get('receiver_id');
            $employer_id = $user->id;
        } else if ($user->is('employee')) {
            $employee_id = $user->id;
            $employer_id = $request->get('receiver_id');
        }
        if ($request->has('job_id')) {
            $conversation = $this->repository->getConversation($employer_id, $employee_id, $request->get('job_id'));
        } else {
            $conversation = $this->repository->getConversation($employer_id, $employee_id);
        }
        return $this->response->item($conversation, new ConversationTransformer);
    }
}
