<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\WorkingExperienceRepository;
use App\Transformers\WorkingExperienceTransformer;
use App\Validators\WorkingExperienceValidator;
use Illuminate\Http\Request;

class WorkingExperienceController extends ApiController {
    protected $repository;
    protected $validator;

	public function __construct(WorkingExperienceRepository $repository, WorkingExperienceValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
	}
	
	public function index() {
		$workingExperiences = $this->repository->all();
        return $this->response->collection($workingExperiences, new WorkingExperienceTransformer);
	}

	public function create() {
		//
	}

	public function store(Request $request) {
		//
	}

	public function show($id) {
		$workingExperience = $this->repository->find($id);
        return $this->response->item($workingExperience, new WorkingExperienceTransformer);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
