<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;

class PasswordController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware());
    }

    public function getSendResetLinkEmailFailureResponse()
    {
        return $this->response->error('can\'t send reset password email', 500);
    }
    public function getResetFailureResponse()
    {
        return $this->response->error('Email or Token not match', 500);
    }
    public function getSendResetLinkEmailSuccessResponse()
    {
        return $this->success();
    }
    public function getResetSuccessResponse($response)
    {
        $token = Auth::user()->getToken();
        return $this->response->array(['token' => $token]);
    }
}
