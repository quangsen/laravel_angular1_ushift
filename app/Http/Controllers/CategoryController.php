<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\CategoryRepository;
use App\Transformers\CategoryTransformer;
use App\Transformers\JobTransformer;
use App\Validators\CategoryValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{
    private $repository;
    private $validator;
    public function __construct(CategoryRepository $repository, CategoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index()
    {
        $categories = $this->repository->all();
        return $this->response->collection($categories, new CategoryTransformer);
    }

    public function findByLevel($level)
    {
        $categories = $this->repository->findByField('level', $level);
        return $this->response->collection($categories, new CategoryTransformer);
    }

    public function store(Request $request)
    {
        $data = $request->only('name', 'parent_id');
        $this->validator->isValid($data, ValidatorInterface::RULE_CREATE);
        if (!isset($data['parent_id'])) {
            $data['parent_id'] = 0;
            $data['level']     = 1;
        } else {
            $parent_category = $this->repository->find($data['parent_id']);
            $data['level']   = $parent_category->level + 1;
        }
        $category = $this->repository->create($data);
        return $this->response->item($category, new CategoryTransformer);
    }

    public function show($id)
    {
        $category = $this->repository->find($id);
        return $this->response->item($category, new CategoryTransformer);
    }

    public function update(Request $request, $id)
    {
        $data = $request->only('name');
        $this->validator->isValid($data, ValidatorInterface::RULE_UPDATE);
        $category = $this->repository->update($data, $id);
        return $this->response->item($category, new CategoryTransformer);
    }
    public function destroy($id)
    {
        $this->repository->delete($id);
        return $this->success();
    }
    public function addJob(Request $request, $id)
    {
        $this->validator->isValid($request, 'addJob');
        $this->repository->attachJob($id, $request->get('job_id'));
        return $this->success();
    }

    public function jobs(Request $request, $id)
    {
        $this->validator->isValid($request, 'getJobs');
        $is_active = (boolean) $request->get('active');
        $jobs      = $this->repository->jobs($id, $is_active);
        return $this->response->paginator($jobs, new JobTransformer);
    }
}
