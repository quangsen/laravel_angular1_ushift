<?php

namespace App\Http\Controllers;

use App\Events\UserLogin;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends ApiController
{
    private $repository;
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        // $this->middleware('jwt.refresh', ['only' => ['me']]);
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                if ($this->repository->findByField('email', $credentials['email'])->isEmpty()) {
                    throw new NotFoundException('Email');
                } else {
                    throw new Exception('Password doestn\'t match', 1004);
                }
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        Event::fire(new UserLogin(Auth::user()));
        return $this->response->array(compact('token'));
    }
    public function me()
    {
        $user = JWTAuth::parseToken()->toUser();

        return $this->response->item($user, new UserTransformer);
    }
    public function refreshToken()
    {
        $token = JWTAuth::parseToken()->refresh();
        return $this->response->array(compact('token'));
    }
    public function logout()
    {
        $token = JWTAuth::parseToken()->invalidate();
        return $this->success();
    }
}
