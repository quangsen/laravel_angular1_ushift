<?php

namespace App\Http\Controllers;

use App\Entities\Shift;
use App\Events\ApplicationStatusWasChanged;
use App\Events\JobCreated;
use App\Events\JobStatusWasChanged;
use App\Events\UserApplied;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\ApiController;
use App\Repositories\JobRepository;
use App\Transformers\ApplicationTransformer;
use App\Transformers\CategoryTransformer;
use App\Transformers\JobTransformer;
use App\Transformers\UserTransformer;
use App\Validators\JobValidator;
use App\Validators\ValidatorInterface;
use Event;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(JobRepository $repository, JobValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth', ['only' => ['store', 'update', 'destroy', 'apply', 'accept', 'changeJobStatus']]);
        $this->middleware('role:employer|admin', ['only' => ['store', 'update', 'destroy', 'accept', 'changeJobStatus']]);
        $this->middleware('role:employee|admin', ['only' => ['apply']]);
    }
    public function index()
    {
        $jobs = $this->repository->getAll(['active' => 1, 'complete' => 0]);
        return $this->response->paginator($jobs, new JobTransformer);
    }
    public function show($id)
    {
        $job = $this->repository->find($id);
        return $this->response->item($job, new JobTransformer);
    }

    public function getJobBySlug($slug)
    {
        $job = $this->repository->findByField('slug', $slug)->first();
        return $this->response->item($job, new JobTransformer);
    }

    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $data = $request->all();
        $slug = str_slug($data['title']);
        $i    = 1;
        while (!$this->repository->findByField('slug', $slug)->isEmpty()) {
            $slug = str_slug($request->get('title')) . '-' . $i;
            $i++;
        }
        $data['slug'] = $slug;
        $shifts       = new Collection($request->get('shifts'));
        $shifts       = $shifts->map(function ($shift) {
            return new Shift((array) $shift);
        });
        $job = $this->repository->create($data);
        $this->repository->attachCategory($job, $data['category_id']);
        $this->repository->saveShifts($job, $shifts->all());
        Event::fire(new JobCreated($job, Auth::user()));
        return $this->response->item($job, new JobTransformer);
    }

    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_UPDATE);
        $job = $this->repository->update($request->all(), $id);
        $this->repository->updateCategory($job, $request->get('category_id'));
        return $this->response->item($job, new JobTransformer);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return $this->success();
    }
    public function apply(Request $request, $job_id)
    {
        $this->validator->isValid($request, 'apply');
        $user         = Auth::user();
        $applications = $this->repository->apply($job_id, $user->id, $request->get('request'), $request->get('shift_ids'));
        Event::fire(new UserApplied($user, $job_id, $applications));
        return $this->response->collection($applications, new ApplicationTransformer);
    }

    public function changeApplicationStatus(Request $request, $job_id)
    {
        $this->validator->isValid($request, 'changeApplicationStatus');
        $user = Auth::user();
        $data = $this->repository->changeApplicationStatus($user->id, $job_id, $request->get('employee_id'), $request->get('data'));
        Event::fire(new ApplicationStatusWasChanged($user, $request->get('employee_id'), $job_id, $data));
        return $this->response->collection($data['applications'], new ApplicationTransformer);
    }

    public function cancelPendingJob($job_id)
    {
        $user     = Auth::user();
        $userInfo = $this->repository->cancelPendingJob($user->id, $job_id);
        return $this->response->item($userInfo, new UserTransformer);
    }

    public function applications(Request $request, $id)
    {
        $applications = $this->repository->getApplications($id);
        return $this->response->collection($applications, new ApplicationTransformer);
    }

    public function getApplicationOfUser($job_id, $user_id)
    {
        $applications = $this->repository->getApplications($job_id, $user_id);
        return $this->response->collection($applications, new ApplicationTransformer);
    }

    public function related($id)
    {
        $jobs = $this->repository->getRelatedJob($id);
        return $this->response->paginator($jobs, new JobTransformer);
    }

    public function isSentApplication($job_id, $user_id)
    {
        $is_sent_application = $this->repository->isSentApplication($job_id, $user_id);
        return $this->response->array(['is_sent_application' => $is_sent_application]);
    }

    public function categories($id)
    {
        $categories = $this->repository->getCategories($id);
        return $this->response->collection($categories, new CategoryTransformer);
    }

    public function changeJobStatus(Request $request, $id)
    {
        $this->validator->isValid($request, 'changeJobStatus');
        $user = Auth::user();
        $job  = $this->repository->find($id);
        if ($user->is('admin') || $user->id == $job->employer_id) {
            $job = $this->repository->update(['complete' => $request->get('completed')], $id);
        } else {
            throw new PermissionDeniedException;
        }
        Event::fire(new JobStatusWasChanged($user, $job));
        return $this->response->item($job, new JobTransformer);
    }

}
