<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\IndustryRepository;
use App\Transformers\IndustryTransformer;
use App\Validators\IndustryValidator;

class IndustryController extends ApiController
{
    private $repository;
    private $validator;
    public function __construct(IndustryRepository $repository, IndustryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }
    public function index()
    {
        $industries = $this->repository->all();
        return $this->response->collection($industries, new IndustryTransformer);
    }
    public function show($id)
    {
        $industry = $this->repository->find($id);
        return $this->response->item($industry, new IndustryTransformer);
    }
}
