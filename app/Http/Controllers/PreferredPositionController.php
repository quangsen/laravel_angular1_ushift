<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\PreferredPositionRepository;
use App\Transformers\PreferredPositionTransformer;
use App\Validators\PreferredPositionValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;

class PreferredPositionController extends ApiController {
	protected $repository;
	protected $validator;

	public function __construct(PreferredPositionRepository $repository, PreferredPositionValidator $validator) {
		$this->repository = $repository;
		$this->validator  = $validator;
	}

	public function index() {
		$preferredPositions = $this->repository->all();
		return $this->response->collection($preferredPositions, new PreferredPositionTransformer);
	}

	public function store(Request $request, $industry_id) {
		$this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
		$data                = $request->all();
		$data['industry_id'] = $industry_id;
		$preferredPosition   = $this->repository->create($data);
		return $this->response->item($preferredPosition, new PreferredPositionTransformer);
	}

	public function show($id) {
		$preferredPosition = $this->repository->find($id);
		return $this->response->item($preferredPosition, new PreferredPositionTransformer);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
