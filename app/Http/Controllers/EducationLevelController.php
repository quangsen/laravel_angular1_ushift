<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\EducationLevelRepository;
use App\Transformers\EducationLevelTransformer;
use App\Validators\EducationLevelValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;

class EducationLevelController extends ApiController
{
    protected $repository;
    protected $validator;
    public function __construct(EducationLevelRepository $repository, EducationLevelValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }
    public function index()
    {
        $educationLevels = $this->repository->all();
        return $this->response->collection($educationLevels, new EducationLevelTransformer);
    }

    public function show($id)
    {
        $educationLevel = $this->repository->find($id);
        return $this->response->item($educationLevel, new EducationLevelTransformer);
    }

    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $educationLevel = $this->repository->create($request->all());
        return $this->response->item($educationLevel, new EducationLevelTransformer);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
        return $this->success();
    }
}
