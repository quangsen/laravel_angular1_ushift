<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use App\Repositories\LastPositionRepository;
use App\Transformers\LastPositionTransformer;
use App\Validators\LastPositionValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;

class LastPositionController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(LastPositionRepository $repository, LastPositionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->middleware('jwt.auth');
        $this->middleware('role:employee');
    }
    public function index($id)
    {
        $last_positions = $this->repository->lastLastPositionOfUser($id);
        return $this->response->collection($last_positions, new LastPositionTransformer);
    }
    public function store(Request $request, $id)
    {
        $last_position = $this->repository->addLastPositionOfUser($request->all(), $id);
        return $this->response->item($last_position, new LastPositionTransformer);
    }
    public function update(Request $request, $user_id, $last_position_id)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_UPDATE);
        $last_position = $this->repository->update($request->all(), $last_position_id);
        return $this->response->item($last_position, new LastPositionTransformer);
    }
    public function destroy($user_id, $last_position_id)
    {
        $this->repository->removeLastPosition($user_id, $last_position_id);
        return $this->success();
    }
}
