<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    // Auth API
    $api->post('login', 'App\Http\Controllers\AuthController@authenticate');
    $api->get('me', ['middleware' => 'jwt.auth', 'uses' => 'App\Http\Controllers\AuthController@me']);
    $api->put('/auth/refresh-token', 'App\Http\Controllers\AuthController@refreshToken');
    $api->delete('/auth/logout', 'App\Http\Controllers\AuthController@logout');
    // User API
    $api->get('users/{id}/create-token', ['uses' => 'App\Http\Controllers\UserController@createToken']);
    $api->put('users/{id}/verify-email', ['uses' => 'App\Http\Controllers\UserController@verifyEmail']);
    $api->put('users/{id}/employee', ['uses' => 'App\Http\Controllers\UserController@updateEmployeeProfile']);
    $api->put('users/{id}/employer', ['uses' => 'App\Http\Controllers\UserController@updateEmployerProfile']);
    $api->get('users/{id}/is-owner-of/{job_id}', ['uses' => 'App\Http\Controllers\UserController@isOwner']);
    $api->resource('users', 'App\Http\Controllers\UserController');
    // Employer
    $api->get('user/{id}/employer/jobs', ['uses' => 'App\Http\Controllers\UserController@jobs']);
    // Employee
    $api->get('user/{id}/applied-jobs', ['uses' => 'App\Http\Controllers\UserController@employeeAppliedJobs']);
    $api->get('user/{id}/applied-shifts', ['uses' => 'App\Http\Controllers\UserController@employeeAppliedShifts']);
    $api->get('user/{id}/pending-or-completed-jobs', ['uses' => 'App\Http\Controllers\UserController@employeePendingOrCompletedJobs']);

    $api->post('password/email', 'App\Http\Controllers\Auth\PasswordController@postEmail');
    $api->post('password/reset', 'App\Http\Controllers\Auth\PasswordController@postReset');
    // Employee Last Postion
    $api->resource('users/{id}/last-positions', 'App\Http\Controllers\LastPositionController');
    // Category API
    $api->resource('categories', 'App\Http\Controllers\CategoryController');
    $api->put('categories/{id}/add-job', 'App\Http\Controllers\CategoryController@addJob');
    $api->get('categories/{id}/jobs', 'App\Http\Controllers\CategoryController@jobs');
    $api->get('categories/level/{level}', 'App\Http\Controllers\CategoryController@findByLevel');
    // File API
    $api->post('file/upload', 'App\Http\Controllers\FileController@upload');
    // Education Level
    $api->resource('education-levels', 'App\Http\Controllers\EducationLevelController');
    // Industry
    $api->resource('industries', 'App\Http\Controllers\IndustryController');
    $api->post('industries/{id}/positions', 'App\Http\Controllers\PreferredPositionController@store');
    // Preferred Position
    $api->resource('preferred-positions', 'App\Http\Controllers\PreferredPositionController');
    $api->resource('preferred-locations', 'App\Http\Controllers\PreferredLocationController');
    // Working Experience
    $api->resource('working-experiences', 'App\Http\Controllers\WorkingExperienceController');
    // Jobs
    $api->resource('jobs', 'App\Http\Controllers\JobController');
    $api->get('jobs/{id}/related', 'App\Http\Controllers\JobController@related');
    $api->get('jobs/slug/{slug}', 'App\Http\Controllers\JobController@getJobBySlug');
    $api->post('jobs/{id}/apply', ['middleware' => ['jwt.auth', 'role:employee'], 'uses' => 'App\Http\Controllers\JobController@apply']);
    $api->get('jobs/{id}/apply/{user_id}/is-sent-application', ['middleware' => ['jwt.auth', 'role:employee'], 'uses' => 'App\Http\Controllers\JobController@isSentApplication']);
    $api->put('jobs/{id}/change-application-status', ['middleware' => ['jwt.auth', 'role:employer'], 'uses' => 'App\Http\Controllers\JobController@changeApplicationStatus']);
    $api->put('jobs/{id}/cancel-pending-job', ['middleware' => ['jwt.auth', 'role:employee'], 'uses' => 'App\Http\Controllers\JobController@cancelPendingJob']);
    $api->get('jobs/{id}/applications', 'App\Http\Controllers\JobController@applications');
    $api->get('jobs/{id}/applications/{user_id}', 'App\Http\Controllers\JobController@getApplicationOfUser');
    $api->get('jobs/{id}/categories', 'App\Http\Controllers\JobController@categories');
    $api->put('jobs/{id}/status', 'App\Http\Controllers\JobController@changeJobStatus');
    // Inbox api
    $api->resource('conversations', 'App\Http\Controllers\ConversationsController');
    $api->get('conversations/{id}/clear', 'App\Http\Controllers\ConversationsController@clear');
    $api->get('conversations/{id}/messages', 'App\Http\Controllers\ConversationsController@messages');
    $api->get('conversations/{id}/new-message', 'App\Http\Controllers\ConversationsController@newMessages');
    $api->get('conversations/{id}/old-messages/{message_id}', 'App\Http\Controllers\ConversationsController@oldMessages');
    $api->post('conversations/{id}/messages', 'App\Http\Controllers\ConversationsController@addMessage');
    $api->get('conversations/{id}/last-message', 'App\Http\Controllers\ConversationsController@lastMessage');
    $api->post('conversations/init', 'App\Http\Controllers\ConversationsController@initConversation');
    $api->resource('messages', 'App\Http\Controllers\MessageController');
});

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'Frontend\HomeController@index');
    Route::get('/about', 'Frontend\AboutController@about');
    Route::get('/how-it-works', 'Frontend\HowitworksController@howItWorks');
    Route::get('/shift-seekers', 'Frontend\ShiftSeekersController@shiftSeekers');
    Route::get('/businesses', 'Frontend\BusinessesController@businesses');
    Route::get('/work-for-ushift', 'Frontend\WorkForUshiftController@workForUshift');
    Route::get('/terms-of-use', 'Frontend\TermsOfUseController@termsOfUse');
    Route::get('/privacy-policy', 'Frontend\PrivacyPolicyController@privacyPolicy');
    Route::get('/cookie-policy', 'Frontend\CookiePolicyController@cookiePolicy');

    Route::any('{path?}', function () {
        return view("frontend.spa");
    })->where("path", ".+");
});
