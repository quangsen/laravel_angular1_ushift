<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class ConversationValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'job_id'      => ['integer'],
            'receiver_id' => ['required', 'integer'],
        ],
        ValidatorInterface::RULE_UPDATE => [],
        'init'                          => [
            'job_id'      => ['integer'],
            'receiver_id' => ['required', 'integer'],
        ],
    ];
}
