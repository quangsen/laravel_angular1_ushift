<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class PreferredPositionValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required', 'max:40'],
        ],
        ValidatorInterface::RULE_UPDATE => [
        	'name' => ['required', 'max:40'],
        ],
    ];
}
