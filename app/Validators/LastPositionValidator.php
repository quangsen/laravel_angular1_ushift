<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class LastPositionValidator extends AbstractValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'company_name'      => ['max:100'],
            'country'           => ['max:100'],
            'role'              => ['max:50'],
            'type'              => ['max:100'],
            'start_date'        => ['date_format:Y-m-d'],
            'end_date'          => ['date_format:Y-m-d'],
            'reason_of_leaving' => ['max:255'],
            'name_of_manager'   => ['max:50'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'company_name'      => ['max:100'],
            'country'           => ['max:100'],
            'role'              => ['max:50'],
            'type'              => ['max:100'],
            'start_date'        => ['date_format:Y-m-d'],
            'end_date'          => ['date_format:Y-m-d'],
            'reason_of_leaving' => ['max:255'],
            'name_of_manager'   => ['max:50'],
        ],
    ];
}
