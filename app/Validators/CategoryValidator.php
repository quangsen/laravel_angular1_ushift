<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class CategoryValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'      => ['required', 'max:100'],
            'parent_id' => [],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['required', 'max:100'],
        ],
        'addJob'                        => [
            'job_id' => ['required', 'integer'],
        ],
        'getJobs'                        => [
            'active' => ['required', 'boolean'],
        ],
    ];
}
