<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class FileValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'file' => 'required', 'mimes:jpeg,png,gif,pdf,doc,docx',
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
