<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class UserValidator extends AbstractValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'first_name' => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'  => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'email'      => ['required', 'email'],
            'password'   => ['required', 'min:6', 'max:30'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'first_name'               => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'                => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'gender'                   => ['boolean'],
            'address'                  => ['max:255'],
            'zipcode'                  => [],
            'mobile'                   => ['regex:/[0-9\s|+]*/', 'max:20'],
            'city'                     => ['regex:/[a-z\s]*/i', 'max:40'],
            'country'                  => ['regex:/[a-z\s]*/i', 'max:40'],
            'birth'                    => ['date_format:Y-m-d'],
            'allow_to_work_in_sg'      => ['boolean'],
            'kind_of_work'             => ['regex:/[parttime|fulltime]/'],
            'hours_per_week'           => ['integer'],
            'industry_id'              => ['integer'],
            'working_experience_id'    => ['integer'],
            'preferred_position_id'    => ['integer'],
            'preferred_position_ids'   => ['array'],
            'preferred_position_ids.*' => ['integer'],
            'education'                => ['max:40'],
        ],
        'UPDATE_EMPLOYEE_PROFILE'       => [
            'first_name'               => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'                => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'gender'                   => ['boolean'],
            'address'                  => ['max:255'],
            'zipcode'                  => [],
            'mobile'                   => ['regex:/[0-9\s|+]*/', 'max:20'],
            'city'                     => ['regex:/[a-z\s]*/i', 'max:40'],
            'country'                  => ['regex:/[a-z\s]*/i', 'max:40'],
            'birth'                    => ['date_format:Y-m-d'],
            'allow_to_work_in_sg'      => ['boolean'],
            'kind_of_work'             => ['regex:/[parttime|fulltime]/'],
            'hours_per_week'           => ['integer'],
            'industry_id'              => ['integer'],
            'working_experience_id'    => ['integer'],
            'preferred_position_id'    => ['integer'],
            'preferred_location_ids'   => ['array'],
            'preferred_location_ids.*' => ['integer'],
            'education'                => ['max:40'],
        ],
    ];
}
