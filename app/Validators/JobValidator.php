<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class JobValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'employer_id' => ['required', 'integer'],
            'title'       => ['max:255'],
            'latitude'    => [],
            'longitude'   => [],
            'address'     => [],
            'short_desc'  => ['required', 'max:255'],
            'description' => ['required'],
            'shifts'      => ['required', 'array'],
            'note'        => [],
            'category_id' => ['required', 'integer'],
            'type'        => [],
            'requirement' => [],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'employer_id' => ['integer'],
            'title'       => ['max:255'],
            'image'       => [],
            'short_desc'  => ['max:255'],
            'description' => [],
            'note'        => [],
            'type'        => [],
            'requirement' => [],
            'salary'      => [],
        ],
        'apply'                         => [
            'request'  => ['required'],
            'shift_ids' => ['required', 'array'],
        ],
        'changeApplicationStatus'       => [
            'employee_id' => ['required', 'integer'],
            'data'      => ['required', 'array'],
        ],
        'changeJobStatus'               => [
            'completed' => ['required', 'boolean'],
        ],
    ];
}
