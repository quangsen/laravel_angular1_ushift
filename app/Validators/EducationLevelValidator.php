<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class EducationLevelValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required', 'max:50'],
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
