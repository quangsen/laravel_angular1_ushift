<?php

namespace App\Message;

use App\Entities\Conversation;
use App\Entities\Job;
use App\Exceptions\PermissionDeniedException;

class Message
{
    public function send(\App\Entities\User $sender, $receiver_id, $job_id, \App\Entities\Message $message)
    {
        $conversation = $this->getConversation($sender, $receiver_id, $job_id);
        $conversation->messages()->save($message);
        return $message;
    }

    public function getConversation(\App\Entities\User $sender, $receiver_id, $job_id)
    {
        if (!$sender->is('employer|employee')) {
            throw new PermissionDeniedException;
        } else {
            if ($sender->is('employer')) {
                $conversation = $sender->employer_conversations()->where('job_id', $job_id)->first();
            } else {
                $conversation = $sender->employee_conversations()->where('job_id', $job_id)->first();
            }
        }
        if (!$conversation) {
            if ($sender->is('employer')) {
                $employer_id = $sender->id;
                $employee_id = $receiver_id;
            } else {
                $employer_id = $receiver_id;
                $employee_id = $sender->id;
            }
            $job          = Job::findOrFail($job_id);
            $conversation = Conversation::create(['title' => $job->title, 'job_id' => $job_id, 'employee_id' => $employee_id, 'employer_id' => $employer_id]);
            $conversation->employer()->attach($employer_id);
        }
        return $conversation;
    }
}
