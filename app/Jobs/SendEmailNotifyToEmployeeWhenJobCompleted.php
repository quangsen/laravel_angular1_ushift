<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmailNotifyToEmployeeWhenJobCompleted extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $employees;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($employees)
    {
        $this->employees = $employees;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->employees as $user) {
            Mail::send('emails.job.employee.job_completed', compact('user'), function ($mail) use ($user) {
                $mail->from('support@ushift.com', 'Ushift');
                $mail->to($user->email, $user->getName())->subject('The job has been completed');
            });
        }
    }
}
