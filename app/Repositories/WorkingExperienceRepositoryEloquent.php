<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\WorkingExperienceRepository;
use App\Entities\WorkingExperience;
use App\Validators\WorkingExperienceValidator;

/**
 * Class WorkingExperienceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class WorkingExperienceRepositoryEloquent extends BaseRepository implements WorkingExperienceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WorkingExperience::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
