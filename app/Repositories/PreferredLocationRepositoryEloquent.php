<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PreferredLocationRepository;
use App\Entities\PreferredLocation;
use App\Validators\PreferredLocationValidator;

/**
 * Class PreferredLocationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PreferredLocationRepositoryEloquent extends BaseRepository implements PreferredLocationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PreferredLocation::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
