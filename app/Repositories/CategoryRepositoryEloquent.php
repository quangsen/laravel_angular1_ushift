<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Entities\Job;
use App\Repositories\CategoryRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CategoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CategoryRepositoryEloquent extends BaseRepository implements CategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    public function attachJob($category_id, $job_id)
    {
        $category = $this->find($category_id);
        $job      = Job::find($job_id);
        $category->jobs()->attach($job_id);
        while ($job->parent_id != 0) {
            $category->jobs()->attach($job->id);
            $job = Job::find($job->parent_id);
        }
        return true;
    }

    public function jobs($id, $is_active = 1)
    {
        $category = $this->find($id);
        $jobs     = $category->jobs()->has('available_shifts')->orderBy('id', 'desc')->where('active', $is_active)->where('complete', 0)->paginate();
        return $jobs;
    }
}
