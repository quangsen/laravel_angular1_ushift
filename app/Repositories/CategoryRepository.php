<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository
 * @package namespace App\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{
	/**
	 * attach job to category
	 * 
	 * @param int $category_id
	 * @param int $job_id
	 * @return boolean
	 */
    public function attachJob($category_id, $job_id);

    /**
     * get all jobs of a category
     * 
     * @param int
     * @param int $is_active = 1
     * @return collection of \Entities\Job
     */
    public function jobs($id, $is_active);
}
