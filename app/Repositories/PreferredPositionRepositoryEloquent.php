<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PreferredPositionRepository;
use App\Entities\PreferredPosition;
use App\Validators\PreferredPositionValidator;

/**
 * Class PreferredPositionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PreferredPositionRepositoryEloquent extends BaseRepository implements PreferredPositionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PreferredPosition::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
