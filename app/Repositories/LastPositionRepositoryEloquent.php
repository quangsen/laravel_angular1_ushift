<?php

namespace App\Repositories;

use App\Entities\LastPosition;
use App\Entities\User;
use App\Exceptions\NotFoundException;
use App\Exceptions\PermissionDeniedException;
use App\Repositories\LastPositionRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LastPositionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LastPositionRepositoryEloquent extends BaseRepository implements LastPositionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LastPosition::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function lastLastPositionOfUser($id)
    {
        $user = User::find($id);
        return $user->employee->last_positions;
    }
    public function addLastPositionOfUser($data = [], $id)
    {
        $user = User::find($id);
        if (!$user->employee) {
            throw new NotFoundException("Employee");
        } else {
            $data['employee_id'] = $user->employee->id;
            return $this->create($data);
        }
    }
    public function removeLastPosition($user_id, $last_position_id)
    {
        $last_position = $this->find($last_position_id);
        if ($last_position->employee->user->id != $user_id) {
            throw new PermissionDeniedException;
        } else {
            return $this->delete($last_position_id);
        }
    }
}
