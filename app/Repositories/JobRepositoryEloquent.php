<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Entities\Job;
use App\Entities\User;
use App\Exceptions\NotFoundException;
use App\Repositories\JobRepository;
use Illuminate\Support\Collection;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class JobRepositoryEloquent
 * @package namespace App\Repositories;
 */
class JobRepositoryEloquent extends BaseRepository implements JobRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Job::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    public function getAll($condition = [])
    {
        if (count($condition)) {
            $jobs = Job::where($condition)->has('available_shifts')->orderBy('id', 'desc')->paginate();
        } else {
            $jobs = Job::orderBy('id', 'desc')->paginate();
        }
        return $jobs;
    }
    public function apply($job_id, $user_id, $request, $shift_ids)
    {
        $job    = $this->find($job_id);
        $shifts = [];
        foreach ($shift_ids as $shift_id) {
            $shifts[$shift_id] = ['request' => $request, 'job_id' => $job_id];
        }
        $user = User::find($user_id);
        $user->shifts()->sync($shifts);
        $applications = new Collection;
        foreach ($job->shifts as $shift) {
            $applications = $applications->merge($shift->users()->wherePivot('user_id', $user_id)->get());
        }
        return $applications;
    }
    public function changeApplicationStatus($employer_id, $job_id, $employee_id, $data)
    {
        $employer                             = User::findOrFail($employer_id);
        $job                                  = $employer->jobs()->where('id', $job_id)->firstOrFail();
        $employee                             = User::findOrFail($employee_id);
        $applicationsHasAcceptStatusIsChanged = new Collection();
        foreach ($data as $item) {
            $application = $employee->shifts()->wherePivot('shift_id', $item['shift_id'])->firstOrFail();
            if (!$application) {
                throw new NotFoundException;
            } else {
                if (isset($item['accept'])) {
                    if ($item['accept']) {
                        if ($application->pivot->accept != '1') {
                            $applicationsHasAcceptStatusIsChanged->push($application);
                            $application->pivot->accept = '1';
                        }
                    } else {
                        if ($application->pivot->accept != '0') {
                            $applicationsHasAcceptStatusIsChanged->push($application);
                            $application->pivot->accept = '0';
                        }
                    }
                }
                if (isset($item['complete'])) {
                    if ($item['complete']) {
                        if ($application->pivot->complete != '1') {
                            $application->pivot->complete = '1';
                        }
                    } else {
                        if ($application->pivot->complete != '0') {
                            $application->pivot->complete = '0';
                        }
                    }
                }
                $application->pivot->save();
            }
        }
        $applications = new Collection;
        foreach ($job->shifts as $shift) {
            $applications = $applications->merge($shift->users()->wherePivot('user_id', $employee_id)->get());
        }
        return compact('applications', 'applicationsHasAcceptStatusIsChanged');
    }

    public function cancelPendingJob($user_id, $job_id)
    {
        $user                 = User::findOrFail($user_id);
        $pivot                = $user->applied_jobs()->wherePivot('job_id', $job_id)->firstOrFail();
        $pivot->pivot->active = 0;
        $pivot->pivot->save();
        return $user;
    }

    public function getApplications($job_id, $user_id = null)
    {
        $job          = $this->find($job_id);
        $applications = new Collection;
        foreach ($job->shifts as $shift) {
            if (isset($user_id)) {
                $applications = $applications->merge($shift->users()->wherePivot('user_id', $user_id)->get());
            } else {
                $applications = $applications->merge($shift->users()->get());
            }
        }
        return $applications;
    }
    public function getRelatedJob($id)
    {
        return $this->model->where('active', 1)->paginate(8);
    }
    public function isSentApplication($job_id, $user_id)
    {
        $job    = $this->find($job_id);
        $shifts = $job->shifts;
        $isSent = false;
        $shifts->each(function ($item) use ($user_id) {
            if ($item->users()->wherePivot('user_id', $user_id)->first()) {
                $isSent = true;
            }
        });
        return $isSent;
    }
    public function attachCategory($job, $category_id)
    {
        $category = Category::findOrFail($category_id);
        $job->categories()->attach($category);
        if ($category->parent_id != '0') {
            $job->categories()->attach(Category::find($category->parent_id));
        }
        return true;
    }
    public function updateCategory($job, $category_id)
    {
        $category   = Category::findOrFail($category_id);
        $categories = [$category_id];
        if ($category->parent_id != '0') {
            array_push($categories, $category->parent_id);
        }
        $job->categories()->sync($categories);
        return true;
    }
    public function getCategories($id)
    {
        return $this->find($id)->categories;
    }
    public function getAllicationOfUser($job_id, $user_id)
    {
        $job         = $this->find($job_id);
        $application = $job->applications()->wherePivot('user_id', $user_id)->firstOrFail();
        return $application;
    }
    public function saveShifts($job, $shifts)
    {
        $job->shifts()->saveMany($shifts);
    }
}
