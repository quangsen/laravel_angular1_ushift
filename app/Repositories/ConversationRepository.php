<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConversationRepository
 * @package namespace App\Repositories;
 */
interface ConversationRepository extends RepositoryInterface
{
    /**
     * Retrieve all conversations of employer
     *
     * @param \App\Entitites\User $user
     * @return Collection
     */
    public function getEmployerConversations($user);

    /**
     * Retrieve all conversations of employee
     *
     * @param \App\Entitites\User $user
     * @return Collection
     */
    public function getEmployeeConversations($user);

    /**
     * Attach a conversation to user
     *
     * @param \App\Entities\User
     * @param \App\Entities\Conversation
     * @return void
     */
    public function attachConversation($user, $conversation);

    /**
     * Retrieve all messages of a conversation
     *
     * @param int
     * @return Pagination
     */
    public function messages($id);

    /**
     * Store new message
     *
     * @param array $data
     * @param int $conversation_id
     * @return \App\Entities\Message
     */
    public function addMessage($data, $conversation_id);

    /**
     * Retrieve new messages
     *
     * @return int $conversation_id
     * @return int $message_id
     * @return int $sender_id
     * @return collection
     */
    public function newMessages($conversation_id, $message_id);

    /**
     * Retrieve old messages
     *
     * @return int $conversation_id
     * @return int $message_id
     * @return int $sender_id
     * @return collection
     */
    public function oldMessages($conversation_id, $message_id);

    /**
     * Create new conversation
     *
     * @param int $job_id
     * @param int $employee_id
     * @return \App\Entities\Conversation
     */
    public function createNewConversation($job_id, $employer_id, $employee_id);

    /**
     * Get conversation between 2 users for a job
     *
     * @param int $employee_id
     * @param int $employer_id
     * @param int $job_id
     * @return \App\Entities\Conversation
     */
    public function getConversation($employer_id, $employee_id, $job_id = null);
}
