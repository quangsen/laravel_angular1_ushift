<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EducationLevelRepository;
use App\Entities\EducationLevel;
use App\Validators\EducationLevelValidator;

/**
 * Class EducationLevelRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EducationLevelRepositoryEloquent extends BaseRepository implements EducationLevelRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EducationLevel::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
