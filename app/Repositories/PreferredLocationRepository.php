<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PreferredLocationRepository
 * @package namespace App\Repositories;
 */
interface PreferredLocationRepository extends RepositoryInterface
{
    //
}
