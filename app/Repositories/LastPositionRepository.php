<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LastPositionRepository
 * @package namespace App\Repositories;
 */
interface LastPositionRepository extends RepositoryInterface
{
    /**
     * Retrieve all last positions of a user
     *
     * @param int
     * @return array - aggregate of last position
     */
    public function lastLastPositionOfUser($id);
    /**
     * add new last positions of a user
     *
     * @param array $data = []
     * @param int $id
     * @return \Entities\LastPosition
     */
    public function addLastPositionOfUser($data = [], $id);

    /**
     * delete positions of a user
     *
     * @param int $user_id
     * @param int $last_position_id
     * @return void
     */
    public function removeLastPosition($user_id, $last_position_id);
}
