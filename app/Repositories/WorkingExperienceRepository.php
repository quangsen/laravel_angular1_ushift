<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WorkingExperienceRepository
 * @package namespace App\Repositories;
 */
interface WorkingExperienceRepository extends RepositoryInterface
{
    //
}
