<?php

namespace App\Repositories;

use App\Entities\Conversation;
use App\Entities\Job;
use App\Entities\Message;
use App\Entities\User;
use App\Repositories\ConversationRepository;
use Exception;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ConversationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ConversationRepositoryEloquent extends BaseRepository implements ConversationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Conversation::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getEmployerConversations($user)
    {
        $conversations = $user->employer_conversations()->orderBy('updated_at', 'desc')->simplePaginate(10);
        return $conversations;
    }

    public function getEmployeeConversations($user)
    {
        $conversations = $user->employee_conversations()->orderBy('updated_at', 'desc')->simplePaginate(10);
        return $conversations;
    }

    public function attachConversation($user, $conversation)
    {
        $user->employer_conversations()->attach($conversation);
    }

    public function messages($id)
    {
        $conversation = $this->find($id);
        $messages     = $conversation->messages()->orderBy('id', 'desc')->paginate();
        return $messages;
    }
    public function addMessage($data, $conversation_id)
    {
        $conversation = $this->find($conversation_id);
        return $conversation->messages()->save(new Message($data));
    }

    public function newMessages($conversation_id, $message_id)
    {
        $conversation = $this->find($conversation_id);
        $messages     = $conversation->messages()->where('id', '>', $message_id)->get();
        return $messages;
    }

    public function oldMessages($conversation_id, $message_id)
    {
        $conversation = $this->find($conversation_id);
        $messages     = $conversation->messages()->where('id', '<', $message_id)->orderBy('id', 'desc')->simplePaginate();
        return $messages;
    }

    public function createNewConversation($job_id, $employer_id, $employee_id)
    {
        $employee = User::findOrFail($employee_id);
        if ($employee->employee_conversations()->where('job_id', $job_id)->first()) {
            throw new Exception("Conversation already created", 1014);
        } else {
            $job = Job::findOrFail($job_id);
        }
        return $this->create(['title' => $job->title, 'job_id' => $job_id, 'employee_id' => $employee_id, 'employer_id' => $employer_id]);
    }

    public function getConversation($employer_id, $employee_id, $job_id = null)
    {
        $employee     = User::findOrFail($employee_id);
        $conversation = $employee->employee_conversations()->where('job_id', $job_id)->first();
        if (!$conversation) {
            if (isset($job_id)) {
                $job          = Job::findOrFail($job_id);
                $conversation = $this->create(['title' => $job->title, 'job_id' => $job_id, 'employer_id' => $employer_id, 'employee_id' => $employee_id]);
            } else {
                $conversation = $this->create(['title' => '', 'job_id' => '', 'employer_id' => $employer_id, 'employee_id' => $employee_id]);
            }
            $conversation->employer()->attach($employer_id);
        }
        return $conversation;
    }
}
