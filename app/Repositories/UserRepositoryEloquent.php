<?php

namespace App\Repositories;

use App\Entities\Employee;
use App\Entities\Employer;
use App\Entities\User;
use App\Repositories\UserRepository;
use Bican\Roles\Models\Role;
use Carbon\Carbon;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function verifyEmail($user)
    {
        if ($user->email_verified == 1) {
            throw new \Exception("email verified", 1006);
        }
        $user->email_verified = 1;
        $user->save();
        return $user;
    }
    public function createEmployeeProfile($attributes = [], $id)
    {
        $attributes['user_id'] = $id;
        return Employee::create($attributes);
    }
    public function updateEmployeeProfile($attributes = [], $id)
    {
        $user = $this->find($id);
        return $user->employee->update($attributes);
    }
    public function createEmployerProfile($attributes = [], $id)
    {
        $attributes['user_id'] = $id;
        return Employer::create($attributes);
    }
    public function updateEmployerProfile($attributes = [], $id)
    {
        $user = $this->find($id);
        return $user->employer->update($attributes);
    }
    public function updatePreferredPositons($positions = [], $id)
    {
        $user = $this->find($id);
        $user->employee->preferred_positions()->sync($positions);
    }
    public function updatePreferredLocations($locations = [], $id)
    {
        $user = $this->find($id);
        $user->employee->preferred_locations()->sync($locations);
    }
    public function updateWorkingExperience($working_experience_id, $id)
    {
        $user                                  = $this->find($id);
        $user->employee->working_experience_id = $working_experience_id;
        $user->employee->save();
    }
    public function attachRole($role, $id)
    {
        $user = $this->find($id);
        $role = Role::where('name', $role)->first();
        $user->attachRole($role);
        return $user;
    }

    public function isOwner($user_id, $job_id)
    {
        $user = $this->find($user_id);
        return !$user->jobs()->where('id', $job_id)->get()->isEmpty();
    }

    public function jobs($id, $active, $status, $per_page = 10)
    {
        $user = $this->find($id);
        $jobs = $user->jobs()->where('active', $active);
        switch ($status) {
            case 'active':
                $jobs = $jobs->where('complete', '0');
                break;
            case 'today':
                $jobs = $jobs->whereRaw('Date(created_at) = CURDATE()');
                break;
            case 'archived':
                $jobs = $jobs->where('complete', '1');
                break;

            default:
                # code...
                break;
        }
        $jobs = $jobs->orderBy('id', 'DESC')->simplePaginate($per_page);
        return $jobs;
    }

    public function getJobsOfEmployee($id, $active = 1)
    {
        $user = $this->find($id);
        $jobs = $user->applied_jobs()->orderBy('id', 'desc')->where('jobs.active', $active)->paginate(5);
        return $jobs;
    }

    public function employeePendingOrCompletedJobs($id, $paginate = null, $is_accept = 0)
    {
        $user         = $this->find($id);
        $applications = $user->applied_jobs()->where('jobs.active', 1)->wherePivot('active', 1)->wherePivot('accept', $is_accept)->paginate($paginate);
        return $applications;
    }

    public function employeeCompletedJobs($id, $paginate = null)
    {
        $user         = $this->find($id);
        $applications = $user->applied_jobs()->where('jobs.active', 1)->wherePivot('active', 1)->wherePivot('accept', 1)->paginate($paginate);
        return $applications;
    }

    public function getAppliedShifts($id, $accept, $complete, $period, $per_page = 10)
    {
        $user   = $this->find($id);
        $shifts = $user->shifts()->where('active', 1)->wherePivot('complete', $complete)->wherePivot('accept', $accept)->with('job')->OrderBy('start_time');
        if ($period == 'upcomming') {
            $shifts->where('start_time', '>=', Carbon::now());
        }
        if ($period == 'past') {
            $shifts->where('start_time', '<', Carbon::now());
        }
        return $shifts->simplePaginate($per_page);
    }
}
