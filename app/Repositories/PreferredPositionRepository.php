<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EducationLevelRepository
 * @package namespace App\Repositories;
 */
interface PreferredPositionRepository extends RepositoryInterface
{
    //
}
