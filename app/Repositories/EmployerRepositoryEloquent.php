<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EmployerRepository;
use App\Entities\Employer;
use App\Validators\EmployerValidator;

/**
 * Class EmployerRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EmployerRepositoryEloquent extends BaseRepository implements EmployerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Employer::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
