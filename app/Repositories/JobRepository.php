<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobRepository
 * @package namespace App\Repositories;
 */
interface JobRepository extends RepositoryInterface
{

    /**
     * get all resource of Job
     *
     * @param array $condition
     * @return Collection
     */
    public function getAll($condition = []);

    /**
     * Employee apply job
     *
     * @param int $job_id
     * @param int $user_id
     * @param string $request
     * @param array $shift_ids
     * @return boolean
     */
    public function apply($job_id, $user_id, $request, $shift_ids);

    /**
     * Employer accept / reject the request
     *
     * @param int $employer_id
     * @param int $job_id
     * @param int $employee_id
     * @param array $data
     * @return boolean
     */
    public function changeApplicationStatus($employer_id, $job_id, $employee_id, $data);

    /**
     * Cancel pending job by employee
     * @param  int $user_id
     * @param  int $job_id
     * @param  int $employee_id
     * @return \Entities\User
     */
    public function cancelPendingJob($user_id, $job_id);

    /**
     * Retrieve all applications of a job
     *
     * @param int job_id
     * @param int user_id
     * @return collection
     */
    public function getApplications($job_id, $user_id = null);

    /**
     * Retrieve related jobs
     *
     * @param int $id
     * @return collection of \Entities\Job
     */
    public function getRelatedJob($id);
    /**
     * check if user already apply in a job
     *
     * @param int $job_id
     * @param int $userid
     * @return boolean
     */
    public function isSentApplication($job_id, $user_id);

    /**
     * attach a category to job
     *
     *
     */
    public function attachCategory($job, $category_id);
    /**
     * attach a category to job
     *
     *
     */
    public function updateCategory($job, $category_id);

    /**
     * save shifts to job
     *
     * @param \App\Entities\Job $job
     * @param Array \App\Entities\Shift $shifts
     * @return void
     */
    public function saveShifts($job, $shifts);

    /**
     * all categories of job
     *
     *
     */
    public function getCategories($id);

}
