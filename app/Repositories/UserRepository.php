<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    /**
     * verify user email
     *
     * @param \Entities\User
     * @return \Entities\User
     */
    public function verifyEmail($user);

    /**
     * create employee profile
     *
     * @param array $attributes
     * @param int $id
     * @return \Entities\Employee
     */
    public function createEmployeeProfile($attributes = [], $id);

    /**
     * update employee profile
     *
     * @param array $attributes
     * @param int $id
     * @return \Entities\Employee
     */
    public function updateEmployeeProfile($attributes = [], $id);

    /**
     * create employer profile
     *
     * @param array $attributes
     * @param int $id
     * @return \Entities\Employer
     */
    public function createEmployerProfile($attributes = [], $id);

    /**
     * update employer profile
     *
     * @param array $attributes
     * @param int $id
     * @return \Entities\Employer
     */
    public function updateEmployerProfile($attributes = [], $id);
    /**
     * update preferred position profile
     *
     * @param array
     * @param int $id
     * @return void
     */
    public function updatePreferredPositons($positions = [], $id);
    /**
     * update preferred position profile
     *
     * @param int $working_experience_id
     * @param int $id
     * @return void
     */
    public function updateWorkingExperience($working_experience_id, $id);

    /**
     * Attach role to user object
     *
     * @param string $role
     * @param int $id
     * @return \Entities\User
     */
    public function attachRole($role, $id);

    /**
     * check user is owner of a job
     *
     * @param int $user_id
     * @param int $job_id
     * @return boolean
     */
    public function isOwner($user_id, $job_id);

    /**
     * Retrieve all jobs active of employer
     * @param  int $id
     * @param  boolean $active
     * @param  boolean $complete
     * @param  int $per_page
     * @return Collection
     */
    public function jobs($id, $active, $complete, $per_page = 10);

    /**
     * Retrieve all jobs active of employee
     * @param  int $user_id
     * @param  boolean|int $active
     * @return Collection
     */
    public function getJobsOfEmployee($user_id, $active);

    /**
     * Retrieve all pending jobs of a employee
     *
     * @param int $user_id
     * @return  Collection
     */
    public function employeePendingOrCompletedJobs($user_id, $paginate = null, $is_accept = 0);

    /**
     * Retrieve all completed jobs of a employee
     *
     * @param int $user_id
     * @return  Collection
     */
    public function employeeCompletedJobs($user_id, $paginate);

    /**
     * list of all applied shift of employee
     *
     * @param int $id
     * @param boolean $accept
     * @param boolean $complete
     * @param string $period (upcomming|past)
     * @param int $per_page
     * @return Collection
     */
    public function getAppliedShifts($id, $accept, $complete, $period, $per_page = 10);
}
