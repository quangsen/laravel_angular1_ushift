<?php

namespace App\Providers;

use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\ConversationRepository;
use App\Repositories\ConversationRepositoryEloquent;
use App\Repositories\EducationLevelRepository;
use App\Repositories\EducationLevelRepositoryEloquent;
use App\Repositories\IndustryRepository;
use App\Repositories\IndustryRepositoryEloquent;
use App\Repositories\JobRepository;
use App\Repositories\JobRepositoryEloquent;
use App\Repositories\LastPositionRepository;
use App\Repositories\LastPositionRepositoryEloquent;
use App\Repositories\MessageRepository;
use App\Repositories\MessageRepositoryEloquent;
use App\Repositories\PreferredLocationRepository;
use App\Repositories\PreferredLocationRepositoryEloquent;
use App\Repositories\PreferredPositionRepository;
use App\Repositories\PreferredPositionRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\WorkingExperienceRepository;
use App\Repositories\WorkingExperienceRepositoryEloquent;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind(UserRepository::class, UserRepositoryEloquent::class);
        App::bind(JobRepository::class, JobRepositoryEloquent::class);
        App::bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        App::bind(EducationLevelRepository::class, EducationLevelRepositoryEloquent::class);
        App::bind(IndustryRepository::class, IndustryRepositoryEloquent::class);
        App::bind(LastPositionRepository::class, LastPositionRepositoryEloquent::class);
        App::bind(PreferredPositionRepository::class, PreferredPositionRepositoryEloquent::class);
        App::bind(PreferredLocationRepository::class, PreferredLocationRepositoryEloquent::class);
        App::bind(WorkingExperienceRepository::class, WorkingExperienceRepositoryEloquent::class);
        App::bind(ConversationRepository::class, ConversationRepositoryEloquent::class);
        App::bind(MessageRepository::class, MessageRepositoryEloquent::class);
        App::bind('message', function () {
            return new \App\Message\Message;
        });
    }
}
