<?php

namespace App\Providers;

use App\Events\ApplicationStatusWasChanged;
use App\Events\FileUpload;
use App\Events\JobCreated;
use App\Events\JobStatusWasChanged;
use App\Events\MessageLoaded;
use App\Events\UserApplied;
use App\Listeners\ApplicationStatusWasChangedListener;
use App\Listeners\FileUploadListener;
use App\Listeners\JobCreatedListener;
use App\Listeners\JobStatusWasChangedListener;
use App\Listeners\MessageLoadedListener;
use App\Listeners\UserAppliedListener;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        FileUpload::class                  => [FileUploadListener::class],
        JobCreated::class                  => [JobCreatedListener::class],
        UserApplied::class                 => [UserAppliedListener::class],
        JobStatusWasChanged::class         => [JobStatusWasChangedListener::class],
        ApplicationStatusWasChanged::class => [ApplicationStatusWasChangedListener::class],
        MessageLoaded::class               => [MessageLoadedListener::class],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\UserEventListener',
    ];
}
