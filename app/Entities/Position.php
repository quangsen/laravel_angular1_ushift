<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table    = 'positions';
    protected $fillable = ['name', 'industry_id'];
}
