<?php

namespace App\Entities;

use App\Entities\Employee;
use App\Entities\PreferredPosition;
use Illuminate\Database\Eloquent\Model;

class PreferredPosition extends Model
{
    protected $table    = 'preferred_positions';
    protected $fillable = ['name', 'industry_id'];

    public function industry()
    {
        return $this->belongsTo(PreferredPosition::class, 'industry_id', 'id');
    }
}
