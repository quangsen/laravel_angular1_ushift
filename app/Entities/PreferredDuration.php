<?php

namespace App\Entities;

use App\Entities\PreferredPosition;
use Illuminate\Database\Eloquent\Model;

class PreferredDuration extends Model
{
    protected $table    = 'preferred_duration';
    protected $fillable = ['name'];

}
