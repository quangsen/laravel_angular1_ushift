<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    protected $table    = 'occupations';
    protected $fillable = ['name'];
}
