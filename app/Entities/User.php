<?php

namespace App\Entities;

use App\Entities\Conversation;
use App\Entities\Employee;
use App\Entities\Employer;
use App\Entities\Job;
use App\Entities\Shift;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use TransformableTrait;
    use HasRoleAndPermission;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'image',
        'cover',
        'mobile',
        'gender',
        'birth',
        'address',
        'zipcode',
        'city',
        'country',
        'summary',
        'is_updated_profile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get username
     *
     * @return string
     */
    public function getName()
    {
        return $this->first_name . $this->last_name;
    }

    /**
     * generate jwt token
     *
     * @return string
     */
    public function getToken()
    {
        return JWTAuth::fromUser($this);
    }

    /**
     * Generate email verify token
     *
     * @return string
     */
    public function getEmailVerifyToken()
    {
        return Hash::make($this->email);
    }

    /**
     * Relationship with employee table
     */
    public function employee()
    {
        return $this->hasOne(Employee::class, 'user_id');
    }

    public function employer()
    {
        return $this->hasOne(Employer::class, 'user_id');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class, 'employer_id', 'id');
    }

    public function shifts()
    {
        return $this->belongsToMany(Shift::class, 'shift_user')->withPivot('request', 'user_id', 'job_id', 'accept', 'active', 'complete')->withTimestamps();
    }

    public function employer_conversations()
    {
        return $this->belongsToMany(Conversation::class);
    }

    public function employee_conversations()
    {
        return $this->hasMany(Conversation::class, 'employee_id', 'id');
    }
}
