<?php

namespace App\Entities;

use App\Entities\Employee;
use App\Entities\PreferredPosition;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table    = 'industries';
    protected $fillable = ['name'];

    public function employee()
    {
        return $this->hasMany(Employee::class, 'industry_id', 'id');
    }

    public function preferred_positions()
    {
        return $this->hasMany(PreferredPosition::class, 'industry_id', 'id');
    }
}
