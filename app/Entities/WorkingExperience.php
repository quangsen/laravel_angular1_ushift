<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class WorkingExperience extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'working_experiences';

    protected $fillable = ['name'];

    public function employee()
    {
        return $this->hasMany(Employee::class, 'working_experience_id');
    }

}
