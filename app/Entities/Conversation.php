<?php

namespace App\Entities;

use App\Entities\Message;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Conversation extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['employee_id', 'employer_id', 'title', 'job_id'];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }

    public function employer()
    {
        return $this->belongsToMany(User::class);
    }

}
