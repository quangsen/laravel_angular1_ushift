<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name', 'parent_id', 'level'];

    public function children()
    {
    	return $this->where('parent_id', $this->id)->get();
    }

    public function jobs() {
    	return $this->belongsToMany(Job::class);
    }

}
