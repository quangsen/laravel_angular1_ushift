<?php

namespace App\Entities;

use App\Entities\Employee;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EducationLevel extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name'];

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

}
