<?php

namespace App\Entities;

use App\Entities\EducationLevel;
use App\Entities\Industry;
use App\Entities\LastPosition;
use App\Entities\PreferredLocation;
use App\Entities\WorkingExperience;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Employee extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'employee';

    protected $fillable = [
        'user_id',
        'allow_to_work_in_sg',
        'kind_of_work',
        'hours_per_week',
        'current_position',
        'industry_id',
        'education_level_id',
        'resume',
    ];

    public function industry()
    {
        return $this->belongsTo(Industry::class);
    }
    public function education()
    {
        return $this->belongsTo(EducationLevel::class, 'education_level_id', 'id');
    }

    public function setAllow_to_work_in_sgAttribute($value)
    {
        if ($value === false || $value === 'false') {
            $value = false;
        } else {
            $value = true;
        }
        $this->attributes['allow_to_work_in_sg'] = $value;
    }

    public function preferred_positions()
    {
        return $this->belongsToMany(PreferredPosition::class, 'employee_preferred_position', 'employee_id', 'position_id');
    }

    public function preferred_locations()
    {
        return $this->belongsToMany(PreferredLocation::class, 'employee_preferred_location', 'employee_id', 'pre_location_id');
    }

    public function working_experience()
    {
        return $this->belongsTo(WorkingExperience::class, 'working_experience_id');
    }

    public function last_positions()
    {
        return $this->hasMany(LastPosition::class, 'employee_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
