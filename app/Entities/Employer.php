<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Employer extends Model implements Transformable {
	use TransformableTrait;

	protected $table = 'employer';

	protected $fillable = [
		'user_id',
		'company_name',
		'company_about',
		'registed_number',
		'company_address',
		'number_of_outlets',
		'company_main_activity',
		'preferred_contact_method',
		'preferred_contact_time',
	];

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

}
