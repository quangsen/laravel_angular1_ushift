<?php

namespace App\Entities;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $fillable = [
        'description',
        'start_time',
        'end_time',
        'salary',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'shift_user')->withPivot('request', 'user_id', 'job_id', 'accept', 'active', 'complete')->withTimestamps();
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
