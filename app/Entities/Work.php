<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $table = 'works';
}
