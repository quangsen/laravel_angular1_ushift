<?php

namespace App\Entities;

use App\Entities\Employee;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LastPosition extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'last_positions';

    protected $fillable = [
        'employee_id',
        'company_name',
        'country',
        'role',
        'type',
        'start_date',
        'end_date',
        'reason_of_leaving',
        'name_of_manager',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

}
