<?php

namespace App\Entities;

use App\Entities\Conversation;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Message extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['type', 'conversation_id', 'content', 'sender_id'];
    protected $touches  = ['conversation'];
    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }
    /**
     * $types = [1 => 'normal message', 4 => 'new user has applied'];
     */
}
