<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $table    = 'availabilities';
    protected $fillable = ['name'];
}
