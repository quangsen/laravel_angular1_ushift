<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Shift;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Job extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'employer_id',
        'title',
        'slug',
        'image',
        'address',
        'latitude',
        'longitude',
        'short_desc',
        'description',
        'note',
        'type',
        'requirement',
        'active',
        'complete',
    ];

    protected $with = ['shifts'];

    public function employer()
    {
        return $this->belongsTo(User::class, 'employer_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function shifts()
    {
        return $this->hasMany(Shift::class);
    }
    public function available_shifts()
    {
        return $this->hasMany(Shift::class)->where('created_at', '>=', Carbon::today());
    }
}
