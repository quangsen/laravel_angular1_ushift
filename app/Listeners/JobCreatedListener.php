<?php

namespace App\Listeners;

use App\Events\JobCreated;
use Illuminate\Support\Facades\Mail;

class JobCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobCreated  $event
     * @return void
     */
    public function handle(JobCreated $event)
    {
        $job  = $event->job;
        $user = $event->user;
        Mail::send('emails.job.employer.created', ['job' => $job], function ($mail) use ($user) {
            $mail->from('support@ushift.com', 'Ushift');

            $mail->to($user->email, $user->getName())->subject('New job has been created successfully ');
        });
    }
}
