<?php

namespace App\Listeners;

use App\Entities\Job;
use App\Entities\User;
use App\Events\ApplicationStatusWasChanged;
use App\Message\Facade\Message;
use Illuminate\Support\Facades\Mail;

class ApplicationStatusWasChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApplicationStatusWasChanged  $event
     * @return void
     */
    public function handle(ApplicationStatusWasChanged $event)
    {
        $employer = $event->employer;
        $employee = User::find($event->employee_id);
        $data     = $event->data;
        $job      = Job::find($event->job_id);

        if (!$applicationHasBeenAccept = $data['applicationsHasAcceptStatusIsChanged']->isEmpty()) {
            $applicationHasBeenAccept = $data['applicationsHasAcceptStatusIsChanged']->search(function ($item) {return $item->pivot->accept == '1';}) !== false;

            if ($applicationHasBeenAccept) {
                $subject = 'Your application has been accepted';
            } else {
                $subject = 'Your application has been cancelled';
            }

            Mail::send('emails.job.employee.application_status_was_changed', compact('job', 'employee', 'employer', 'applicationHasBeenAccept'), function ($mail) use ($employee, $subject) {
                $mail->from('support@ushift.com', 'Ushift');
                $mail->to($employee->email, $employee->getName())->subject($subject);
            });

            $message = new \App\Entities\Message([
                'type'      => 5,
                'content'   => json_encode([
                    'employee_id'              => $employee->id,
                    'job_slug'                 => $job->slug,
                    'applicationHasBeenAccept' => $applicationHasBeenAccept,
                ]),
                'sender_id' => $employer->id,
            ]);
            Message::send($employer, $employee->id, $job->id, $message);
        }
    }
}
