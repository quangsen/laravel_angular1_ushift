<?php

namespace App\Listeners;

use App\Entities\Conversation;
use App\Events\MessageLoaded;

class MessageLoadedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MessageLoaded  $event
     * @return void
     */
    public function handle(MessageLoaded $event)
    {
        Conversation::find($event->conversation_id)->messages()->update(['unread' => 0]);
    }
}
