<?php

namespace App\Listeners;

use App\Events\JobStatusWasChanged;
use App\Message\Facade\Message;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class JobStatusWasChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobStatusWasChanged  $event
     * @return void
     */
    public function handle(JobStatusWasChanged $event)
    {
        $job      = $event->job;
        $employer = $event->user;
        /**
         * Send email notify to Employer
         */
        Mail::send('emails.job.employer.job_status_was_chaged', ['job' => $job, 'user' => $employer], function ($mail) use ($employer) {
            $mail->from('support@ushift.com', 'Ushift');

            $mail->to($employer->email, $employer->getName())->subject('The job has been completed');
        });

        /**
         * Send email notify to all accepted shift
         */
        $shifts          = $job->shifts;
        $userWasAccepted = new Collection;
        foreach ($shifts as $shift) {
            $userWasAccepted = $userWasAccepted->merge($shift->users()->wherePivot('accept', 1)->get());
        }
        $userWasAccepted = $userWasAccepted->unique('id');
        foreach ($userWasAccepted as $employee) {
            Mail::send('emails.job.employee.job_completed', compact('employee'), function ($mail) use ($employee) {
                $mail->from('support@ushift.com', 'Ushift');
                $mail->to($employee->email, $employee->getName())->subject('The job has been completed');
            });

            $message = new \App\Entities\Message([
                'type'      => 2,
                'content'   => json_encode([
                    'job_id'   => $job->id,
                    'job_slug' => $job->slug,
                ]),
                'sender_id' => $employer->id,
            ]);
            Message::send($employer, $employee->id, $job->id, $message);
        }
    }
}
