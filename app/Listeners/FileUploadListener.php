<?php

namespace App\Listeners;

use App\Events\FileUpload;

class FileUploadListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FileUploadEvent  $event
     * @return void
     */
    public function handle(FileUpload $event)
    {
        
    }
}
