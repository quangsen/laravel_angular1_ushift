<?php

namespace App\Listeners;

use App\Entities\Job;
use App\Events\UserApplied;
use App\Message\Facade\Message;
use Mail;

class UserAppliedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserApplied  $event
     * @return void
     */
    public function handle(UserApplied $event)
    {
        $employee = $event->employee;
        $job_id   = $event->job_id;
        $shifts   = $event->shifts;

        $job      = Job::find($job_id);
        $employer = $job->employer;
        Mail::send('emails.job.employer.applied', ['job' => $job, 'employee' => $employee], function ($mail) use ($employer) {
            $mail->from('support@ushift.com', 'Ushift');

            $mail->to($employer->email, $employer->getName())->subject('You have new application');
        });

        $message = new \App\Entities\Message([
            'type'      => 4,
            'content'   => json_encode(['employee_id' => $employee->id, 'job_slug' => $job->slug]),
            'sender_id' => $employee->id,
        ]);

        Message::send($employee, $employer->id, $job_id, $message);
    }
}
