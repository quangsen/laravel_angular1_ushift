<?php

namespace App\Listeners;

use App\Entities\User;
use App\Events\UserLogin;
use App\Events\UserRegister;
use Illuminate\Support\Facades\Mail;
use Log;

class UserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event when user register
     *
     * @param UserRegister $event
     * @return void
     */
    public function onUserRegister(UserRegister $event)
    {
        $user = $event->user;
        Mail::send('emails.register_confirmation', ['user' => $user], function ($mail) use ($user) {
            $mail->from('support@ushift.com', 'Ushift');

            $mail->to($user->email, $user->getName())->subject('New user confirmation');
        });
    }
    /**
     * Handle the event when user login
     *
     * @param UserRegister $event
     * @return void
     */
    public function onUserLogin(UserLogin $event)
    {
        // user login handler
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen('App\Events\UserRegister', 'App\Listeners\UserEventListener@onUserRegister');
        $events->listen('App\Events\UserLogin', 'App\Listeners\UserEventListener@onUserLogin');
    }

}
