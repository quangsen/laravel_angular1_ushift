<?php

namespace App\Events;

use App\Entities\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserApplied extends Event
{
    use SerializesModels;

    public $employee;
    public $job_id;
    public $shifts;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $employee, $job_id, $shifts)
    {
        $this->employee = $employee;
        $this->job_id   = $job_id;
        $this->shifts   = $shifts;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
