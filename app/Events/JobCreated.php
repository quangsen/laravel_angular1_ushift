<?php

namespace App\Events;

use App\Entities\Job;
use App\Entities\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class JobCreated extends Event
{
    use SerializesModels;

    public $job;
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $user)
    {
        $this->job      = $job;
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
