<?php

namespace App\Events;

use App\Entities\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ApplicationStatusWasChanged extends Event
{
    use SerializesModels;

    public $employer;
    public $employee_id;
    public $job_id;
    public $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $employer, $employee_id, $job_id, $data)
    {
        $this->employer    = $employer;
        $this->employee_id = $employee_id;
        $this->job_id      = $job_id;
        $this->data        = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
