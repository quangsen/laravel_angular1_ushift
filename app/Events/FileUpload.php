<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class FileUpload extends Event
{
    use SerializesModels;

    public $request;
    public $path;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Request $request, $path)
    {
        $this->request = $request;
        $this->path    = $path;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
