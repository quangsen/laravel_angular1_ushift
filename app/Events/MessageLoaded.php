<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageLoaded extends Event
{
    use SerializesModels;

    public $conversation_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($conversation_id)
    {
        $this->conversation_id = $conversation_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
