var gulp = require('gulp');
var jshint = require('gulp-jshint');
module.exports = function() {
    return gulp.src('./frontend/js/hybird.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
}
