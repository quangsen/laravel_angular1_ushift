var gulp = require('gulp');
var browserify = require('gulp-browserify');
module.exports = function() {
    return gulp.src('./frontend/js/hybird.js')
        .pipe(browserify({
            insertGlobals: true
        }))
        .pipe(gulp.dest('public/assets/js'));
}
