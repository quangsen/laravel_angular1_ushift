var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    gulp.src([
    		'./frontend/bower_components/components-font-awesome/css/font-awesome.css',
            './frontend/bower_components/Materialize/dist/css/materialize.css',
            './frontend/bower_components/angular-fx/dist/angular-fx.min.css',
            './frontend/bower_components/dropzone/dist/min/dropzone.min.css',
            './frontend/bower_components/angular-xeditable/dist/css/xeditable.css',
            './frontend/bower_components/trix/dist/trix.css',
            './frontend/bower_components/angular-material/angular-material.min.css',
            './frontend/bower_components/angular-material-datetimepicker/css/material-datetimepicker.css'
        ])
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(concat("library.min.css"))
        .pipe(gulp.dest('public/assets/css'));
};