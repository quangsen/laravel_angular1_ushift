<?php

use App\Entities\PreferredDuration;
use Illuminate\Database\Seeder;

class PreferredDurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $durations = [

        ];
        foreach ($durations as $duration) {
            PreferredDuration::create(['name' => $duration]);
        }
    }
}
