<?php

use App\Entities\WorkingExperience;
use Illuminate\Database\Seeder;

class WorkingExperienceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'not yet',
            'less than 1 month',
            '1-3 month',
            '4 - 12 month',
            '1 -2 years',
            'more than 2 years',
        ];
        foreach ($data as $value) {
            WorkingExperience::create(['name' => $value]);
        }
    }
}
