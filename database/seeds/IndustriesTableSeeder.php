<?php

use App\Entities\Industry;
use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industries = [
            'receptionist',
            'admin assistant',
            'personal assistant',
            'call handler',
        ];
        foreach ($industries as $industry) {
            Industry::create(['name' => $industry]);
        }
    }
}
