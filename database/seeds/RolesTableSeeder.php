<?php

use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create([
            'name'        => 'Admin',
            'slug'        => 'admin',
        ]);

        $employee = Role::create([
            'name'  => 'Employee',
            'slug'  => 'employee',
        ]);

        $employer = Role::create([
            'name'  => 'Employer',
            'slug'  => 'employer',
        ]);
    }
}
