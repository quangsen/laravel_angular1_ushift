<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'restaurant service & hospitality' => ['bar-tender', 'chef', 'waiter'],
            'event'                            => ['PG'],
            'drivers'                          => ['taxi driver', 'bus driver'],
        ];
        foreach ($categories as $key => $subcategories) {
            $parent_category        = new Category;
            $parent_category->name  = $key;
            $parent_category->level = 1;
            $parent_category->save();
            foreach ($subcategories as $name) {
                $subcategory            = new Category;
                $subcategory->name      = $name;
                $subcategory->level     = 2;
                $subcategory->parent_id = $parent_category->id;
                $subcategory->save();
            }
        }
    }
}
