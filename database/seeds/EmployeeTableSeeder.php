<?php

use App\Entities\Employee;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = Employee::create([
            'user_id'               => '3',
            'allow_to_work_in_sg'   => 1,
            'kind_of_work'          => 'fulltime',
            'hours_per_week'        => '30',
            'industry_id'           => '2',
            'education_level_id'    => '5',
            'working_experience_id' => '3',
            'resume'                => 'http://www.orimi.com/pdf-test.pdf',
        ]);

        $employee->preferred_positions()->attach(1);
        $employee->preferred_locations()->attach(1);
    }
}
