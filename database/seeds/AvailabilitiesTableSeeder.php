<?php

use App\Entities\Availability;
use Illuminate\Database\Seeder;

class AvailabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $availabilites = ['monday', 'tuesday', 'wednesday', 'thusday', 'friday', 'saturday', 'sunday'];
        foreach ($availabilites as $availability) {
            Availability::create(['name' => $availability]);
        }
    }
}
