<?php

use App\Entities\LastPosition;
use Illuminate\Database\Seeder;

class LastPositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'employee_id'       => '1',
            'company_name'      => 'Codersvn',
            'country'           => 'Vietnam',
            'role'              => 'Manager',
            'type'              => 'Restaurant',
            'start_date'        => '2010-01-01',
            'end_date'          => '2099-01-01',
            'reason_of_leaving' => 'I don\'t know',
            'name_of_manager'   => 'Hieupv',
        ];
        LastPosition::create($data);
    }
}
