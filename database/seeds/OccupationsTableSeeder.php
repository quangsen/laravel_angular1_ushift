<?php

use App\Entities\Occupation;
use Illuminate\Database\Seeder;

class OccupationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $occupations = [
            'morning',
            'afternoon',
            'evening',
            'night',
            'after midnight',
            'any',
        ];
        foreach ($occupations as $occupation) {
            Occupation::create(['name' => $occupation]);
        }
    }
}
