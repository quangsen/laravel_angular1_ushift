<?php

use App\Entities\Employer;
use Illuminate\Database\Seeder;

class EmployerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\en_SG\PhoneNumber($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $employers = [
            [
                'user_id'                  => 4,
                'company_name'             => $faker->company(),
                'registed_number'          => '123456789',
                'company_address'          => $faker->address(),
                'company_about'            => $faker->paragraph(),
                'number_of_outlets'        => 'multiple outlet company',
                'company_main_activity'    => 'Import / Export',
                'preferred_contact_method' => 'Skype: hieupv@codersvn',
                'preferred_contact_time'   => 'GMT+7 8AM - 17PM',
            ],
        ];
        foreach ($employers as $employer) {
            Employer::create($employer);
        }
    }
}
