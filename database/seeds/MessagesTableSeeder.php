<?php

use App\Entities\Conversation;
use App\Entities\Message;
use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conversations = Conversation::all();

        foreach ($conversations as $conversation) {
            for ($i = 0; $i < 50; $i++) {
                $faker = new Faker\Generator();
                $faker->addProvider(new Faker\Provider\Lorem($faker));
                $rand    = mt_rand(0, 1);
                $message = [
                    'type'            => 1,
                    'conversation_id' => $conversation->id,
                    'content'         => $faker->sentence(),
                    'sender_id'       => $rand ? $conversation->employee_id : '4',
                ];
                Message::create($message);
            }
        }
    }
}
