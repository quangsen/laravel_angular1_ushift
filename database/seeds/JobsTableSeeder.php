<?php

use App\Entities\Category;
use App\Entities\Job;
use App\Entities\Shift;
use App\Entities\User;
use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $employees = User::whereHas('roles', function ($q) {
            $q->where('slug', 'employee');
        })->take(5)->get();

        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $description = '';
        foreach ($faker->paragraphs() as $paragraph) {
            $description .= '<p>' . $paragraph . '</p>';
        }

        $title      = $faker->sentence();
        $slug       = str_slug($title);
        $slug_index = 1;
        while (!Job::where('slug', $slug)->get()->isEmpty()) {
            $slug = $slug . '-' . $slug_index;
            $slug_index++;
        }
        $jobs = [
            [
                'employer_id' => 4,
                'title'       => $title,
                'slug'        => $slug,
                'image'       => '/uploads/default_job_image.png',
                'address'     => $faker->address(),
                'latitude'    => $faker->latitude(),
                'longitude'   => $faker->longitude(),
                'short_desc'  => $faker->paragraph(),
                'description' => $description,
                'note'        => $faker->sentence(),
                'type'        => mt_rand(0, 1) ? 'parttime' : 'fulltime',
                'requirement' => 'No requirement',
                'active'      => 1,
                'complete'    => mt_rand(0, 1),
            ],
        ];
        foreach ($jobs as $job) {
            $job = Job::create($job);
            $job->categories()->attach(Category::find(2));
            $job->categories()->attach(Category::find(1));

            $shifts = [
                new Shift([
                    'description' => 'Morning',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 08:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 12:00:00',
                ]),
                new Shift([
                    'description' => 'Afternoon',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 13:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 17:00:00',
                ]),
                new Shift([
                    'description' => 'Evenning',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 17:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 22:00:00',
                ]),
            ];
            $job->shifts()->saveMany($shifts);

            $job->shifts->each(function ($item) use ($employees, $faker) {
                $employees->map(function ($user) use ($faker, $item) {
                    $item->users()->attach($user->id, ['request' => $faker->sentence(), 'job_id' => $item->id, 'accept' => 0]);
                });
            });

            // foreach ($employees as $employee) {
            //     $kind_of_work = mt_rand(0, 1) ? 'parttime' : 'fulltime';
            //     $job->applications()->attach($employee, ['request' => $faker->sentence(), 'shift_id' => $job->shifts()->first()->id, 'price' => mt_rand(1000, 5000), 'accept' => 0]);
            // }

        }
        for ($i = 0; $i < 100; $i++) {
            $faker = new Faker\Generator();
            $faker->addProvider(new Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
            $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
            $faker->addProvider(new Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
            $faker->addProvider(new Faker\Provider\DateTime($faker));
            $faker->addProvider(new Faker\Provider\Internet($faker));
            $description = '';
            foreach ($faker->paragraphs() as $paragraph) {
                $description .= '<p>' . $paragraph . '</p>';
            }

            $title      = $faker->sentence();
            $slug       = str_slug($title);
            $slug_index = 1;
            while (!Job::where('slug', $slug)->get()->isEmpty()) {
                $slug = $slug . '-' . $slug_index;
                $slug_index++;
            }
            $job = [
                'employer_id' => 4,
                'title'       => $title,
                'slug'        => $slug,
                'image'       => '/uploads/default_job_image.png',
                'address'     => $faker->address(),
                'latitude'    => $faker->latitude(),
                'longitude'   => $faker->longitude(),
                'short_desc'  => $faker->paragraph(),
                'description' => $description,
                'note'        => $faker->sentence(),
                'type'        => mt_rand(0, 1) ? 'parttime' : 'fulltime',
                'requirement' => 'No requirement',
                'active'      => 1,
                'complete'    => mt_rand(0, 1),
            ];
            $job         = Job::create($job);
            $category_id = mt_rand(1, 9);
            $category    = Category::find($category_id);
            $job->categories()->attach($category);
            if ($category->parent_id != '0') {
                $job->categories()->attach(Category::find($category->parent_id));
            }
            $shifts = [
                new Shift([
                    'description' => 'Morning',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 08:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 12:00:00',
                ]),
                new Shift([
                    'description' => 'Afternoon',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 13:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 17:00:00',
                ]),
                new Shift([
                    'description' => 'Evenning',
                    'salary'      => mt_rand(10, 1500),
                    'start_time'  => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 17:00:00',
                    'end_time'    => $faker->dateTimeBetween($statDate = '-10 days', $endData = '+30 days')->format('Y-m-d') . ' 22:00:00',
                ]),
            ];
            $job->shifts()->saveMany($shifts);

            $job->shifts->each(function ($item) use ($employees, $faker) {
                $employees->map(function ($user) use ($faker, $item) {
                    $item->users()->attach($user->id, ['request' => $faker->sentence(), 'job_id' => $item->id, 'accept' => 0]);
                });
            });

            // foreach ($employees as $employee) {
            //     $kind_of_work = mt_rand(0, 1) ? 'parttime' : 'fulltime';
            //     $job->applications()->attach($employee, ['request' => $faker->sentence(), 'shift_id' => $job->shifts()->first()->id, 'price' => mt_rand(1000, 5000), 'accept' => 0]);
            // }
        }
    }
}
