<?php

use App\Entities\User;
use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole    = Role::find(1);
        $employeeRole = Role::find(2);
        $employerRole = Role::find(3);
        $users        = [
            'admin'    => [
                'email' => 'admin@ushift.com',
                'role'  => 'adminRole',
            ],
            'hieupv'   => [
                'email' => 'hieupv.est@gmail.com',
                'role'  => 'adminRole',
            ],
            'employee' => [
                'email' => 'employee@ushift.com',
                'role'  => 'employeeRole',
            ],
            'employer' => [
                'email' => 'employer@ushift.com',
                'role'  => 'employerRole',
            ],
        ];
        foreach ($users as $u) {
            $faker = new Faker\Generator();
            $faker->addProvider(new Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
            $faker->addProvider(new Faker\Provider\en_SG\PhoneNumber($faker));
            $faker->addProvider(new Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new Faker\Provider\DateTime($faker));
            $faker->addProvider(new Faker\Provider\Internet($faker));
            $user         = new User;
            $user->email  = $u['email'];
            $user->gender = mt_rand(0, 1);

            if ($user->gender == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $user->first_name     = $faker->firstName($gender);
            $user->last_name      = $faker->lastName($gender);
            $user->mobile         = $faker->PhoneNumber();
            $user->birth          = $faker->dateTimeThisCentury->format('Y-m-d');
            $user->address        = $faker->address();
            $user->zipcode        = $faker->postcode();
            $user->city           = $faker->city();
            $user->country        = $faker->country();
            $user->password       = Hash::make('secret');
            $user->email_verified = 1;
            $user->active         = mt_rand(0, 1);
            $user->image          = '/uploads/default_avatar.png';
            $user->cover          = '/uploads/image-cover.jpg';
            $user->save();
            $user->attachRole(${$u['role']});
        }
        for ($i = 0; $i < 50; $i++) {
            $faker = new Faker\Generator();
            $faker->addProvider(new Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
            $faker->addProvider(new Faker\Provider\en_SG\PhoneNumber($faker));
            $faker->addProvider(new Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new Faker\Provider\DateTime($faker));
            $faker->addProvider(new Faker\Provider\Internet($faker));
            $user         = new User;
            $user->email  = $faker->email();
            $user->gender = mt_rand(0, 1);

            if ($user->gender == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }
            $user->first_name     = $faker->firstName($gender);
            $user->last_name      = $faker->lastName($gender);
            $user->mobile         = $faker->PhoneNumber();
            $user->birth          = $faker->dateTimeThisCentury->format('Y-m-d');
            $user->address        = $faker->address();
            $user->zipcode        = $faker->postcode();
            $user->city           = $faker->city();
            $user->country        = $faker->country();
            $user->password       = Hash::make('secret');
            $user->email_verified = mt_rand(0, 1);
            $user->active         = mt_rand(0, 1);
            $user->image          = '/uploads/default_avatar.png';
            $user->save();
            if (mt_rand(0, 1) == 0) {
                $user->attachRole($employerRole);
            } else {
                $user->attachRole($employeeRole);
            }
        }
    }
}
