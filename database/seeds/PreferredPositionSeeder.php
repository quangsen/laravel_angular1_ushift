<?php

use App\Entities\PreferredPosition;
use Illuminate\Database\Seeder;

class PreferredPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $preferred_positions = [
            [
                'name'        => 'service',
                'industry_id' => 1,
            ],
            [
                'name'        => 'kitchen',
                'industry_id' => 1,
            ],
            [
                'name'        => 'retail',
                'industry_id' => 1,
            ],
            [
                'name'        => 'cleaning',
                'industry_id' => 1,
            ],
            [
                'name'        => 'admin',
                'industry_id' => 1,
            ],
            [
                'name'        => 'delivery',
                'industry_id' => 1,
            ],
            [
                'name'        => 'other',
                'industry_id' => 1,
            ],
            [
                'name'        => 'secretary',
                'industry_id' => 2,
            ],
        ];

        foreach ($preferred_positions as $preferred_position) {
            PreferredPosition::create($preferred_position);
        }
    }
}
