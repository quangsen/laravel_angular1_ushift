<?php

use App\Entities\PreferredLocation;
use Illuminate\Database\Seeder;

class PreferredLocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
            'north',
            'south',
            'east',
            'west',
            'central',
            'any',
        ];
        foreach ($locations as $location) {
            PreferredLocation::create(['name' => $location]);
        }
    }
}
