<?php

use App\Entities\EducationLevel;
use Illuminate\Database\Seeder;

class EducationLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['GCSE', 'NVQ', 'A-Levels / IB', 'Bachelors', 'Masters', 'Doctorate'];
        foreach ($data as $name) {
            EducationLevel::create(['name' => $name]);
        }
    }
}
