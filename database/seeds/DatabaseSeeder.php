<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(IndustriesTableSeeder::class);
        $this->call(AvailabilitiesTableSeeder::class);
        $this->call(PreferredPositionSeeder::class);
        $this->call(OccupationsTableSeeder::class);
        $this->call(EmployeePreferredPositionTableSeeder::class);
        $this->call(PreferredLocationsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(WorkingExperienceTableSeeder::class);
        $this->call(EducationLevelTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
        $this->call(LastPositionsTableSeeder::class);
        $this->call(EmployerTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(ConverstationsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        
    }
}
