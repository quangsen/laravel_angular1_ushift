<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferredLocationUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferred_location_user', function (Blueprint $table) {
            $table->integer('pre_location_id')->unsigned()->index();
            $table->foreign('pre_location_id')->references('id')->on('preferred_locations')->onDelete('cascade');
            $table->integer('employee_id')->unsigned()->index();
            $table->foreign('employee_id')->references('id')->on('users')->onDelete('cascade');
            $table->primary(['pre_location_id', 'employee_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('preferred_location_user');
    }
}
