<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePreferredPositionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_preferred_position', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned()->index();
            $table->foreign('employee_id')->references('id')->on('employee')->onDelete('cascade');
            $table->integer('position_id')->unsigned()->index();
            $table->foreign('position_id')->references('id')->on('preferred_positions')->onDelete('cascade');
            $table->primary(['employee_id', 'position_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_preferred_position');
    }
}
