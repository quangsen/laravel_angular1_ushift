<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePreferredLocationPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_preferred_location', function (Blueprint $table) {
            $table->integer('employee_id')->unsigned()->index();
            $table->foreign('employee_id')->references('id')->on('employee')->onDelete('cascade');
            $table->integer('pre_location_id')->unsigned()->index();
            $table->foreign('pre_location_id')->references('id')->on('preferred_locations')->onDelete('cascade');
            $table->primary(['employee_id', 'pre_location_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_preferred_location');
    }
}
