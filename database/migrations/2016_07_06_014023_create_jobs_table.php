<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employer_id')->unsigned();
            $table->foreign('employer_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('image');
            $table->string('address');
            $table->string('latitude', 20);
            $table->string('longitude', 20);
            $table->string('short_desc');
            $table->longText('description');
            $table->text('note');
            $table->string('type');
            $table->string('requirement');
            $table->boolean('active')->default(1);
            $table->boolean('complete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobs');
    }
}
