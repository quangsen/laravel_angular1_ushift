<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('allow_to_work_in_sg');
            $table->string('kind_of_work');
            $table->string('hours_per_week');
            $table->integer('industry_id')->unsinged();
            $table->integer('current_position')->unsigned();
            $table->integer('education_level_id')->unsigned();
            $table->integer('working_experience_id')->unsigned();
            $table->string('resume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee');
    }
}
