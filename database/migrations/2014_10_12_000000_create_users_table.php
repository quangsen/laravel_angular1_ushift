<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 40)->unique();
            $table->string('first_name', 20);
            $table->string('last_name', 20);
            $table->string('image');
            $table->string('cover');
            $table->string('mobile', 20);
            $table->boolean('gender');
            $table->date('birth', 20);
            $table->string('address');
            $table->string('zipcode', 8);
            $table->string('city', 40);
            $table->string('country', 40);
            $table->string('password');
            $table->text('summary');
            $table->boolean('email_verified');
            $table->boolean('is_updated_profile');
            $table->boolean('active')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
