<?php
return [
    '1001' => 'Email already exists',
    '1002' => 'User not found',
    '1003' => 'token doesn\'t match',
    '1004' => 'Email not found',
    '1005' => 'Password doestn\'t match',
];
