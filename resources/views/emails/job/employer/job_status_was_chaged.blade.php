{!! $user->getName() !!} was marked <a href="{!! asset('/job/'.$job->slug) !!}">{!! $job->title !!}</a> as
@if($job->active == 1)
	completed
@else
	incomplete
@endif
