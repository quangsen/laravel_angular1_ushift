@extends('frontend.master')

@section('title', 'Job Detail')

@section('header')
	@include('frontend.partials._header_material')
@stop
@section('content')
	<div class="row box-job-page">
		<div class="container-bt ">
			<div class="col m12 l9">
				<div class="white box-job-detail">
					<div class="col m7 l7 title ">
						<h2>Kitchen Crew (Lunch)</h2>
					</div>
					<div class="col m5 l5 image-represent right-align">
						<img class="activator" src="../assets/images/jobs/office.jpg">
					</div>
					<div class="box-detail-second row">
						<div class="title col s2 ">
							<b>Summary</b>
						</div>
						<div class="content col s10 ">
							<p class="title">Interested in Managing a Seafood Hawker stall?</p>
						</div>
					</div>
					<div class="box-detail-second row">
						<div class="title col s2 ">
							<b>Description</b>
						</div>
						<div class="content col s10">
							<div>
							    <p class="title">Responsibilities:</p>
							    <ul>
							        <li>-&nbsp;&nbsp;Managing the day by day operation of the stall</li>
							        <li>-&nbsp;&nbsp;Manage the daily supply order</li>
							        <li>-&nbsp;&nbsp;Basic cashiering/accounting/handling of cash in and out</li>
							        <li>-&nbsp;&nbsp;Responsible of the stall when the owner is not around</li>
							        <li>-&nbsp;&nbsp;Assist with some Social Media Marketing</li>
							    </ul>
							</div>
							<div>
							    <p class="title">Requirements:</p>
							    <ul>
							        <li>-&nbsp;&nbsp;Someone that is really active (Long standing hours)</li>
							        <li>-&nbsp;&nbsp;Responsible</li>
							        <li>-&nbsp;&nbsp;Honest</li>
							        <li>-&nbsp;&nbsp;Previous managing experience handling F&amp;B business will be an added advantage</li>
							        <li>-&nbsp;&nbsp;Willingness to learn</li>
							    </ul>
							</div>
						</div>
					</div>
					<div class="box-detail-second row">
						<div class="title col s2 ">
							<b>Attire</b>
						</div>
						<div class="content col s10 ">
							<p class="title">Black Jeans/Pants and Non-Slip Shoes(Top Provided)</p>
						</div>
					</div>
					<div class="box-detail-second row">
						<div class="title col s2 ">
							<b>Tags</b>
						</div>
						<div class="content col s10 ">
							<p class="title">kitchen, full time, east coast, service, f&b </p>
						</div>
					</div>
					<div class="box-detail-second row right-align shortlist">
						<span class="">
							Still undecided?
							<a class="waves-effect waves-light btn signup blue-grey lighten-1" href="">Shortlist</a>
						</span>
					</div>
					<div class="box-detail-second ro available">
						<div class="col s12">
							<h3 class="title">Available shifts</h3>
							<p><i class="red-text">Note: Need to commit at least 10 hours</i></p>
							<h4>Week of 25th Jul - 31st Jul</h4>

							<div class="calendar-detail col s6 m3 l2">
							    <div class="row  grey lighten-3">
							    	<p class="title">Mon, 25</p>
							    	<a href="" class="white-text deep-purple lighten-2">
							    		<p>Lunch 10:30am- 3:30pm, $8.00/hr</p>
							    		<p>Places: 2 more</p>
							    	</a>
							    </div>
							</div>
							<div class="calendar-detail col s6 m3 l2">
							    <div class="row  grey lighten-3">
								    <p class="title">Mon, 25</p>
								    <a href="" class="white-text deep-purple lighten-2">
								    	<p>Lunch 10:30am- 3:30pm, $8.00/hr</p>
								    	<p>Places: 2 more</p>
								    </a>
								</div>
							</div>
							<div class="calendar-detail col s6 m3 l2">
								<div class="row  grey lighten-3">
								    <p class="title">Mon, 25</p>
								    <a href="" class="white-text blue darken-2">
								    	<p>Lunch 10:30am- 3:30pm, $8.00/hr</p>
								    	<p>Places: 2 more</p>
								    </a>
								</div>
							</div>
							<div class="col s12 list-button">
								<div class="row">
									<a class="waves-effect waves-light btn blue-grey lighten-1" href="">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i> Review
									</a>
									<a class="waves-effect waves-light btn grey lighten-3 black-text" href="">
										<span>2</span> shifts
									</a>
									<a class="waves-effect waves-light btn grey lighten-3 black-text" href="">
										<span>10</span> hrs $
									</a>
									<a class="waves-effect waves-light btn grey lighten-3 black-text" href="">
										<span>80.00</span>
									</a>
									<a class="waves-effect waves-light btn  blue-grey darken-3" href="">
										Submit
									</a>
								</div>
							</div>
							<div class="col s12 right-align note-shortlisted">
								<span class="">*Note that only shortlisted candidates may be notified.</span>
							</div>
						</div>
						<div class="col s12 box-detail-second left-align shortlist">
							<span class="">
								<a class="waves-effect waves-light btn signup blue-grey lighten-1" href="">More listings</a>
							</span>
						</div>
					</div>
				</div>
				<div class="white box-job-detail bid-box">
					<div class=" row navbar-title grey darken-3">
						<div class="col s9 qualified">
							<p class="white-text">List of qualified candidates (3)</p>
						</div>
						<div class="col s3 price">
							<p class="white-text center-align">Price</p>
						</div>
					</div>
					<div class=" row row-people white lighten-5">
						<div class="col s9 qualified">
							<div class="col s12 m4 center-align qualified-avatar">
									<img class="avatar" src="../assets/images/ben.jpg" alt="ushift">
									<p class="portfolio center-align">
										<a href="">Portfolio</a>
									</p>
							</div>
							<div class="col s12 m8 qualified-information">
								<p class="list-description">
									<span class="name"><a href=""><b>Sanji</b></a></span>
									<span class="birth">06/09/1996</span>
									<span class="country">
										<img src="../assets/images/country/usa.png" alt="usa">
									</span>
									<span class="online">1 minute ago</span>
								</p>
								<p class="text-description">
									Work together as a part of the team to run daily operations. All team members share in all responsibilities from cashiering to prep work so there is always more to learn.
								</p>
							</div>
						</div>
						<div class="col s3 price">
							<p class="center-align"><b>$69</b><br><span>in 2 days</span></p>
						</div>
					</div>
					<div class=" row row-people white lighten-5">
						<div class="col s9 qualified">
							<div class="col s12 m4 center-align qualified-avatar">
									<img class="avatar" src="../assets/images/ben.jpg" alt="ushift">
									<p class="portfolio center-align">
										<a href="">Portfolio</a>
									</p>
							</div>
							<div class="col s12 m8 qualified-information">
								<p class="list-description">
									<span class="name"><a href=""><b>Sanji</b></a></span>
									<span class="birth">06/09/1996</span>
									<span class="country">
										<img src="../assets/images/country/usa.png" alt="usa">
									</span>
									<span class="online">1 minute ago</span>
								</p>
								<p class="text-description">
									Work together as a part of the team to run daily operations. All team members share in all responsibilities from cashiering to prep work so there is always more to learn.
								</p>
							</div>
						</div>
						<div class="col s3 price">
							<p class="center-align"><b>$69</b><br><span>in 2 days</span></p>
						</div>
					</div>
					<div class=" row row-people white lighten-5">
						<div class="col s9 qualified">
							<div class="col s12 m4 center-align qualified-avatar">
									<img class="avatar" src="../assets/images/ben.jpg" alt="ushift">
									<p class="portfolio center-align">
										<a href="">Portfolio</a>
									</p>
							</div>
							<div class="col s12 m8 qualified-information">
								<p class="list-description">
									<span class="name"><a href=""><b>Sanji</b></a></span>
									<span class="birth">06/09/1996</span>
									<span class="country">
										<img src="../assets/images/country/usa.png" alt="usa">
									</span>
									<span class="online">1 minute ago</span>
								</p>
								<p class="text-description">
									Work together as a part of the team to run daily operations. All team members share in all responsibilities from cashiering to prep work so there is always more to learn.
								</p>
							</div>
						</div>
						<div class="col s3 price">
							<p class="center-align"><b>$69</b><br><span>in 2 days</span></p>
						</div>
					</div>
				</div>
				<div class="white box-job-detail related-box">
					<div class="col s12 title ">
						<h3>Other positions nearby...</h3>
					</div>
					<div class="col s12 related-list">
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						<div class="col s6 m4 l3 related-detail">
							<div class="row related-detail-box">
								<p class="view-price">
									<span class="left-align"><i class="fa fa-eye" aria-hidden="true"></i> 1002</span>
									<span class="right-align right"><strong>$</strong> up to $8.00/hr</span>
								</p>
								<div class="img-thumbnail" style="background-image: url('../assets/images/jobs/office.jpg')">
									<a href=""><img src="../assets/images/jobs/default.png" alt="ushift"></a>
									<span class="tag-text">Marina, Esplanade, Suntec</span>
								</div>
								<a href=""><h4 class="title">Server / Service Crew</h4></a>
								<p class="information-shop">
									<span class="text left-align"><a href="">Eat at Seven @Suntec City Mall</a></span>
									<span class="icon right-align right"><i class="fa fa-inbox" aria-hidden="true"></i></span>
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			@include('frontend.sidebar._job_detail')
		</div>
	</div>
@stop