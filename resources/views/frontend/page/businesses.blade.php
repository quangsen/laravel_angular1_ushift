@extends('frontend.master')

@section('title', 'How it Works') 
@section('description', 'How it Works')
@section('description', 'How it Works')

@section('head')
@include('frontend.partials._bootstrap_style')
@stop

@section('header')<!-- ===========================Header Start================================  -->
<header id="shift-seekers-head">
    <div class="container">
        @include('frontend.partials._menu_main')

    </div>
</header>
<!-- /.Header End   -->
@stop
@section('content')
<section id='shift-seekers'>
    <div class="container">
        <div class="shift-seekers_middle">
            <h1>You are short on Staff? </h1>
            <h2>We are here for you </h2>
            <div class="shift-seekers_middle-bottom">
                <div class="middle-bottom right">
                    <p style='font-size: 17px;'>Whether you are confronted with last minute absenteeism, vacation, unexpected turnover or planning for a specific need, Ushift alleviates the stress, time, and costs associated with staffing. </p>
                </div>
                <div class="middle-bottom right" style="padding-top: 20px;">
                    <p style='font-size: 17px;'>Ushift lets you connect at the last minute with screened hard-working professionals on a per-shift or limited engagement basis for a wide range of positions: Servers, Bartenders, Cooks, Porters, Housekeeping, Customer Service, Call Center, Retail, Warehouse Packers, Brand Ambassadors, Promo Models etc.</p>
                </div>
            </div>
        </div>
        <div id="join-ushift">
                <div class="call-to-action"> 
                    <a href="register" class="button-call-to-action">Find your Staff here</a>
                </div>
            </div>
    </div>
</section>
@stop