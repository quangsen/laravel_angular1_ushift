@extends('frontend.master')

@section('title', 'Search')

@section('header')
	@include('frontend.partials._header_material')
@stop

@section('content')
<div class="box-gray">	
	<div class="container-bt">
		<div class="row">
			<div class="white col m12 l2 sub-category">
				<h2 class="title">Refine Results</h2>
				<ul>
					<li class="col s12 m4 l12"><a href="#">All</a></li>
					<li class="col s12 m4 l12"><a href="#">Custome Service</a></li>
					<li class="col s12 m4 l12"><a href="#">Lead Generator</a></li>
					<li class="col s12 m4 l12"><a href="#">Other</a></li>
					<li class="col s12 m4 l12"><a href="#">Call Center</a></li>
					<li class="col s12 m4 l12"><a href="#">Sales</a></li>
				</ul>
			</div>
			<div class="col m12 l10 list-jobs">
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
				<div class="col s6 m4 l3">
					<div class="card">
					    <div class="card-image waves-effect waves-block waves-light">
					      <img class="activator" src="../assets/images/jobs/office.jpg">
					    </div>
					    <div class="card-content">
					      <span class="card-title activator grey-text text-darken-4"><a href=""><h3>Bartender position 3 shifts available</h3></a><i class="button-open material-icons right">more_vert</i></span>
					      <p class="prince">
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-bars fa-lg" aria-hidden="true"></i>
					      	</a>
					      	<a href="" data-hint="Favorite">
					      		<i class="fa fa-heart" aria-hidden="true"></i>
					      	</a>
					      	<span class="right right-align"><i>Includes Packages</i> $5</span>
					      </p>
					    </div>
					    <div class="card-reveal">
					      <span class="card-title grey-text text-darken-4"><h3>Bartender position 3 shifts available</h3><i class="button-close material-icons right">close</i></span>
					      <p>Here is some more information about this product that is only revealed once clicked on.</p>
					    </div>
					  </div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop