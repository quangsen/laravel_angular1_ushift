@extends('frontend.master')

@section('title', 'Ushift')
@section('description','Ushift connects businesses who have shifts to fill with people looking for part time work.')

@section('head')
@include('frontend.partials._bootstrap_style')
@stop

@section('content')
<!-- ===========================Partners================================  -->
<section id="positions">
    <div class="container">
        <h2 class="text-center">100’s of available positions</h2>
        
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/cook.jpg' alt='cook'/>
                    </div>
                    <div class="position-title">
                        <p>Cook</p>
                    </div>
                </div>

            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/bartender.jpg' alt='sales'/>
                    </div>
                    <div class="position-title">
                        <p>Bartender</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/warehouse.jpg' alt='warehouse'/>
                    </div>
                    <div class="position-title">
                        <p>Warehouse</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/door-man.jpg' alt='door-man'/>
                    </div>
                    <div class="position-title">
                        <p>Door Man</p>
                    </div>
                </div>
            </div>
        
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/model.jpg' alt='model'/>
                    </div>
                    <div class="position-title">
                        <p>Model</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/driver.jpg' alt='Driver'/>
                    </div>
                    <div class="position-title">
                        <p>Driver</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/retail.jpg' alt='Retail'/>
                    </div>
                    <div class="position-title">
                        <p>Retail</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class='position-item'>
                    <div class="position-img">
                        <img src='assets/images/position/customer-service.jpg' alt='Customer Service'/>
                    </div>
                    <div class="position-title">
                        <p>Customer Service</p>
                    </div>
                </div>
            </div>
        
    </div>
</section>
<!-- /.Partners End   -->
<!-- ===========================How Ushift Works================================  -->
<!--<section id="ushift-works">
    <div class="container">
        <h2 class="text-center">How Ushift Works</h2>
        <div class="ushift-works-content text-center">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="ushift-works-item">
                        <img src="assets/images/how-it-works_1.png" alt="" />
                        <h3>Choose</h3>
                        <p>The top of the Source Editor has a tab for each open document. Each tab shows the name of the document. If the document has been modified and has not been saved the name of the document is displayed in bold in the tab.</p>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4">
                    <div class="ushift-works-item">
                        <img src="assets/images/how-it-works_2.png" alt="" />
                        <h3>Job gets done</h3>
                        <p>The top of the Source Editor has a tab for each open document. Each tab shows the name of the document. If the document has been modified and has not been saved the name of the document is displayed in bold in the tab.</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="ushift-works-item">
                        <img src="assets/images/how-it-works_3.png" alt="" />
                        <h3>Rate & Relax</h3>
                        <p>The top of the Source Editor has a tab for each open document. Each tab shows the name of the document. If the document has been modified and has not been saved the name of the document is displayed in bold in the tab.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- /.How Ushift Works End   -->

<!-- ===========================How to start================================  -->
<section id="ushift-works" style="background: #c44e38; color: #fff">
    <div class="container" >
        <h2 class="text-center" style="background: #c44e38; color: #fff">How to start</h2>
        <!--        <div class="ushift-works-content text-center">
                    <div class="row">
                        <h3>Ushift connects businesses who have shifts to fill with people looking for part time work. Our website and apps make it easy for businesses to post offers and for qualified workers to claim them.</h3>
                    </div>
                </div>-->
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 different-item-col">
                <div class="different-item">
                    <div class="different-item-text">
                        <h3>For shift seekers</h3>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3" >Step 1</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Register your account.</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3" >Step 2</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Apply for a shift. The employer might engage with you.</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3" >Step 3</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Receive a job offer. Accept.</div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 different-item-col" >
                <div class="different-item">

                    <div class="different-item-text">
                        <h3>For businesses</h3>
                        
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3" >Step 1</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Register your account.</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">Step 2</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Create a shift.</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">Step 3</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">Receive applications of screened candidates matching your search. You can review their skills on their profile and engage with them directly on the platform.</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-3">Step 4</div>
                            <div  class="col-md-9 col-sm-9 col-xs-9">You can send a job offer to whoever you have shortlisted and decide if the job goes to the first to accept.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ===========================How we're different================================  -->
<section id="different" >
    <div class="container">

        <h2 class="text-center" style="padding-bottom: 30px">How we're different</h2>
        <div class="different-content">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h3 style="text-align: center;padding-bottom: 30px">For shift seekers</h3>
                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/real-time.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>Real time shifts</h3>
                                <p>Whenever you have some available time and want to make extra money, US let you screen thousands of available shifts.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/work.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>Work when you want</h3>
                                <p>You organize your schedule as you please.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/cheap.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>Free</h3>
                                <p>The entire process of looking and applying for a shift is free of charge.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <h3 style="text-align: center;padding-bottom: 30px">For businesses</h3>
                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/real-time-staffing.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>Real time staffing</h3>
                                <p>US let you connect at the last minute with qualified shift seekers.</p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/perfect-match.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>A perfect match</h3>
                                <p>We always make sure that our candidates who apply for your available positions match your criteria.</p>
                            </div>
                        </div>
                    </div>
                    <!--                <div class="col-md-6 col-sm-6">
                                        <div class="different-item">
                                            <div class="different-item-img">
                                                <img src="assets/images/diff.png" alt="" />
                                            </div>
                                            <div class="different-item-text">
                                                <h3>Free</h3>
                                                <p>The entire process of posting a shift and recruiting a part timer is free of charge.</p>
                                            </div>
                                        </div>
                                    </div>-->

                    <div class="col-md-12 col-sm-12">
                        <div class="different-item">
                            <div class="different-item-img">
                                <img src="assets/images/difference/save-time.jpg" alt="" />
                            </div>
                            <div class="different-item-text">
                                <h3>Time saved</h3>
                                <p>We interview and proceed to a background check of all shift seekers.</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!-- /.How we're different End   -->
<!-- ===========================What Our Clients Say================================  -->

<!--
<section id="testimonial">
    <div class="container">
        <h2 class="text-center">What Our Clients Say</h2>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="testimonial-item">
                    <p>
                        <i class="fa fa-quote-left" aria-hidden="true"></i> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.The top of the Source Editor has a tab for each open document.
                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                    </p>
                    <br/>
                    <p>- Thoko Scoot, Event Manager, Mixology Event</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="testimonial-item">
                    <p>
                        <i class="fa fa-quote-left" aria-hidden="true"></i> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.The top of the Source Editor has a tab for each open document.
                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                    </p>
                    <br/>
                    <p>- Thoko Scoot, Event Manager, Mixology Event</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="testimonial-item">
                    <p>
                        <i class="fa fa-quote-left" aria-hidden="true"></i> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.The top of the Source Editor has a tab for each open document.
                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                    </p>
                    <br/>
                    <p>- Thoko Scoot, Event Manager, Mixology Event</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="testimonial-item">
                    <p>
                        <i class="fa fa-quote-left" aria-hidden="true"></i> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.
                        <i class="fa fa-quote-right" aria-hidden="true"></i>
                    </p>
                    <br/>
                    <p>- Thoko Scoot, Event Manager, Mixology Event</p>
                </div>
            </div>
        </div>
    </div>
</section>-->



<!-- /.What Our Clients Say End   -->
<!-- ===========================What Do We Stand For================================  -->
<!--<section id="stand-for">
    <div class="container">
        <h2 class="text-center">What Do We Stand For</h2>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="stand-for-item">
                    <h3>We believe in fair employment standards </h3>
                    <p> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by The world's leading conservation organization, WWF works in 100 countries and is supported by</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="stand-for-item">
                    <img src="assets/images/pencil.png" alt="" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="stand-for-item">
                    <h3>We treat people as humans</h3>
                    <p> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by The world's leading conservation organization, WWF works in 100 countries and is supported by</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="stand-for-item">
                    <h3>We want to create more freedom </h3>
                    <p> For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by The world's leading conservation organization, WWF works in 100 countries and is supported by</p>
                    <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization,</p>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- /.What Do We Stand For End   -->
@stop
