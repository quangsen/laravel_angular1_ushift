@extends('frontend.master') 

@section('title', 'About Us') 

@section('head')
    @include('frontend.partials._bootstrap_style')
@stop

@section('header')
<!-- ===========================Header Start================================  -->
<header id="about-head">
    <div class="container">
        @include('frontend.partials._menu_main')
        <div class="header_middle">
            <h1>Find a shift. Fill a shift. We make it easy.</h1>
            <div class="header_middle-bottom">
                <div class="middle-bottom-left">
                    <h2>Our Team</h2>
                </div>
                <div class="middle-bottom-right">
                    <p>We are a team of passionate entrepreneurs based in Singapore building a technology focused on only one thing: connect people who want to do temporary extra work with businesses who need them.</p>
                    <!--<p>We are an ambitious team of entrepreneurs, developers and hiring specialist on a mission to build nothing less than the world's best platform to find and offer short term jobs. In a highly competitive market with new players on the rise we set out to be on the forefront of creating the futureof employement. We won't stop until we've achieved this dream!</p>-->
                </div>
            </div>
        </div>
    </div>
</header>
<!-- /.Header End   -->
@stop
@section('content')


<section id="team">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <h2>Location</h2>
            </div>
        </div>-->
        <div class="team-member">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/singapore.jpg" alt="Singapore" />
                        </div>
                        <div class="mem-intro">
                            <h4>Singapore</h4>
                            <p class="position">Address</p>
                            <p>346 River Valley Road, #20-01, Singapore 238373
                            </p>
                        </div>
                    </div>
                </div>
                 <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/hong-kong.jpg" alt="Hong Kong" />
                        </div>
                        <div class="mem-intro">
                            <h4>Hong Kong</h4>
                            <p class="position">Comming soon</p>
                            <!--<p>346 River Valley Road, #20-01, Singapore 238373-->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/vietnam.jpg" alt="Vietnam" />
                        </div>
                        <div class="mem-intro">
                            <h4>Vietnam</h4>
                            <p class="position">Comming soon</p>
                            <!--<p>346 River Valley Road, #20-01, Singapore 238373-->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
<div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/thailand.jpg" alt="Thailand" />
                        </div>
                        <div class="mem-intro">
                            <h4>Thailand</h4>
                            <p class="position">Comming soon</p>
                            <!--<p>346 River Valley Road, #20-01, Singapore 238373-->
                            </p>
                        </div>
                    </div>
                </div>
               
    <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/indonesia.jpg" alt="Indonesia" />
                        </div>
                        <div class="mem-intro">
                            <h4>Indonesia</h4>
                            <p class="position">Comming soon</p>
                            <!--<p>346 River Valley Road, #20-01, Singapore 238373-->
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/flags/malaysia.jpg" alt="Malaysia" />
                        </div>
                        <div class="mem-intro">
                            <h4>Malaysia</h4>
                            <p class="position">Comming soon</p>
                            <!--<p>346 River Valley Road, #20-01, Singapore 238373-->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ===========================The Team================================  -->
<!--<section id="team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>The Team</h2>
            </div>
        </div>
        <div class="team-member">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/steffen.jpg" alt="Steffen" />
                        </div>
                        <div class="mem-intro">
                            <h4>Steffen Wulff</h4>
                            <p class="position">Cofounder</p>
                            <p>Steffen Wulff is one of the founders, he provides the level headed direction to both the company and his Rammstein tribute barber shop quartet. Loves drinking vinegar from a teacup and highly polished shoes.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/oli.jpg" alt="Oli johnson" />
                        </div>
                        <div class="mem-intro">
                            <h4>Oli johnson</h4>
                            <p class="position">Cofounder</p>
                            <p>OliJohnson is one of the founders, is an excellent communicator, but only through the art of interpretive dance or email. His proudest moment is every morning when he has finished styling his hair.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/ben.jpg" alt="Ben Dixon" />
                        </div>
                        <div class="mem-intro">
                            <h4>Ben Dixon</h4>
                            <p class="position">Cofounder & CTO</p>
                            <p>Ben Dixon is the technical founder who's self proclaimed highest moment was winning a look-a-Iike contest as a character from Lord of the Rings - though he refuses to admit which one. A cereal polygamist he has a different breakfast every day.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/pete.jpg" alt="Pete Hykin" />
                        </div>
                        <div class="mem-intro">
                            <h4>Pete Hykin</h4>
                            <p class="position">Head of Business Development</p>
                            <p>Pete Hykin heads the company mime troupe. He‘s efficient, ruthless and tactical. But only at connect four. Loves puppies and wearing slippers in the office. Owns a complete set of signed High school musical movies.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/james.jpg" alt="James White" />
                        </div>
                        <div class="mem-intro">
                            <h4>James White</h4>
                            <p class="position">Lead UX Developer</p>
                            <p>James White is part of the development team and spends most of his time in the dark, debugging things he doesn’t really understand. Goes to the gym frequently with little effect, no-one is really sure what he does there. Once ate a vegetable by mistake.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/kevin.jpg" alt="Kevin Edwards" />
                        </div>
                        <div class="mem-intro">
                            <h4>Kevin Edwards</h4>
                            <p class="position">Lead Backend Developer</p>
                            <p>Kevin Edwards is part of the development team. He spends his days grimacing at his screen, his lunch and the people around him. Is deeply allergic to criticism and onions. He is very proud of his collection of unusual moulds.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/jette.jpg" alt="Jette Schaffran" />
                        </div>
                        <div class="mem-intro">
                            <h4>Jette Schaffran</h4>
                            <p class="position">Business Development</p>
                            <p>Jette is a Belgian who is as committed to Business Development as she is to building a friendly and welcoming office environment. Unfortunately Jette lost the ability to express emotions after an elaborate practical joke went wrong involving a taxidermied crocodile stuffed with hard boiled eggs and two hundred pounds of soft cheese. Dislikes emoji, red ink and practical jokes.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/heather.jpg" alt="Heather Everitt" />
                        </div>
                        <div class="mem-intro">
                            <h4>Heather Everitt</h4>
                            <p class="position">Project Manager</p>
                            <p>Marion manages ushift operations with the dignity and grace one would not expect from someone who's first career was as a stage mind-reader. She is enthusiastically involved with the shoe-horn collectors guild and invented her own star sign: Caseusian - the sign for which is a disturbing mix of animal parts and pine cones.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="team-single">
                        <div class="mem-img">
                            <img src="assets/images/marion.jpg" alt="Marion De Najar" />
                        </div>
                        <div class="mem-intro">
                            <h4>Marion De Najar</h4>
                            <p class="position">Operations Manager</p>
                            <p>Marion manages ushift operations with the dignity and grace one would not expect from someone who's first career was as a stage mind-reader. She is enthusiastically involved with the shoe-horn collectors guild and invented her own star sign: Caseusian - the sign for which is a disturbing mix of animal parts and pine cones.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!-- /.The Team End -->


@stop