@extends('frontend.master')

@section('title', 'How it Works') 

@section('head')
@include('frontend.partials._bootstrap_style')
@stop

@section('header')
<!-- ===========================Header Start================================  -->
<header id="hiw-head"  class="bg-default">
    <div class="container">
        @include('frontend.partials._menu_main')
    </div>
</header>
@stop

@section('content')
<section id='howitworks'>
    <div class="container">
        <div class="howitworks_middle">
            <h1>How Ushift Works ? </h1>
            <h2>It's very simple</h2>
            <div class="howitworks_middle-bottom">

                <div class="middle-bottom right">
                    <p>Ushift connects businesses who have shifts to fill with people looking for part time work. Our website and apps make it easy for businesses to post offers and for qualified workers to claim them.</p></p>
                </div>
            </div>
        </div>
    </div>
</section>


<!--<section id='faq-staff'>
    <div class="container">
        <div class="faq-staff_middle">
            <h1>Frequently Asked Questions</h1>
            <h2>Staff FAQ's</h2>
            <div class="faq-staff_middle-bottom">
                <div class="col-md-6">
                    <div class="question-text">
                        <h3>When will I get paid?</h3>
                        <div class="step">
                            <p>You will be paid on the second Friday after the end of the working week.</p>
                            <p>You can see when you are going to be next paid and how much it will be in the Ushift app.</p>
                        </div>                        
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="question-text">
                        <h3>What if I’m going to be late for a shift?</h3>
                        <div class="step">
                            <p>You should contact Ushift and the employer if you are going to be late for a shift. With enough warning the employer can prepare for it.</p>
                            <p>Your punctuality is listed on your proﬁle. If an employer notes a poor punctuality on your time sheet it may reduce your job offers from other employers for a while.</p>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id='faq-employer'>
    <div class="container">
        <div class="faq-employer_middle">
            <h1>Frequently Asked Questions</h1>
            <h2>Employers FAQ's</h2>
            <div class="faq-employer_middle-bottom">
                <div class="col-md-6">
                    <div class="question-text">
                        <h3>How do I create a new job offer?</h3>
                        <div class="step">
                            <p class="step-no">Step-1 Login</p>
                            <p class="step-content">New to the platform? <a href="{{ url('register') }}">Click here to sign up</a></p>
                            <p class="step-content">Already on the platform? <a href="{{ url('login') }}">Click here to Log in</a></p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-2 Create an engagement</p>
                            <p class="step-content">Just click on "New Engagement" </p>
                            <p class="step-content">Select the type of staff you need, your postcode and the number of staff you would like.</p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-3 Shortlist Candidates</p>
                            <p class="step-content">You’ll see a list of Candidates that match your search. You can review their
                                skills and check their language skills and personality by watching their
                                video.
                            </p>
                            <p class="step-content">Shortlist the people you would like to make an offer to. Some people might
                                not be able to take the job, you will have to shortlist a minimum of 3 people
                                for every staff member you would like to ﬁll.
                            </p>
                            <p class="step-content">Note: The ﬁrst people who accept the role will be the ones who do the job.</p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-4 Create Your Offer</p>
                            <p class="step-content">Click "Create Job Offer"</p>
                            <p class="step-content">Select the date of the ﬁrst shift. You can arrange future shifts directly with the staff members.</p>
                            <p class="step-content">Complete the form, it will ask you for everything the staff need to know. Put in as much information as you can to encourage people to accept your offer.</p>
                            <p class="step-content">Click "Send Offer” and you’re done.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="question-text" style='padding-bottom: 50px; '>
                        <h3>Can I choose who gets the job?</h3>
                        <div class="step">
                            <p>You can shortlist the people you would like to work with. Your offer goes to
                                all these people and the ﬁrst to accept gets the job.</p>
                            <p>Our staff are often busy, so this system gives you the best chance for your job being ﬁlled.</p>
                        </div>                        
                    </div>
                    <div class="question-text">
                        <h3>I’ve worked with someone before,can I use them again?</h3>
                        <div class="step">
                            <p>You can shortlist the people you would like to work with. Your offer goes to
                                all these people and the ﬁrst to accept gets the job.
                            </p>
                            <p>Our staff are often busy, so this system gives you the best chance for your job being ﬁlled.</p>
                        </div>                        
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>-->

<!--<section id="howitworks">
    <div class="container-fluid text-center">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>How Ushift Works</h2>
            </div>
            <div class="how-it-works-p">
            <p>
        Ushift connects businesses who have shifts to fill with people looking for part time work. Our website and apps make it easy for businesses to post offers and for qualified workers to claim them.</p>
            </div>
            </div>
        
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Frequently Asked Questions</h2>
            </div>
        </div>
        <div class="row">
            <div class="employers-faq">
                <div class="go-staff">
                    <a href="" id="go-to-staff">Skip to Staff FAQ's <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                </div>
                <div class="faq-title">
                    <h2>Employers FAQ's</h2>
                </div>
            </div>
        </div>
    </div>    
    <div class="container">       
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="faq-question-wrapper">
                    <div class="question-icon">
                        <img src="assets/images/FAQ-icon.png" alt="FAQ" />
                     </div>
                    <div class="question-text">
                        <h3>How do I create a new job offer?</h3>
                        <div class="step">
                            <p class="step-no">Step-1 Login</p>
                            <p>New to the platform? <a href="{{ url('register') }}">Click here to sign up</a></p>
                            <p>Already on the platform? <a href="{{ url('login') }}">Click here to Log in</a></p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-2 Create an engagement</p>
                            <p>Just click on "New Engagement" </p>
                            <p>Select the type of staff you need, your postcode and the number of staff you would like.</p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-3 Shortlist Candidates</p>
                            <p>You’ll see a list of Candidates that match your search. You can review their
                            skills and check their language skills and personality by watching their
                            video.
                            </p>
                            <p>Shortlist the people you would like to make an offer to. Some people might
                            not be able to take the job, you will have to shortlist a minimum of 3 people
                            for every staff member you would like to ﬁll.
                            </p>
                            <p>Note: The ﬁrst people who accept the role will be the ones who do the job.</p>
                        </div>
                        <div class="step">
                            <p class="step-no">Step-4 Create Your Offer</p>
                            <p>Click "Create Job Offer"</p>
                            <p>Select the date of the ﬁrst shift. You can arrange future shifts directly with the staff members.</p>
                            <p>Complete the form, it will ask you for everything the staff need to know. Put in as much information as you can to encourage people to accept your offer.</p>
                            <p>Click "Send Offer” and you’re done.</p>
                        </div>
                    </div>
                </div>
                <div class="faq-question-wrapper">
                    <div class="question-icon">
                        <img src="assets/images/FAQ-icon.png" alt="FAQ" />
                     </div>
                    <div class="question-text">
                        <h3>Can I choose who gets the job?</h3>
                        <div class="step">
                            <p>You can shortlist the people you would like to work with. Your offer goes to
                            all these people and the ﬁrst to accept gets the job.</p>
                            <p>Our staff are often busy, so this system gives you the best chance for your job being ﬁlled.</p>
                        </div>                        
                    </div>
                </div>
                <div class="faq-question-wrapper">
                    <div class="question-icon">
                        <img src="assets/images/FAQ-icon.png" alt="FAQ" />
                     </div>
                    <div class="question-text">
                        <h3>I’ve worked with someone before,can I use them again?</h3>
                        <div class="step">
                            <p>You can shortlist the people you would like to work with. Your offer goes to
                                all these people and the ﬁrst to accept gets the job.
                            </p>
                            <p>Our staff are often busy, so this system gives you the best chance for your job being ﬁlled.</p>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    <div class="container-fluid text-center">       
        <div class="row">
            <div class="employers-faq">
                <div class="go-staff">
                    <a href="" id="go-to-em">Skip to Employers FAQ's <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                </div>
                <div class="faq-title">
                    <h2>Staff FAQ's</h2>
                </div>
            </div>
        </div>
    </div> 
    
    <div class="container">       
        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <div class="faq-question-wrapper">
                    <div class="question-icon">
                        <img src="assets/images/FAQ-icon.png" alt="FAQ" />
                     </div>
                    <div class="question-text">
                        <h3>When will I get paid?</h3>
                        <div class="step">
                            <p>You will be paid on the second Friday after the end of the working week.</p>
                            <p>You can see when you are going to be next paid and how much it will be in the Ushift app.</p>
                        </div>                        
                    </div>
                </div>
                <div class="faq-question-wrapper">
                    <div class="question-icon">
                        <img src="assets/images/FAQ-icon.png" alt="FAQ" />
                     </div>
                    <div class="question-text">
                        <h3>What if I’m going to be late for a shift?</h3>
                        <div class="step">
                            <p>You should contact Ushift and the employer if you are going to be late for a shift. With enough warning the employer can prepare for it.</p>
                            <p>Your punctuality is listed on your proﬁle. If an employer notes a poor punctuality on your time sheet it may reduce your job offers from other employers for a while.</p>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>    /.How It Works End   -->
@stop