@extends('frontend.master')

@section('title', 'How it Works') 
@section('description', 'How it Works')
@section('description', 'How it Works')

@section('head')
    @include('frontend.partials._bootstrap_style')
@stop

@section('header')<!-- ===========================Header Start================================  -->
<header id="shift-seekers-head">
    <div class="container">
        @include('frontend.partials._menu_main')

    </div>
</header>
<!-- /.Header End   -->
@stop
@section('content')
<section id='shift-seekers'>
    <div class="container">
        <div class="shift-seekers_middle">
            <h1>You are looking for a Shift? </h1>
            <h2>We are here for you </h2>
            <div class="shift-seekers_middle-bottom">
 
                <div class="middle-bottom right">
                    <p>Whenever you have some available time and want to make extra money, Ushift hooks you up with thousands of available shifts in the coolest venues and hottest companies in town.</p>
                </div>
            </div>
        </div>
        <div id="join-ushift">
            <div class="call-to-action"> 
                <a href="register" class="button-call-to-action">Find a job now</a>
            </div>
        </div>
    </div>
    
</section>
@stop