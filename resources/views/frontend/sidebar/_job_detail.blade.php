<div class="col m12 l3 sidebar-job">
	<h1 class="title right-align">Hana Nana</h1>
	<div class="col s12 offset-l2 l10 white space-bottom">
		<img class="logo-shop" src="../assets/images/jobs/office.jpg" alt="logo shop">
		<div class="about-us row">
			<h3 class="title">About us...</h3>
			<p class="content">
				A bistro using french cooking techniques, world flavors. Serving gastronomical dishes of fine dining standard with affordable prices in casual setting.
			</p>
		</div>
	</div>
	<div class="col s12 offset-l2 l10 white space-bottom">
		<div class="map">
		<h3 class="title">Location</h3>
			<p class="light-blue-text">
				456 Alexandra Road, Nol Building, Pasir Panjang, Alexandra, Kent Ridge, Singapore 119962
			</p>
			<div class="map-box">
				<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d178451.34605485946!2d-95.78065978859752!3d33.78988316689249!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s456+Alexandra+Road%2C+Nol+Building%2C+Pasir+Panjang%2C+Alexandra%2C+Kent+Ridge%2C+Singapore+119962!5e0!3m2!1sen!2s!4v1468916411780" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div class="col s12 offset-l2 l10 white space-bottom">
		<div class="tag-category">
		<h3 class="title">Category</h3>
			<p class="list-cate">
				<a class="waves-effect waves-light btn signup blue-grey lighten-1" href="">Food</a>
				<a class="waves-effect waves-light btn signup blue-grey lighten-1" href="">New</a>
				<a class="waves-effect waves-light btn signup blue-grey lighten-1" href="">japan</a>
			</p>
		</div>
	</div>
</div>