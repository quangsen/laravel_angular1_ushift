<!DOCTYPE html>
<html lang="en" ng-app="hybird-app" ng-controller="MainCtrl">

<head>
	<link rel="icon" type="image/ico" href="{!! asset('assets/images/favicon/favicon.png') !!}">
	<title>@section('title')Ushift @show </title>
        <meta name="description" content="@section('description') Ushift @show">
        <meta name="keywords" content="@yield('keywords')workers comp, W2, Background checks, Payroll services, Payroll taxes, Summer jobs, Holiday help, part-time retail, retail jobs, temp retail job, part-time, retail temp, temp staffing, small business, small business retail job, On demand staffing, on demand retail staffing, Retail staffing, Retail staff, PT Retail Help, FT Retail Associates, PT Sales Associate, Temp Sales Associates, flexible schedule, retail staffing agency, staffing agency, retail job, customer service job, retail sales job, staffing platform, retail job, Event staff, Event jobs @show">
    @section('head') 
        @include('frontend.partials._style') 
    @show
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>	
	@section('header')
		@include('frontend.partials._header') 
	@show

	@yield('content')

	@section('footer')
		@include('frontend.partials._footer') 
	@show
</body>

</html>
