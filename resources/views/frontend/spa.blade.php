<!DOCTYPE html>
<html ng-app="app">

<head>
    <title ng-bind="title"></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1, minimum-scale=0.5, maximum-scale=1.0" />
    <link rel="stylesheet" href="{!! asset('assets/css/library.min.css') !!}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('assets/css/app.css') !!}">
    <script type="text/javascript">
    var BASE_URL = "{!! asset('') !!}";
    var GOOGLE_MAP_API_KEY = "AIzaSyC8kSmRKmdDjAkfOMQpNBA6brXBOCcuoTs";
    var JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token";
    </script>
    <script src="{!! asset('assets/js/app.js') !!}"></script>
</head>

<body>
    <div ui-view="main"></div>
</body>

</html>
