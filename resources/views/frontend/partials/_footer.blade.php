<!-- Scroll Top Start -->
<div class="scroll-to-top">
    <div class="scrollup">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </div>
</div>
<!-- Scroll Top End -->

<!-- ===========================Footer Start================================  -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col s12 m4">
                <div class="footer-left">
                    <div class="footer-logo">
                        <img src="assets/images/footer_logo.png" alt="" />
                    </div>
                    <div class="address">
                        <address>
                            <a href="tel:+6582452082"><i class="fa fa-headphones" aria-hidden="true"></i> +65 8245 2082</a>
                            <br>
                            <a href="mailto:hello@ushift.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> hello@ushift.com</a>
                            <br>
<!--                            <a href=""><i class="fa fa-comment-o" aria-hidden="true"></i> Live Chat</a>
                            <br>-->
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col s12 m8">
                <div class="footer-right row">
                    <p>Copyright &copy; 2016 All Right Reserved By Ushift Pte Ltd</p>
                    <ul>
                        <li><a href="{{ url('about') }}">About Us</a></li>
                        <li><a href="{{ url('terms-of-use') }}">Terms of Use</a></li>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('cookie-policy') }}">Cookie Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /.Footer End   -->
