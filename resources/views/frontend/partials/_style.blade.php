<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/font-awesome.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/main.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('bower_components/Materialize/dist/css/materialize.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style_dev.css') !!}">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<script type="text/javascript" src="{!! asset('bower_components/jquery/dist/jquery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('bower_components/Materialize/dist/js/materialize.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('assets/js/main.js') !!}"></script>