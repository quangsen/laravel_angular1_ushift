<!-- ===========================Header Start================================  -->
<header>
    <div class="container">
        @include('frontend.partials._menu_main')
        <div class="header_middle">
            <h1>Low on staff?</h1>
            <h2>We're here for you.</h2>
        </div>
    </div>
    <div class="header_bottom" style="min-height: 150px;">
        <div class="container" style=" display: none;">
            <div class="header-bottom-content" >
                <p>Enter your details to browse and book our staff</p>
                <form>
                    <div class="row">
                        <div class="col-md-3 col-sm-3 input-item">
                            <div class="form-group">
                                <label>Your email address?</label>
                                <input type="email" class="form-control" placeholder="Email Address *">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 input-item">
                            <div class="form-group">
                                <label>Your name?</label>
                                <input type="text" class="form-control" placeholder="Your Name">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 input-item">
                            <div class="form-group">
                                <label>Your Phone number?</label>
                                <input type="text" class="form-control" placeholder="Phone no.">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 input-item">
                            <input type="submit" class="btn btn-default btn-lg" value="Browse Staff">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<!-- /.Header End   -->
