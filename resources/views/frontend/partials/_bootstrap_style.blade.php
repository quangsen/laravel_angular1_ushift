<link rel="stylesheet" type="text/css" href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/font-awesome.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/main.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/responsive.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('assets/css/style_dev.css') !!}">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<script type="text/javascript" src="{!! asset('bower_components/jquery/dist/jquery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>
<script type="text/javascript">
	var BASE_URL = "{!! asset('') !!}";
	var JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token";
</script>
<script type="text/javascript" src="{!! asset('assets/js/hybird.js') !!}"></script>