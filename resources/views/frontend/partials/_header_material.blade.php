<div class="black">
	<div class="container-bt row">
		<div class="col s6 m3 logo-main">
			<a href="{{ url('/') }}" class="brand-logo"><img src="assets/images/footer_logo.png" alt="LOGO" /></a>
		</div>
		<div class="col s6 m3 box-login right-align">
			<a class="white-text login" href="#">Log in</a>
			<a class="waves-effect waves-light btn signup" href="">Sign Up</a>
		</div>
		<div class="col s12 m6 search">
			<div class="box-search input-search search-wrapper card focused">
				<input id="search"><i class="material-icons">search</i>
				<div class="search-results"></div>
			</div>
		</div>
	</div>
</div>
<nav class="white menu-list-job">
	<div class="container-bt">
      <div class="nav-wrapper">
        <a href="#" data-activates="mobile-demo" class="button-collapse black-text right"><i class="material-icons">menu</i></a>
        <ul class="left hide-on-med-and-down">
        	@include('frontend.partials._menu_search')
        </ul>
        <ul class="side-nav" id="mobile-demo">
        	@include('frontend.partials._menu_search')
        </ul>
      </div>
	</div>
</nav>