<div class="header_top">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (isset($pagename) && $pagename == 'home') 
            <a class="navbar-brand" href="{{ url('/') }}"><img src="assets/images/logo.png" alt="LOGO" /></a>
            @else
            <a class="navbar-brand" href="{{ url('/') }}"><img src="assets/images/logo-white.png" alt="LOGO" /></a>
            @endif
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('how-it-works') }}" @if (isset($pagename) && $pagename == 'home') class="white-shadow" @endif>How it works</a></li>
                <li><a href="{{ url('shift-seekers') }}" @if (isset($pagename) && $pagename == 'home') class="white-shadow" @endif>For shift seekers</a></li>
                <li><a href="{{ url('businesses') }}" @if (isset($pagename) && $pagename == 'home') class="white-shadow" @endif>For businesses</a></li>
                <li class="login" ng-if="!isLoggedIn"><a href="{{ url('login') }}">Sign Up/Login</a></li>
                <li class="login" ng-if="isLoggedIn && user.isEmployer()"><a href="{{ url('dashboard/home') }}">Dashboard</a></li>
                <li class="login" ng-if="isLoggedIn && user.isEmployee()"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            </ul>
        </div>
    </nav>
</div>