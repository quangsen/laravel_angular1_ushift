# Installation
Make a file .env is copy of .env.example and fill with your infomations
Run composer install command
```
composer install
```
3. Create database by run migration command [https://laravel.com/docs/5.1/migrations]
```
php artisan migrate --seed
```
every time we need to update database, just run command
```
php artisan migrate
```
if we want remove all database and migration again
```
php artisan migrate:reset
```
4. Install bower modules
Go to public folder
```
bower install
```

# APIs
Postman collection
```
https://www.getpostman.com/collections/4f109135ac4b8d53a1bb
```