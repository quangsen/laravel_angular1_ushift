$(document).ready(function() {
    //for scroll top        
    $(window).scroll(function() {
        if ($(this).scrollTop() > 150) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
        return false;
    });

    //Material
    $('.button-collapse').sideNav();

});
